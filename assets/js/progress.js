$(function() {
  var current_progress = 0;
  var interval = setInterval(function() {
      current_progress += 10;
      $(".dynamic")
      .css("width", current_progress + "%")
      .attr("aria-valuenow", current_progress)
      .text(current_progress + "%");
      if (current_progress >= 80)
          clearInterval(interval);
  }, 1000);
});

$(function() {
  var current_progress = 0;
  var interval = setInterval(function() {
      current_progress += 20;
      $(".dynamic2")
      .css("width", current_progress + "%")
      .attr("aria-valuenow", current_progress)
      .text(current_progress + "%");
      if (current_progress >= 100)
          clearInterval(interval);
  }, 1000);
});

$(function() {
  var current_progress = 0;
  var interval = setInterval(function() {
      current_progress += 10;
      $(".dynamic3")
      .css("width", current_progress + "%")
      .attr("aria-valuenow", current_progress)
      .text(current_progress + "%");
      if (current_progress >= 90)
          clearInterval(interval);
  }, 1000);
});

$(function() {
  var current_progress = 0;
  var interval = setInterval(function() {
      current_progress += 20;
      $(".dynamic4")
      .css("width", current_progress + "%")
      .attr("aria-valuenow", current_progress)
      .text(current_progress + "%");
      if (current_progress >= 50)
          clearInterval(interval);
  }, 1000);
});

$('.form').find('input, textarea').on('keyup blur focus', function (e) {
  
  var $this = $(this),
      label = $this.prev('label');

	  if (e.type === 'keyup') {
			if ($this.val() === '') {
          label.removeClass('active highlight');
        } else {
          label.addClass('active highlight');
        }
    } else if (e.type === 'blur') {
    	if( $this.val() === '' ) {
    		label.removeClass('active highlight'); 
			} else {
		    label.removeClass('highlight');   
			}   
    } else if (e.type === 'focus') {
      
      if( $this.val() === '' ) {
    		label.removeClass('highlight'); 
			} 
      else if( $this.val() !== '' ) {
		    label.addClass('highlight');
			}
    }

});

$('.tab a').on('click', function (e) {
  
  e.preventDefault();
  
  $(this).parent().addClass('active');
  $(this).parent().siblings().removeClass('active');
  
  target = $(this).attr('href');

  $('.tab-content > div').not(target).hide();
  
  $(target).fadeIn(600);
  
});