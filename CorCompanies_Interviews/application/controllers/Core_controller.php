<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Core_controller extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('core_model');
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->helper(array('form', 'url'));
    }
    public function loadpage()
    {
      $page=$this->uri->segment('1');
      $data['page_title']=$this->uri->segment('1');
      $this->load->view($page, $data);
      $this->load->view('footer');
    }
    public function index()
    {
      $data['page_title']=$this->uri->segment('1');
      $this->load->view('header',$data);
      $this->load->view('index');
      $this->load->view('footer');
    }

    public function home()
    {
      $data['page_title']=$this->uri->segment('1');
      if ($this->session->userdata('name') == '') {
      redirect('login');
      } else {
      $data['id'] = $this->session->userdata('id');
      $id = $this->session->userdata('id');
      $data['status'] = $this->session->userdata('status');
      $status = $this->session->userdata('status');
      $data['transac_id'] =  $this->core_model->get_transac_id($id);
      $this->load->view('home', $data);
      $this->load->view('footer');
    }
  }
    public function signup()
    {
      $email=$this->input->post('email');
      $mail_exist = $this->core_model->ismail_exist($email);
      if(!$mail_exist)
      {
        $query=$this->core_model->signup();
          if($query > 0)
          {
            $username=$this->input->post('email');
            $password=$this->input->post('password');
            $email=$this->input->post('email');
            $this->load->model('Mail_model');
            $this->Mail_model->sendRegistrationEmail($username,$password,$email);
            $this->session->set_flashdata('registration','You have signed up successfully......');
            $this->session->set_flashdata('message','Password and Username is send to your mail id !! If not found in inbox please check the spam Folder.');
            redirect('login');
          }
          else
          {
            $this->session->set_flashdata('registererr','Failed.... Please try again...');
            redirect('login');
          }
      }
      else
      {
        $this->session->set_flashdata('mailererr','Email alredy exists...');
            redirect('login');
      }
    }
    public function login()
    {

        $login=$this->core_model->login();
        if($login)
        {
          $this->session->set_userdata('cand_data',$login);
          $status = $this->session->userdata('status');
          $type = $this->session->userdata('type');
          $id = $this->session->userdata('id');
          $name = $this->session->userdata('name');
          if($status == 0 && $type == "cand_core")
          {
            redirect('profile1');
          }
          elseif($status == 2 && $type == "cand_core")
          {
            $data['status'] = $this->session->userdata('status');
            $data['transac_id'] =  $this->core_model->get_transac_id($id);
            $this->load->view('home', $data);
            $this->load->view('footer');
          }
          elseif($status == 1 && $type == "cand_core")
          {
            // redirect('index');
            $data['status'] = $this->session->userdata('status');
            $data['transac_id'] =  $this->core_model->get_transac_id($id);
            $this->load->view('home', $data);
            $this->load->view('footer');
          }
        }
        else
        {
            $this->session->set_flashdata('loginerror','Username or Password Is Incorrect');
            redirect('login');
        }
    }

    public function logout()
    {
      $this->session->unset_userdata('cand_data');
      $this->session->sess_destroy();
      $data['page_title']=$this->uri->segment('1');
      $this->load->view('login',$data);
      $this->load->view('footer');
    }

    public function profile()
    {
        $data['page_title']=$this->uri->segment('1');
        if ($this->session->userdata('name') == '') {
        redirect('login');
        } else {
        $data['id'] = $this->session->userdata('id');
        $id = $this->session->userdata('id');
        $data['cand_details'] =  $this->core_model->get_cand_details($id);
        $this->load->view('profile1',$data);
        }
    }

    public function profile2()
    {
        $data['page_title']=$this->uri->segment('1');
        if ($this->session->userdata('name') == '') {
        redirect('login');
        } else {
        $data['id'] = $this->session->userdata('id');
        $id = $this->session->userdata('id');
        $data['cand_details'] =  $this->core_model->get_cand_details($id);
        $this->load->view('profile2',$data);
        }
    }

    public function profile3()
    {
        $data['page_title']=$this->uri->segment('1');
        if ($this->session->userdata('name') == '') {
        redirect('login');
        } else {
        $data['id'] = $this->session->userdata('id');
        $id = $this->session->userdata('id');
        $data['cand_details'] =  $this->core_model->get_cand_details($id);
        $this->load->view('profile3',$data);
        }
    }

    public function profile4()
    {
        $data['page_title']=$this->uri->segment('1');
        if ($this->session->userdata('name') == '') {
        redirect('login');
        } else {
        $data['id'] = $this->session->userdata('id');
        $id = $this->session->userdata('id');
        $data['cand_details'] =  $this->core_model->get_cand_details($id);
        $this->load->view('profile4',$data);
        }
    }

    public function getState()
    {
        $this->core_model->getState();
    }
    public function getCity()
    {
        $this->core_model->getCity();
    }

    public function do_upload_resume() 
    {

      $config['upload_path'] = './upload/core_resume';
      $config['allowed_types'] = 'pdf|doc|docx';
      $config['max_size'] = '5000';
      $config['overwrite'] = TRUE;
      $resume = time().$_FILES['userfile']['name']; 
      $config['file_name'] = $resume;
      $this->load->library('upload', $config);
      $this->upload->initialize($config);
      if (!$this->upload->do_upload('userfile')) 
      {
          $error = array('error' => $this->upload->display_errors());  
          print_r($error);
          return FALSE;
      } 
      else
      {
          $data = array('upload_data' => $this->upload->data());
          return $resume;
      }

    }

    // public function cand_core_registration()
    // {

    //   $id = $this->session->userdata('id');
    //   $status = $this->session->userdata('status');
    //   $resume=$this->do_upload_resume();
    //   $query=$this->core_model->cand_core_registration($resume,$id);
    //   if($query)
    //   {
    //       $this->session->set_flashdata('profile','Your profile Completed Successfully......');
    //         redirect('home');
    //   }
    //   else
    //   {
    //       $this->session->set_flashdata('profilerr','Failed...Please Try Again...!');
    //       redirect('profile1');
    //   }
    // }

    public function submit_transac_id()
    {
      $id = $this->session->userdata('id');
      $query = $this->core_model->submit_transac_id($id);
      if($query)
      {
        redirect('index');
      }
      else
      {
        redirect('home');
      }
    }

    public function pr1_registration()
    {

      $id = $this->session->userdata('id');
      $status = $this->session->userdata('status');
      $query=$this->core_model->pr1_registration($id);
      if($query)
      {
            redirect('profile2');
      }
      else
      {
          redirect('profile1');
      }
    }

    public function pr2_registration()
    {

      $id = $this->session->userdata('id');
      $status = $this->session->userdata('status');
      $resume=$this->do_upload_resume();
      $query=$this->core_model->pr2_registration($id,$resume);
      if($query)
      {
            redirect('profile3');
      }
      else
      {
          redirect('profile2');
      }
    }

    public function pr3_registration()
    {

      $id = $this->session->userdata('id');
      $status = $this->session->userdata('status');
      $query=$this->core_model->pr3_registration($id);
      if($query)
      {
            redirect('profile4');
      }
      else
      {
          redirect('profile3');
      }
    }

    public function pr4_registration()
    {

      $id = $this->session->userdata('id');
      $status = $this->session->userdata('status');
      $query=$this->core_model->pr4_registration($id);
      if($query)
      {
            redirect('home');
      }
      else
      {
          redirect('profile4');
      }
    }

    
}
?>
