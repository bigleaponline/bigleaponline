<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title><?php echo $page_title; ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/style.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/colors.css">
<link rel="icon" href="<?php echo base_url(); ?>/assets/images/favicone.png">
</head>
<body>
<div id="wrapper"> 
  <header class="sticky-header">
    <div class="container">
      <div class="sixteen columns"> 
        <div id="logo">
          <h1><a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>/assets/images/logo.png" alt="Work Scout" /></a></h1>
        </div>
        <nav id="navigation" class="menu">
          <ul class="responsive float-right">
            <li><a href="<?php echo base_url('login'); ?>#tab2"><i class="fa fa-user"></i> Sign Up</a></li>
            <li><a href="<?php echo base_url('login'); ?>"><i class="fa fa-lock"></i> Log In</a></li>
          </ul>
        </nav>
        <!--<div id="mobile-navigation"> <a href="#menu" class="menu-trigger"><i class="fa fa-reorder"></i> Menu</a> </div>-->
      </div>
    </div>
  </header> 
  <div class="clearfix"></div>
  <div id="titlebar" class="single">
    <div class="container">
      <div class="sixteen columns">
        <h2>My Account</h2>
        <nav id="breadcrumbs">
          <ul>
            <li>You are here:</li>
            <li><a href="#">Home</a></li>
            <li>My Account</li>
          </ul>
        </nav>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="my-account">
      <ul class="tabs-nav">
        <li class=""><a href="#tab1">Login</a></li>
        <li><a href="#tab2">Register</a></li>
      </ul>
      <div class="tabs-container"> 
      <?php 
    //   if($this->session->flashdata('registration'))
    //   {
    //     echo '<div class="alert text-success" role="alert">'.$this->session->flashdata('registration').'</div>';   
    //   }
    if($this->session->flashdata('registration'))
        {
          echo '<div class="alert text-success" role="alert">'.$this->session->flashdata('registration').$this->session->flashdata('message').'</div>';
          echo '<div class="alert text-success" role="alert">'.$this->session->flashdata('message1').'</div>';
        }
      if($this->session->flashdata('registererr'))
      {
        echo '<div class="alert text-danger" role="alert">'.$this->session->flashdata('registererr').'</div>';   
      }
     if($this->session->flashdata('loginerror'))
      {
        echo '<div class="alert text-danger" role="alert">'.$this->session->flashdata('loginerror').'</div>';   
      }
      ?>
        <!-- Login -->
        <div class="tab-content" id="tab1" style="display: none;">
          <form method="post" class="login" action="<?php echo base_url();?>core_controller/login">
            <p class="form-row form-row-wide">
              <label for="username">Username: <i class="ln ln-icon-Male"></i>
                <input type="text" class="input-text" name="username" id="username" value="" placeholder="Enter your mail id"/>
              </label>
            </p>
            <p class="form-row form-row-wide">
              <label for="password">Password: <i class="ln ln-icon-Lock-2"></i>
                <input class="input-text" type="password" name="password" id="password"/>
              </label>
            </p>
            <p class="form-row">
              <input type="submit" class="button border fw margin-top-10" name="login" value="Login" />
              <label for="rememberme" class="rememberme">
                <input name="rememberme" type="checkbox" id="rememberme" value="forever" />
                Remember Me</label>
            </p>
            <p class="lost_password"> <a href="#" >Lost Your Password?</a> </p>
          </form>
        </div>
        <div class="tab-content" id="tab2" style="display: none;">
          <form method="post" class="register"  action="<?php echo base_url();?>core_controller/signup">
            <p class="form-row form-row-wide">
              <label for="username2">Name: <i class="ln ln-icon-Male"></i>
                <input type="text" class="input-text" name="name" id="name" value="" />
              </label>
            </p>
            
            <p class="form-row form-row-wide">
              <label for="email2">Email Address: <i class="ln ln-icon-Mail"></i>
                <input type="text" class="input-text" name="email" id="email" value="" />
              </label>
            </p>
             <p class="form-row form-row-wide">
              <label for="email2">Phone Number: <i class="ln ln-icon-Smartphone-3"></i>
                <input type="text" class="input-text" name="phone" id="phone" value="" onkeypress="return isNumberKey(event)"  maxlength="10"/>
              </label>
            </p>
            <p class="form-row form-row-wide">
              <label for="password1">Password: <i class="ln ln-icon-Lock-2"></i>
                <input class="input-text" type="password" name="password" id="password"/>
              </label>
            </p>
            <p class="form-row form-row-wide">
              <label for="password2">Repeat Password: <i class="ln ln-icon-Lock-2"></i>
                <input class="input-text" type="password" name="c_password" id="c_password"/>
              </label>
            </p>
            <p class="form-row">
              <input type="submit" class="button border fw margin-top-10" name="register" value="Register" />
            </p>
          </form>
        </div>
      </div>
    </div>
  </div>
  <div class="margin-top-30"></div>
  <script>function isNumberKey(evt)
{
   var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

         return true;
}
</script>
 <!-- <div id="footer">
    <div class="container">
      <div class="footer-bottom">
        <div class="sixteen columns">
          <h4>Follow Us</h4>
          <ul class="social-icons">
            <li><a class="facebook" href="#"><i class="icon-facebook"></i></a></li>
            <li><a class="twitter" href="#"><i class="icon-twitter"></i></a></li>
            <li><a class="gplus" href="#"><i class="icon-gplus"></i></a></li>
            <li><a class="linkedin" href="#"><i class="icon-linkedin"></i></a></li>
          </ul>
          <div class="copyrights">© Copyright 2020 by <a href="#">BigLeapOnline</a>. All Rights Reserved.</div>
        </div>
      </div>
    </div>
  </div>
  <div id="backtotop"><a href="#"></a></div>
</div>

<script src="scripts/jquery-3.4.1.min.js"></script> 
<script src="scripts/jquery-migrate-3.1.0.min.js"></script> 
<script src="scripts/custom.js"></script> 
<script src="scripts/jquery.superfish.js"></script> 
<script src="scripts/jquery.themepunch.tools.min.js"></script> 
<script src="scripts/jquery.themepunch.revolution.min.js"></script> 
<script src="scripts/jquery.themepunch.showbizpro.min.js"></script> 
<script src="scripts/jquery.flexslider-min.js"></script> 
<script src="scripts/chosen.jquery.min.js"></script> 
<script src="scripts/jquery.magnific-popup.min.js"></script> 
<script src="scripts/waypoints.min.js"></script> 
<script src="scripts/jquery.counterup.min.js"></script> 
<script src="scripts/jquery.jpanelmenu.js"></script> 
<script src="scripts/stacktable.js"></script> 
<script src="scripts/slick.min.js"></script> 
<script src="scripts/headroom.min.js"></script>
</body>
</html> -->