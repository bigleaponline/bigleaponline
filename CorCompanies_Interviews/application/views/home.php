<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>BigLeap-Core Company Pool Drive</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/style.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/colors.css">
<link rel="icon" href="<?php echo base_url(); ?>/assets/images/favicone.png">
</head>
<body>
<div id="wrapper"> 
  <header class="sticky-header">
    <div class="container">
      <div class="sixteen columns"> 
        <div id="logo">
          <h1><a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>/assets/images/logo.png" alt="Work Scout" /></a></h1>
        </div>
        <nav id="navigation" class="menu">
          <ul class="responsive float-right">
            <?php if($this->session->userdata('name') != ''){?>
            <li><a href="<?php echo base_url('logout'); ?>"><i class="fa fa-user"></i> Logout</a></li>
            
          <?php } else {?>
            <li><a href="<?php echo base_url('login'); ?>#tab2"><i class="fa fa-user"></i> Sign Up</a></li>
            <li><a href="<?php echo base_url('login'); ?>"><i class="fa fa-lock"></i> Log In</a></li>
          <?php } ?>
          </ul>
        </nav>
        <div id="mobile-navigation"> <a href="#menu" class="menu-trigger"><i class="fa fa-reorder"></i> Menu</a> </div>
      </div>
    </div>
  </header>
  <div class="clearfix"></div>
  <div id="titlebar" class="single">
    <div class="container">
      <div class="sixteen columns">
        <h2>My Dashboard</h2>
        <nav id="breadcrumbs">
          <ul>
            <li>You are here:</li>
            <li><a href="#">Home</a></li>
            <li>My Dashboard</li>
          </ul>
        </nav>
      </div>
    </div>
  </div>
  <div class="container">
    <?php 
    
    if($transac_id){
    foreach ($transac_id as $u) {
     $t_id = $u['transac_id'];
    }

    if($t_id == ''){?>
    <p class="reg-para">Your registration will be completed by making the payment and entering the bank Transaction ID.</p>
    <div class="my-account">     
      <div class="tabs-container"> 
        <div class="tab-content" id="tab1" style="display: none;">
          <form method="post" class="login" action="<?php echo base_url();?>core_controller/submit_transac_id">
            <p class="form-row form-row-wide">
              <label for="username" style="text-align: center">Enter Your UPI/UTR Transaction ID <i class="ln ln-icon-ID-3"></i>
                <input type="text" class="input-text" name="transac_id" id="transac_id" value="" placeholder="For example: 007614007691" onkeypress="return isNumberKey(event)" required/>
              </label>
            </p>
            <p class="form-row">
              <input type="submit" class="button border fw margin-top-10" name="login" value="Complete Your Registration" />
            </p>
          </form>
        </div>
      </div>
    </div>
    <?php } elseif($status == 1 && $t_id != ''){?>
    <p class="reg-para" style="text-align: center; font-weight: 900;">Your registration is confirmed !! <br> Best Wishes !!</p>
    <div class="my-account">     
    </div>
    <?php } elseif($t_id != ''){?>
      <p class="reg-para">It may take few hours to reflect the registration amount in your account following which your registration will be confirmed.</p>
    <div class="my-account">     
                                                   
    </div>
  <?php } } ?>
    
  </div>
  <div class="margin-top-30"></div>
  <script>
      function isNumberKey(evt)
{
   var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

         return true;
}
  </script>
