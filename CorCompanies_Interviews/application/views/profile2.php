<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title><?php echo $page_title; ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/style.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/colors.css">
<link rel="icon" href="<?php echo base_url(); ?>/assets/images/favicone.png">
</head>

<body>
  <?php 
  foreach($cand_details as $details)
  {

  }
  ?>
<div id="wrapper"> 
  <header class="sticky-header">
    <div class="container">
      <div class="sixteen columns"> 
        <div id="logo">
          <h1><a href="<?php echo base_url('index'); ?>"><img src="<?php echo base_url(); ?>/assets/images/logo.png" alt="Work Scout" /></a></h1>
        </div>
        <nav id="navigation" class="menu">
          <ul class="responsive float-right">
            <?php if($this->session->userdata('name') != ''){?>
            <li><a href="<?php echo base_url('logout'); ?>"><i class="fa fa-user"></i> Logout</a></li>
            
          <?php } else {?>
            <li><a href="<?php echo base_url('login'); ?>#tab2"><i class="fa fa-user"></i> Sign Up</a></li>
            <li><a href="<?php echo base_url('login'); ?>"><i class="fa fa-lock"></i> Log In</a></li>
          <?php } ?>
          </ul>
        </nav>
       <!-- <div id="mobile-navigation"> <a href="#menu" class="menu-trigger"><i class="fa fa-reorder"></i> Menu</a> </div>-->
      </div>
    </div>
  </header>
    <div class="clearfix"></div>
  <div id="titlebar" class="single">
    <div class="container">
      <div class="sixteen columns">
        <h2>My Account</h2>
        <nav id="breadcrumbs">
          <ul>
            <li>You are here:</li>
            <li><a href="#">Home</a></li>
            <li>My Account</li>
            <li>Profile Create</li>
          </ul>
        </nav>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="my-account-progress">
<div class="row">
    <div class="col-md-12">
        <form id="msform" action="<?php echo base_url();?>core_controller/pr2_registration" method="post" enctype="multipart/form-data">
            <ul id="progressbar">
              <li>personal details</li>
              <li class="active">TRIP Information</li>
                <li>Physical and Financial Status</li>
                <li>Submit</li>
            </ul>
            
            <fieldset>
                <h2 class="fs-title">educational details</h2>
                <!--<h3 class="fs-subtitle">Your presence on the social network</h3>-->
                <div class="row">
                  <div class="col-md-3">
                    <label for="fname">Post Graduation</label>
                <input class="form-control" type="hidden" name="qualfcn_catgry[]" id="qualfcn_catgry_1" value="PG"  readonly="readonly" />
                <input class="form-control" type="text" name="course[]" id="course_1" placeholder="Enter Your Graduation">
                  </div>
                  <div class="col-md-3">
                    <label for="fname">Stream</label>
                     <select name="subject[]" id="subject_1" class="form-input">
                      <option value="hide">Choose Stream</option>
                       <option value="Applied Electronics and Instrumentation Engineering ">Applied Electronics and Instrumentation Engineering </option>
                      <!--<option value="Aeronautical Engineering ">Aeronautical Engineering </option>-->
                      <!--<option value="Chemical Engineering">Chemical Engineering</option>-->
                      <option value="Civil Engineering">Civil Engineering </option>
                      <!--<option value="Computer Science Engineering">Computer Science Engineering</option>-->
                      <option value="Electrical and Electronics Engineering">Electrical and Electronics Engineering</option>
                      <option value="Electronics and Communication Engineering">Electronics and Communication Engineering</option>
                      <!--<option value="Electronics and Instrumentation Engineering">Electronics and Instrumentation Engineering</option>-->
                      <!--<option value="Information Technology">Information Technology</option>-->
                      <!--<option value="Marine Engineering">Marine Engineering</option>-->
                      <option value="Mechanical Engineering">Mechanical Engineering</option>
                      <option value=" Mechatronics Engineering">Mechatronics Engineering</option>
                      <!--<option value=" Mechatronics Engineering">Others</option>-->
                    </select>
                  </div>
                  <div class="col-md-3">
                    <label for="fname">University/College</label>
                   <input class="form-control" type="text" name="college[]" id="college_1" placeholder="Enter Your College">
                  </div>
                  <div class="col-md-2">
                    <label for="fname">Year Of Passing</label>
                     <select name="pass_year[]" id="pass_year_1" class="form-input">
                      <option value="hide">Passing Year</option>
                      <?php for ($year=1900; $year <= 2021; $year++){ ?>
                      <option value="<?php echo $year;?>"><?php echo $year;?></option>
                      <?php }; ?>
                    </select>
                  </div>
                  <div class="col-md-1">
                    <label for="fname">Marks</label>
                    <input class="form-control" type="text" name="mark[]" id="mark_1" placeholder="50%" onkeypress="return isNumberKey(event)"  maxlength="2">
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-3">
                    <label for="fname">Graduation</label>
                <input class="form-control" type="hidden" name="qualfcn_catgry[]" id="qualfcn_catgry_2" value="UG"  readonly="readonly" />
                <input class="form-control" type="text" name="course[]" id="course_2" placeholder="Enter Your Graduation">
                  </div>
                  <div class="col-md-3">
                    <label for="fname">Stream</label>
                     <select name="subject[]" id="subject_2" class="form-input">
                       <option value="hide">Choose Stream</option>
                       <option value="Applied Electronics and Instrumentation Engineering ">Applied Electronics and Instrumentation Engineering </option>
                      <!--<option value="Aeronautical Engineering ">Aeronautical Engineering </option>-->
                      <!--<option value="Chemical Engineering">Chemical Engineering</option>-->
                      <option value="Civil Engineering">Civil Engineering </option>
                      <!--<option value="Computer Science Engineering">Computer Science Engineering</option>-->
                      <option value="Electrical and Electronics Engineering">Electrical and Electronics Engineering</option>
                      <option value="Electronics and Communication Engineering">Electronics and Communication Engineering</option>
                      <!--<option value="Electronics and Instrumentation Engineering">Electronics and Instrumentation Engineering</option>-->
                      <!--<option value="Information Technology">Information Technology</option>-->
                      <!--<option value="Marine Engineering">Marine Engineering</option>-->
                      <option value="Mechanical Engineering">Mechanical Engineering</option>
                      <option value=" Mechatronics Engineering">Mechatronics Engineering</option>
                      <!--<option value=" Mechatronics Engineering">Others</option>-->
                    </select>
                  </div>
                  <div class="col-md-3">
                    <label for="fname">University/College</label>
                    <input class="form-control" type="text" name="college[]" id="college_2" placeholder="Enter Your College">
                  </div>
                  <div class="col-md-2">
                    <label for="fname">Year Of Passing</label>
                     <select name="pass_year[]" id="pass_year_2" class="form-input">
                      <option value="hide">Passing Year</option>
                      <?php for ($year=1900; $year <= 2021; $year++){ ?>
                      <option value="<?php echo $year;?>"><?php echo $year;?></option>
                      <?php }; ?>
                    </select>
                  </div>
                  <div class="col-md-1">
                    <label for="fname">Marks</label>
                    <input class="form-control" type="text" name="mark[]" id="mark_2" placeholder="50%" onkeypress="return isNumberKey(event)"  maxlength="2">
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-3">
                    <label for="fname">Diploma</label>
                <input class="form-control" type="hidden" name="qualfcn_catgry[]" id="qualfcn_catgry_3" value="Diploma"  readonly="readonly" />
                <input class="form-control" type="text" name="course[]" id="course_3" placeholder="Enter Your Diploma">
                  </div>
                  <div class="col-md-3">
                    <label for="fname">Stream</label>
                     <select name="subject[]" id="subject_3" class="form-input">
                      <option value="hide">Choose Stream</option>
                       <option value="Applied Electronics and Instrumentation Engineering ">Applied Electronics and Instrumentation Engineering </option>
                      <!--<option value="Aeronautical Engineering ">Aeronautical Engineering </option>-->
                      <!--<option value="Chemical Engineering">Chemical Engineering</option>-->
                      <option value="Civil Engineering">Civil Engineering </option>
                      <!--<option value="Computer Science Engineering">Computer Science Engineering</option>-->
                      <option value="Electrical and Electronics Engineering">Electrical and Electronics Engineering</option>
                      <option value="Electronics and Communication Engineering">Electronics and Communication Engineering</option>
                      <!--<option value="Electronics and Instrumentation Engineering">Electronics and Instrumentation Engineering</option>-->
                      <!--<option value="Information Technology">Information Technology</option>-->
                      <!--<option value="Marine Engineering">Marine Engineering</option>-->
                      <option value="Mechanical Engineering">Mechanical Engineering</option>
                      <option value=" Mechatronics Engineering">Mechatronics Engineering</option>
                      <!--<option value=" Mechatronics Engineering">Others</option>-->
                    </select>
                  </div>
                  <div class="col-md-3">
                    <label for="fname">University/College</label>
                    <input class="form-control" type="text" name="college[]" id="college_3" placeholder="Enter Your College">
                  </div>
                  <div class="col-md-2">
                    <label for="fname">Year Of Passing</label>
                     <select name="pass_year[]" id="pass_year_3" class="form-input">
                      <option value="hide">Passing Year</option>
                      <?php for ($year=1900; $year <= 2021; $year++){ ?>
                      <option value="<?php echo $year;?>"><?php echo $year;?></option>
                      <?php }; ?>
                    </select>
                  </div>
                  <div class="col-md-1">
                    <label for="fname">Marks</label>
                    <input class="form-control" type="text" name="mark[]" id="mark_3" placeholder="50%" onkeypress="return isNumberKey(event)"  maxlength="2">
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-3">
                    <label for="fname">PlusTwo/ITI</label>
                    <input class="form-control" type="hidden" name="qualfcn_catgry[]" id="qualfcn_catgry_4" value="HSE"  readonly="readonly" />
                    <input class="form-control" type="text" name="course[]" id="course_4" placeholder="Enter Your Course">
                  </div>
                  <div class="col-md-3">
                    <label for="fname">Board</label>
                     <select name="subject[]" id="subject_4" class="form-control">
                      <option value="hide">Board</option>
                      <option value="STATE">STATE</option>
                      <option value="CBSE">CBSE</option>
                    </select>
                  </div>
   
                  <div class="col-md-3">
                    <label for="fname">Year Of Passing</label>
                     <select name="pass_year[]" id="pass_year_4" class="form-control">
                <option value="hide">Passing Year</option>
                 <?php for ($year=1900; $year <= 2021; $year++){ ?>
                <option value="<?php echo $year;?>"><?php echo $year;?></option>
                <?php }; ?>
              </select>
                  </div>
                  <div class="col-md-3">
                    <label for="fname">Marks</label>
                    <input class="form-control wow fadeInUp" type="text" name="mark[]" id="mark_4" placeholder="50%" onkeypress="return isNumberKey(event)"  maxlength="2">
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-3">
                    <label for="fname">SSLC</label>
                    <input class="form-control" type="hidden" name="qualfcn_catgry[]" id="qualfcn_catgry_5" value="HSE"  readonly="readonly" />
                    <input class="form-control" type="text" name="course[]" id="course_5" placeholder="Enter Your Course">
                  </div>
                  <div class="col-md-3">
                    <label for="fname">Board</label>
                     <select name="subject[]" id="subject_5" class="form-control">
                      <option value="hide">Board</option>
                      <option value="STATE">STATE</option>
                      <option value="CBSE">CBSE</option>
                    </select>
                  </div>
   
                  <div class="col-md-3">
                    <label for="fname">Year Of Passing</label>
                     <select name="pass_year[]" id="pass_year_5" class="form-control">
                <option value="hide">Passing Year</option>
                 <?php for ($year=1900; $year <= 2021; $year++){ ?>
                <option value="<?php echo $year;?>"><?php echo $year;?></option>
                <?php }; ?>
              </select>
                  </div>
                  <div class="col-md-3">
                    <label for="fname">Marks</label>
                    <input class="form-control wow fadeInUp" type="text" name="mark[]" id="mark_5" placeholder="50%" onkeypress="return isNumberKey(event)"  maxlength="2">
                  </div>
                </div>
                <div class="row txtlft">
                  <div class="col-md-12 col-sm-12 col-lg-12">
 <div class="main-wrapper">
     <h4>Upload your Resume *(txt or pdf file.)</h4>
        <div class="upload-main-wrapper">
            
                <div class="upload-wrapper">
                    
                        <input type="file" id="upload-file" name="userfile">
                        <svg version="1.1" xmlns:xlink="" preserveAspectRatio="xMidYMid meet" viewBox="224.3881704980842 176.8527621722847 221.13266283524905 178.8472378277154" width="221.13" height="178.85"><defs><path d="M357.38 176.85C386.18 176.85 409.53 204.24 409.53 238.02C409.53 239.29 409.5 240.56 409.42 241.81C430.23 246.95 445.52 264.16 445.52 284.59C445.52 284.59 445.52 284.59 445.52 284.59C445.52 309.08 423.56 328.94 396.47 328.94C384.17 328.94 285.74 328.94 273.44 328.94C246.35 328.94 224.39 309.08 224.39 284.59C224.39 284.59 224.39 284.59 224.39 284.59C224.39 263.24 241.08 245.41 263.31 241.2C265.3 218.05 281.96 199.98 302.22 199.98C306.67 199.98 310.94 200.85 314.93 202.46C324.4 186.96 339.88 176.85 357.38 176.85Z" id="b1aO7LLtdW"></path><path d="M306.46 297.6L339.79 297.6L373.13 297.6L339.79 255.94L306.46 297.6Z" id="c4SXvvMdYD"></path><path d="M350.79 293.05L328.79 293.05L328.79 355.7L350.79 355.7L350.79 293.05Z" id="b11si2zUk"></path></defs><g><g><g><use xlink:href="#b1aO7LLtdW" opacity="1" fill="#ffffff" fill-opacity="1"></use></g><g><g><use xlink:href="#c4SXvvMdYD" opacity="1" fill="#363535" fill-opacity="1"></use></g><g><use xlink:href="#b11si2zUk" opacity="1" fill="#363535" fill-opacity="1"></use></g></g></g></g></svg>
                        <span class="file-upload-text">Upload File</span>
                        <div class="file-success-text">
                         <svg version="1.1" id="check" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                    viewBox="0 0 100 100"  xml:space="preserve">
                <circle style="fill:rgba(0,0,0,0);stroke:#ffffff;stroke-width:10;stroke-miterlimit:10;" cx="49.799" cy="49.746" r="44.757"/>
                <polyline style="fill:rgba(0,0,0,0);stroke:#ffffff;stroke-width:10;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;" points="
                    27.114,51 41.402,65.288 72.485,34.205 "/>
                </svg> <span>Successfully</span></div>
                    </div>
                    <p id="file-upload-name"></p>
        </div>
    </div>
                  </div>
                </div>
                <!-- <input type="button" name="previous" class="previous action-button-previous" value="Previous"/> -->
                <!-- <input type="button" name="next" class="next action-button" value="Next"/> -->
                 <button type="submit" name="submit" class="btn btn-default action-button">Next</button>
            </fieldset>
          
          
        </form>
        <!-- link to designify.me code snippets -->
        <!-- /.link to designify.me code snippets -->
    </div>
</div>
    </div>
  </div>
  <div class="margin-top-30"></div>
  <div id="footer">
    <div class="container">
      <div class="footer-bottom">
        <div class="sixteen columns">
          <h4>Follow Us</h4>
          <ul class="social-icons">
            <li><a class="facebook" href="#"><i class="icon-facebook"></i></a></li>
            <li><a class="twitter" href="#"><i class="icon-twitter"></i></a></li>
            <li><a class="gplus" href="#"><i class="icon-gplus"></i></a></li>
            <li><a class="linkedin" href="#"><i class="icon-linkedin"></i></a></li>
          </ul>
          <div class="copyrights">© Copyright 2020 by <a href="#">BigLeapOnline</a>. All Rights Reserved.</div>
        </div>
      </div>
    </div>
  </div>
  <div id="backtotop"><a href="#"></a></div>
</div>

<script src="<?php echo base_url(); ?>/assets/scripts/jquery-3.4.1.min.js"></script> 
<script src="<?php echo base_url(); ?>/assets/scripts/jquery-migrate-3.1.0.min.js"></script> 
<script src="<?php echo base_url(); ?>/assets/scripts/custom.js"></script> 
<script src="<?php echo base_url(); ?>/assets/scripts/jquery.superfish.js"></script> 
<script src="<?php echo base_url(); ?>/assets/scripts/jquery.themepunch.tools.min.js"></script> 
<script src="<?php echo base_url(); ?>/assets/scripts/jquery.themepunch.revolution.min.js"></script> 
<script src="<?php echo base_url(); ?>/assets/scripts/jquery.themepunch.showbizpro.min.js"></script> 
<script src="<?php echo base_url(); ?>/assets/scripts/jquery.flexslider-min.js"></script> 
<script src="<?php echo base_url(); ?>/assets/scripts/chosen.jquery.min.js"></script> 
<script src="<?php echo base_url(); ?>/assets/scripts/jquery.magnific-popup.min.js"></script> 
<script src="<?php echo base_url(); ?>/assets/scripts/waypoints.min.js"></script> 
<script src="<?php echo base_url(); ?>/assets/scripts/jquery.counterup.min.js"></script> 
<script src="<?php echo base_url(); ?>/assets/scripts/jquery.jpanelmenu.js"></script> 
<script src="<?php echo base_url(); ?>/assets/scripts/stacktable.js"></script> 
<script src="<?php echo base_url(); ?>/assets/scripts/slick.min.js"></script> 
<script src="<?php echo base_url(); ?>/assets/scripts/headroom.min.js"></script>
<script src="<?php echo base_url(); ?>/assets/scripts/progress2.js"></script>
<script src="<?php echo base_url(); ?>/assets/scripts/progress.js"></script>
<script>
$(document).ready(function() {
  $('.datepicker').pickadate({
    formatSubmit: 'dd-mm-yyyy',
    min: [2016,12,17],
    max: [2017,01,15],
    closeOnSelect: false,
    closeOnClear: false,
  });
  $('.date-field').autotab('number');
  
  $('.single-date-field').mask('00/00/0000', 
    {placeholder: "_ _ /_ _ /_ _ _ _"});
}); 
</script>
<script>
$(document).ready(function(){
             $('#upload-file').change(function() {
                var filename = $(this).val();
                $('#file-upload-name').html(filename);
                if(filename!=""){
                    setTimeout(function(){
                        $('.upload-wrapper').addClass("uploaded");
                    }, 600);
                    setTimeout(function(){
                        $('.upload-wrapper').removeClass("uploaded");
                        $('.upload-wrapper').addClass("success");
                    }, 1600);
                }
            });
        });
  </script>
  <script>
function getState(val) 
 {

  $.ajax({
          type: "POST",
          url: "<?php echo base_url();?>core_controller/getState",
          data:'country_id='+val,
          success: function(data)
          {
            $("#state").html(data);
            getCity();
          }
  });
 }
 function getCity(val) 
 {
  $.ajax({
          type: "POST",
          url: "<?php echo base_url();?>core_controller/getCity",
          data:'state_id='+val,
          success: function(data)
          {
            $("#city").html(data);
          }
      });
 }

function isNumberKey(evt)
{
   var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

         return true;
}
</script>
</body>
</html>