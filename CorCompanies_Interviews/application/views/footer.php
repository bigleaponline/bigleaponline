  <div id="footer">
    <div class="container">
      <div class="footer-bottom">
        <div class="sixteen columns">
          <h4>Follow Us</h4>
          <ul class="social-icons">
            <li><a class="facebook" href="#"><i class="icon-facebook"></i></a></li>
            <li><a class="twitter" href="#"><i class="icon-twitter"></i></a></li>
            <li><a class="gplus" href="#"><i class="icon-gplus"></i></a></li>
            <li><a class="linkedin" href="#"><i class="icon-linkedin"></i></a></li>
          </ul>
          <div class="copyrights">©  Copyright 2020 by <a href="#">BigLeapOnline</a>. All Rights Reserved.</div>
        </div>
      </div>
    </div>
  </div>
  <div id="backtotop"><a href="#"></a></div>
</div>

<script src="<?php echo base_url(); ?>/assets/scripts/jquery-3.4.1.min.js"></script> 
<script src="<?php echo base_url(); ?>/assets/scripts/jquery-migrate-3.1.0.min.js"></script> 
<script src="<?php echo base_url(); ?>/assets/scripts/custom.js"></script> 
<script src="<?php echo base_url(); ?>/assets/scripts/jquery.superfish.js"></script> 
<script src="<?php echo base_url(); ?>/assets/scripts/jquery.themepunch.tools.min.js"></script> 
<script src="<?php echo base_url(); ?>/assets/scripts/jquery.themepunch.revolution.min.js"></script> 
<script src="<?php echo base_url(); ?>/assets/scripts/jquery.themepunch.showbizpro.min.js"></script> 
<script src="<?php echo base_url(); ?>/assets/scripts/jquery.flexslider-min.js"></script> 
<script src="<?php echo base_url(); ?>/assets/scripts/chosen.jquery.min.js"></script> 
<script src="<?php echo base_url(); ?>/assets/scripts/jquery.magnific-popup.min.js"></script> 
<script src="<?php echo base_url(); ?>/assets/scripts/waypoints.min.js"></script> 
<script src="<?php echo base_url(); ?>/assets/scripts/jquery.counterup.min.js"></script> 
<script src="<?php echo base_url(); ?>/assets/scripts/jquery.jpanelmenu.js"></script> 
<script src="<?php echo base_url(); ?>/assets/scripts/stacktable.js"></script> 
<script src="<?php echo base_url(); ?>/assets/scripts/slick.min.js"></script> 
<script src="<?php echo base_url(); ?>/assets/scripts/headroom.min.js"></script>
</body>
</html>