<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title><?php echo $page_title; ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/style.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/colors.css">
<link rel="icon" href="<?php echo base_url(); ?>/assets/images/favicone.png">
</head>
<body>
<div id="wrapper"> 
  <header class="transparent sticky-header">
    <div class="container">
      <div class="sixteen columns"> 
        <div id="logo">
          <h1><a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>/assets/images/logo.png" alt="Work Scout" /></a></h1>
        </div>
        <nav id="navigation" class="menu">
          <ul class="float-right">
            <?php if($this->session->userdata('name') != ''){?>
            <li><a href="<?php echo base_url('logout'); ?>"><i class="fa fa-user"></i> Logout</a></li>
            
          <?php } else {?>
            <li><a href="<?php echo base_url('login'); ?>#tab2"><i class="fa fa-user"></i> Sign Up</a></li>
            <li><a href="<?php echo base_url('login'); ?>"><i class="fa fa-lock"></i> Log In</a></li>
          <?php } ?>
          </ul>
        </nav>
       <!-- <div id="mobile-navigation"> <a href="#menu" class="menu-trigger"><i class="fa fa-reorder"></i></a> </div>-->
      </div>
    </div>
  </header>