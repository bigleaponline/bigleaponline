
  <div class="clearfix"></div>
  <div id="banner" class="with-transparent-header parallax background" style="background-image: url(http://bigleaponline.com/CorCompanies_Interviews/assets/images/banner-home-02.jpg)" data-img-width="2000" data-img-height="1330" data-diff="300">
    <div class="container">
      <div class="sixteen columns">
        <div class="search-container"> 
        <h1>Attn. Core Company Career Aspirants:</h1>
          <!--<h2>Find Job</h2>-->
          <!--<input type="text" class="ico-01" placeholder="job title, keywords or company name" value=""/>-->
          <!--<input type="text" class="ico-02" placeholder="city, province or region" value=""/>-->
          <!--<button><i class="fa fa-search"></i></button>-->
          <!--<div class="browse-jobs">Core Companies (Mechanical, Civil & Electrical and Electronics) are hiring through us this summer.</div>-->
        </div>
      </div>
    </div>
  </div>
  <div class="cor-abt">
    <div class="container">
      <div class="row">
        <div class="col-md-7">
          <h3 class="margin-bottom-20">Core Companies (Mechanical, Civil, Electrical and Electronics) are hiring through us this summer.</h3>
          <p>Are you a 2020/2019 passing out core company job aspirant?<br> We have 40+ companies from across multiple locations in South India hiring Mechanical, Civil, Electronics and Electrical engineers(BE, BTech and Diploma) this March and April. Recruitment drives will be conducted in Trivandrum, Kottayam, Ernakulam, Thrissur and Calicut.</p>
          <ul>
            <li><i class="fa fa-long-arrow-right" aria-hidden="true"></i>First event to be conducted in Calicut at BigLeap office </li>
            <li><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Only 500 candidates(first-come-first-serve) will be selected for this event and to avoid crowd interviews will be scheduled in different time slots in order to manage rush.</li>
            <li><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Minimum 10 companies from each sector will be recruiting. </li>
            <li><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Candidates should pay a fee of Rs.200 + GST (Total Rs.236) for confirming participation immediately. (<a href="#bank-details" style="color: #0a49fb;">Bank Details</a>)</li>
            <li><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Registration will be completed by entering the Transaction ID</li>
          </ul>
          <p>For Further details please call us @ +91 9744642227, +91 9744642224</p>
        </div>
        <div class="col-md-5">
          <div class="abt-cor"><img src="assets/images/abt-banner.png" class=""></div>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="sixteen columns">
      <h3 class="margin-bottom-20 margin-top-10">Popular Categories</h3>
      <div class="categories-boxes-container"> 
        <a href="#" class="category-small-box"> <i class="ln ln-icon-Bar-Chart"></i>
        <h4>Mechanical Engineering Jobs</h4>
        <span>32</span> </a> 
        <a href="#" class="category-small-box"> <i class="ln ln-icon-Car"></i>
        <h4>Civil Engineering Jobs</h4>
        <span>76</span> </a> 
        <a href="#" class="category-small-box"> <i class="ln  ln-icon-Worker"></i>
        <h4>Electrical Engineering Jobs</h4>
        <span>31</span> </a>  
        <a href="#" class="category-small-box"> <i class="ln  ln-icon-Student-Female"></i>
        <h4>Electronics Engineering Jobs</h4>
        <span>76</span> </a> </div>
      <div class="clearfix"></div>
      <div class="margin-top-30"></div>
      <a href="#" class="button centered">Browse All Categories</a>
      <div class="margin-bottom-55"></div>
    </div>
  </div>
  <div class="container"> 
    <div class="eleven columns joblist-sect">
      <!-- <div class="padding-right">
        <h3 class="margin-bottom-25">Recent Jobs</h3>
        <div class="listings-container"> 
          <a href="job-page-alt.html" class="listing full-time">
          <div class="listing-logo"> <img src="<?php echo base_url(); ?>/assets/images/job-list-logo-01.png" alt=""> </div>
          <div class="listing-title">
            <h4>Marketing Coordinator - SEO / SEM Experience <span class="listing-type">Full-Time</span></h4>
            <ul class="listing-icons">
              <li><i class="ln ln-icon-Management"></i> King</li>
              <li><i class="ln ln-icon-Map2"></i> 7th Avenue, New York, NY, United States</li>
              <li><i class="ln ln-icon-Money-2"></i> $5000 - $7000</li>
              <li>
                <div class="listing-date new">new</div>
              </li>
            </ul>
          </div>
          </a> 
          <a href="job-page.html" class="listing part-time">
          <div class="listing-logo"> <img src="<?php echo base_url(); ?>/assets/images/job-list-logo-02.png" alt=""> </div>
          <div class="listing-title">
            <h4>Core PHP Developer for Site Maintenance <span class="listing-type">Full-Time</span></h4>
            <ul class="listing-icons">
              <li><i class="ln ln-icon-Management"></i> Cubico</li>
              <li><i class="ln ln-icon-Map2"></i> Sydney</li>
              <li><i class="ln ln-icon-Money-2"></i> $125 / hour</li>
              <li>
                <div class="listing-date">3d ago</div>
              </li>
            </ul>
          </div>
          </a> 
          <a href="job-page-alt.html" class="listing full-time">
          <div class="listing-logo"> <img src="<?php echo base_url(); ?>/assets/images/job-list-logo-01.png" alt=""> </div>
          <div class="listing-title">
            <h4>Restaurant Team Member - Crew <span class="listing-type">Full-Time</span></h4>
            <ul class="listing-icons">
              <li><i class="ln ln-icon-Management"></i> King</li>
              <li><i class="ln ln-icon-Map2"></i> Sydney</li>
              <li>
                <div class="listing-date">3d ago</div>
              </li>
            </ul>
          </div>
          </a> 
          <a href="job-page.html" class="listing internship">
          <div class="listing-logo"> <img src="<?php echo base_url(); ?>/assets/images/job-list-logo-04.png" alt=""> </div>
          <div class="listing-title">
            <h4>Power Systems User Experience Designer <span class="listing-type">Full-Time</span></h4>
            <ul class="listing-icons">
              <li><i class="ln ln-icon-Management"></i> Hexagon</li>
              <li><i class="ln ln-icon-Map2"></i> London</li>
              <li><i class="ln ln-icon-Money-2"></i> $55 / hour</li>
              <li>
                <div class="listing-date">4d ago</div>
              </li>
            </ul>
          </div>
          </a> 
          <a href="job-page.html" class="listing freelance">
          <div class="listing-logo"> <img src="<?php echo base_url(); ?>/assets/images/job-list-logo-05.png" alt=""> </div>
          <div class="listing-title">
            <h4>iPhone / Android Music App Development <span class="listing-type">Full-Time</span></h4>
            <ul class="listing-icons">
              <li><i class="ln ln-icon-Management"></i> Hexagon</li>
              <li><i class="ln ln-icon-Map2"></i> London</li>
              <li><i class="ln ln-icon-Money-2"></i> $85 / hour</li>
              <li>
                <div class="listing-date">4d ago</div>
              </li>
            </ul>
          </div>
          </a> </div>
        <a href="browse-jobs.html" class="button centered"><i class="fa fa-plus-circle"></i> Show More Jobs</a>
        <div class="margin-bottom-55"></div>
      </div> -->
    </div> 
  </div>
    <div class="bank-details" id="bank-details">
  	<div class="container">
  		<div class="row">
  		<div class="bank-sect">
  			<h2>bank details</h2>
  			<p>BigLeap Solutions Pvt Ltd</p>
  			<p>united bank of india, calicut branch</p>
  			<p>A/c No - 1424050003506</p>
  			<p>IFSC - UTBI0KOZ831</p>
  		</div>
  		</div>
  	</div>
  </div>
  <a href="#" class="flip-banner" data-background="<?php echo base_url(); ?>/assets/images/all-categories-photo.jpg" data-color="#de1f26" data-color-opacity="0.9">
  <div class="flip-banner-content">
    <h2 class="flip-visible">Call Now for Registration !!</h2>
    <h2 class="flip-hidden">+91 9744642227, +91 9744642224<i class="fa fa-angle-right"></i></h2>
  </div>
  </a>
