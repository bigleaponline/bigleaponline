<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title><?php echo $page_title; ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/style.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/colors.css">
<link rel="icon" href="<?php echo base_url(); ?>/assets/images/favicone.png">
</head>

<body>
  <?php 
  foreach($cand_details as $details)
  {

  }
  ?>
<div id="wrapper"> 
  <header class="sticky-header">
    <div class="container">
      <div class="sixteen columns"> 
        <div id="logo">
          <h1><a href="<?php echo base_url('index'); ?>"><img src="<?php echo base_url(); ?>/assets/images/logo.png" alt="Work Scout" /></a></h1>
        </div>
        <nav id="navigation" class="menu">
          <ul class="responsive float-right">
            <?php if($this->session->userdata('name') != ''){?>
            <li><a href="<?php echo base_url('logout'); ?>"><i class="fa fa-user"></i> Logout</a></li>
            
          <?php } else {?>
            <li><a href="<?php echo base_url('login'); ?>#tab2"><i class="fa fa-user"></i> Sign Up</a></li>
            <li><a href="<?php echo base_url('login'); ?>"><i class="fa fa-lock"></i> Log In</a></li>
          <?php } ?>
          </ul>
        </nav>
       <!-- <div id="mobile-navigation"> <a href="#menu" class="menu-trigger"><i class="fa fa-reorder"></i> Menu</a> </div>-->
      </div>
    </div>
  </header>
    <div class="clearfix"></div>
  <div id="titlebar" class="single">
    <div class="container">
      <div class="sixteen columns">
        <h2>My Account</h2>
        <nav id="breadcrumbs">
          <ul>
            <li>You are here:</li>
            <li><a href="#">Home</a></li>
            <li>My Account</li>
            <li>Profile Create</li>
          </ul>
        </nav>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="my-account-progress">
<div class="row">
    <div class="col-md-12">
        <form id="msform" action="<?php echo base_url();?>core_controller/pr4_registration" method="post" enctype="multipart/form-data">
            <ul id="progressbar">
              <li>personal details</li>
              <li>TRIP Information</li>
                <li>Physical and Financial Status</li>
                <li class="active">Submit</li>
            </ul>

          
            <fieldset>
                <h2 class="fs-title">Payment Options</h2>
                <h3 class="fs-subtitle">Fill in your credentials to authorize submission</h3>

                <label for="fname">UPI/UTR Transaction ID (# ID acquired from bank once payment has done)</label>
                <input type="text" name="transac_id" placeholder="For example: 007614007691" onkeypress="return isNumberKey(event)"/>
                <!-- <input type="button" name="previous" class="previous action-button-previous" value="Previous"/> -->
                <!-- <input type="submit" name="submit" class="submit action-button" value="Submit"/> -->
                <button type="submit" name="submit" class="btn btn-default action-button">Submit</button>
            </fieldset>
        </form>
        <!-- link to designify.me code snippets -->
        <!-- /.link to designify.me code snippets -->
    </div>
</div>
    </div>
  </div>
  <div class="margin-top-30"></div>
  <div id="footer">
    <div class="container">
      <div class="footer-bottom">
        <div class="sixteen columns">
          <h4>Follow Us</h4>
          <ul class="social-icons">
            <li><a class="facebook" href="#"><i class="icon-facebook"></i></a></li>
            <li><a class="twitter" href="#"><i class="icon-twitter"></i></a></li>
            <li><a class="gplus" href="#"><i class="icon-gplus"></i></a></li>
            <li><a class="linkedin" href="#"><i class="icon-linkedin"></i></a></li>
          </ul>
          <div class="copyrights">© Copyright 2020 by <a href="#">BigLeapOnline</a>. All Rights Reserved.</div>
        </div>
      </div>
    </div>
  </div>
  <div id="backtotop"><a href="#"></a></div>
</div>

<script src="<?php echo base_url(); ?>/assets/scripts/jquery-3.4.1.min.js"></script> 
<script src="<?php echo base_url(); ?>/assets/scripts/jquery-migrate-3.1.0.min.js"></script> 
<script src="<?php echo base_url(); ?>/assets/scripts/custom.js"></script> 
<script src="<?php echo base_url(); ?>/assets/scripts/jquery.superfish.js"></script> 
<script src="<?php echo base_url(); ?>/assets/scripts/jquery.themepunch.tools.min.js"></script> 
<script src="<?php echo base_url(); ?>/assets/scripts/jquery.themepunch.revolution.min.js"></script> 
<script src="<?php echo base_url(); ?>/assets/scripts/jquery.themepunch.showbizpro.min.js"></script> 
<script src="<?php echo base_url(); ?>/assets/scripts/jquery.flexslider-min.js"></script> 
<script src="<?php echo base_url(); ?>/assets/scripts/chosen.jquery.min.js"></script> 
<script src="<?php echo base_url(); ?>/assets/scripts/jquery.magnific-popup.min.js"></script> 
<script src="<?php echo base_url(); ?>/assets/scripts/waypoints.min.js"></script> 
<script src="<?php echo base_url(); ?>/assets/scripts/jquery.counterup.min.js"></script> 
<script src="<?php echo base_url(); ?>/assets/scripts/jquery.jpanelmenu.js"></script> 
<script src="<?php echo base_url(); ?>/assets/scripts/stacktable.js"></script> 
<script src="<?php echo base_url(); ?>/assets/scripts/slick.min.js"></script> 
<script src="<?php echo base_url(); ?>/assets/scripts/headroom.min.js"></script>
<script src="<?php echo base_url(); ?>/assets/scripts/progress2.js"></script>
<script src="<?php echo base_url(); ?>/assets/scripts/progress.js"></script>
<script>
$(document).ready(function() {
  $('.datepicker').pickadate({
    formatSubmit: 'dd-mm-yyyy',
    min: [2016,12,17],
    max: [2017,01,15],
    closeOnSelect: false,
    closeOnClear: false,
  });
  $('.date-field').autotab('number');
  
  $('.single-date-field').mask('00/00/0000', 
    {placeholder: "_ _ /_ _ /_ _ _ _"});
}); 
</script>
<script>
$(document).ready(function(){
             $('#upload-file').change(function() {
                var filename = $(this).val();
                $('#file-upload-name').html(filename);
                if(filename!=""){
                    setTimeout(function(){
                        $('.upload-wrapper').addClass("uploaded");
                    }, 600);
                    setTimeout(function(){
                        $('.upload-wrapper').removeClass("uploaded");
                        $('.upload-wrapper').addClass("success");
                    }, 1600);
                }
            });
        });
  </script>
  <script>
function getState(val) 
 {

  $.ajax({
          type: "POST",
          url: "<?php echo base_url();?>core_controller/getState",
          data:'country_id='+val,
          success: function(data)
          {
            $("#state").html(data);
            getCity();
          }
  });
 }
 function getCity(val) 
 {
  $.ajax({
          type: "POST",
          url: "<?php echo base_url();?>core_controller/getCity",
          data:'state_id='+val,
          success: function(data)
          {
            $("#city").html(data);
          }
      });
 }

function isNumberKey(evt)
{
   var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

         return true;
}
</script>
</body>
</html>