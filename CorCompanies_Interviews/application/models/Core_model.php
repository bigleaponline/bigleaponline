<?php
ob_start();
class Core_model extends CI_Model 
{
    public function __construct()
    {
        parent::__construct();
        $this->db->cache_on();
        $this->load->database();
       
    }
    public function login()
    {
        $username=$this->input->post('username');
        $password=base64_encode($this->input->post('password'));
        $query=$this->db->get_where('signup_details',array('email'=>$username,'password'=>$password));
        // print_r($this->db->last_query());die();
        if($query->num_rows()>0)
        {
            foreach($query->result() as $details)
            $cand_data=array(
                        'id'=>$details->id,
                        'name'=>$details->name,
                        'password'=>$details->password,
                        'type'=>$details->type,
                        'status'=>$details->status
                        );
            $this->session->set_userdata($cand_data);
            // print_r($cand_data);exit();
            return true;
        }
        else
        {
            return false;
        }
    }

    public function signup()
    {
        $email    = $this->input->post('email');
        $password=$this->input->post('password');
        $data=array(
            'name'=>$this->input->post('name'),
            'phone'=>$this->input->post('phone'),
            'email'=>$this->input->post('email'),
            'type'=>"cand_core",
            'password'=>base64_encode($this->input->post('password')),
            'status'=>0
            );
        $insert=$this->db->insert('signup_details',$data);
        if($insert)
        {
            // $config = Array(
            //                 'protocol' => 'smtp',
            //                 'smtp_host' => 'TLS://mail.bigleaponline.com',
            //                 'smtp_port' => 465,
            //                 'smtp_user' => 'noreply@bigleaponline.com',
            //                 'smtp_pass' => 'zrJP1V9w28C@#p',
            //                 'mailtype'  => 'html', 
            //                 'charset'   => 'iso-8859-1',
            //                 'wordwrap' => TRUE
            //             );

            // $this->load->library('email', $config);
            // $this->email->set_newline("\r\n");
            // $message="Thank you for registering with Us.Your credentials are given below\r\n Username:".$email."\r\n And Password:".$password;
            // $this->email->from('noreply@bigleaponline.com');
            // $this->email->to($email); 
            // $this->email->subject('CorCompanies_Interviews');
            // $this->email->message($message);  
            // if($this->email->send())
            // {
            //     // echo $message;exit();
            //     echo "send successfully";
            // }
            // else
            // {
            //     show_error($this->email->print_debugger());
            // }

            return true;
        }
        else
        {
            return false;
        }

    }

    public function get_cand_details($id)
    {
        $this->db->select('*');
        $this->db->from('signup_details');
        $this->db->where('id',$id);
        $query = $this->db->get()->result_array();
        return $query;
    }

    public function getState()
    {
        if (! empty($_POST["country_id"])) 
        {
            $query = $this->db->get_where('tbl_states',array('country_id'=>$_POST["country_id"]));
            $results = $query->result();
            ?>
            <option value disabled selected>Select State</option>
            <?php
            foreach ($results as $state) 
            {
            ?>
                <option value="<?php echo $state->id; ?>"><?php echo $state->name; ?></option>
                <?php
            }
        }
    }
    public function getCity()
    {
        if (! empty($_POST["state_id"])) 
        {
            $query =$this->db->get_where('tbl_cities',array('state_id'=>$_POST['state_id']));
            $results =$query->result();
            ?>
            <option value disabled selected>Select City</option>
            <?php
            foreach ($results as $city) 
            {
                ?>
                <option value="<?php echo $city->id; ?>"><?php echo $city->name; ?></option>
                <?php
            }
        }
    }

    // public function cand_core_registration($resume,$id)
    // { 
    //     $country = $this->input->post('pref_overseas_country');
    //     $data = array(
    //                     'cand_id' => $id,
    //                     'f_name' => $this->input->post('name'),
    //                     'dob' => $this->input->post('dob'),
    //                     'gender' => $this->input->post('gender'),
    //                     'whatsapp' => $this->input->post('whatsapp'),
    //                     'country' => $this->input->post('country'),
    //                     'state' => $this->input->post('state'),
    //                     'city' => $this->input->post('city'),
    //                     'drive_loc' => $this->input->post('drive_loc'),
    //                     'experience' => $this->input->post('experience'),
    //                     'internship' => $this->input->post('internship'),
    //                     'intern_interest' => $this->input->post('intern_interest'),
    //                     'job_loc' => $this->input->post('job_loc'),
    //                     'transac_id' => $this->input->post('transac_id'),
    //                     'overseas' => implode(",", $country),
    //                     'resume' => $resume

    //                 );
    //     $candpro=$this->db->insert('job_seekers', $data);
    //     if($candpro)
    //     {
    //         $cand_id = $this->db->insert_id();
    //         $row_no  = 4;
    //         $qualfcn_catgry  = $this->input->post('qualfcn_catgry');
    //         $course  = $this->input->post('course');
    //         $subject  = $this->input->post('subject');
    //         $university  = $this->input->post('college');
    //         $pass_year  = $this->input->post('pass_year');
    //         $mark  = $this->input->post('mark');
    //         for($i=0;$i<$row_no;$i++)
    //         {
    //             $data1['qualfcn_catgry'] = $qualfcn_catgry[$i];
    //             $data1['course'] = $course[$i];
    //             $data1['subject'] = $subject[$i];
    //             $data1['university'] = $university[$i];
    //             $data1['pass_year'] = $pass_year[$i];
    //             $data1['mark '] = $mark [$i];
    //             $data1['cand_id'] = $cand_id;
    //             $candedu=$this->db->insert('cand_edu_details',$data1);
    //         }
    //         $data2['status']= 2;
    //         $this->db->where('id',$id);
    //         $this->db->update('signup_details',$data2);
    //         return true;
    //     }
    //     else
    //     {
    //         return false;
    //     }
    // }

    public function submit_transac_id($id)
    {
        $data['transac_id']= $this->input->post('transac_id');
        $this->db->where('cand_id',$id);
        $insert=$this->db->update('job_seekers',$data);
        if($insert)
        {
            // $data2['status']= 1;
            // $this->db->where('id',$id);
            // $this->db->update('signup_details',$data2);
            return true;
        }
        else
        {
            return false;
        }

    }

    public function get_transac_id($id)
    {
        $this->db->select('transac_id');
        $this->db->from('job_seekers');
        $this->db->where('cand_id',$id);
        $query = $this->db->get()->result_array();
        // print_r($this->db->last_query());die();
        return $query;

    }
    
    public function ismail_exist($email)
    {
        $this->db->where('email',$email);
        $query = $this->db->get('signup_details');
        if ($query->num_rows() > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function pr1_registration($id)
    {
        $data = array(
                    'cand_id' => $id,
                    'f_name' => $this->input->post('name'),
                    'dob' => $this->input->post('dob'),
                    'gender' => $this->input->post('gender'),
                    'whatsapp' => $this->input->post('whatsapp'),
                    'country' => $this->input->post('country'),
                    'state' => $this->input->post('state'),
                    'city' => $this->input->post('city'),
                    'drive_loc' => $this->input->post('drive_loc')

                    );
        $candpro=$this->db->insert('job_seekers', $data);
        if($candpro)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function pr2_registration($id,$resume)
    {
       if($resume)
        
        $data = array(
                        
                    'resume' => $resume

                    );
        
        else
        $data = array(
                        
                    'resume' => "Null"

                    );
        $this->db->where('cand_id',$id);
        $insert=$this->db->update('job_seekers',$data);
        if($insert)
        {
        $cand_id = $id;
        $row_no  = 5;
        $qualfcn_catgry  = $this->input->post('qualfcn_catgry');
        $course  = $this->input->post('course');
        $subject  = $this->input->post('subject');
        $university  = $this->input->post('college');
        $pass_year  = $this->input->post('pass_year');
        $mark  = $this->input->post('mark');
        for($i=0;$i<$row_no;$i++)
        {
            $data1['qualfcn_catgry'] = $qualfcn_catgry[$i];
            $data1['course'] = $course[$i];
            $data1['subject'] = $subject[$i];
            $data1['university'] = $university[$i];
            $data1['pass_year'] = $pass_year[$i];
            $data1['mark '] = $mark [$i];
            $data1['cand_id'] = $cand_id;
            $candedu=$this->db->insert('cand_edu_details',$data1);
        }
        return true;
        }

        else
        {
            return false;
        }
    }

    public function pr3_registration($id)
    {
       $country = $this->input->post('pref_overseas_country');
        $data = array(
                        'backlogs' => $this->input->post('backlogs'),
                        'experience' => $this->input->post('experience'),
                        'internship' => $this->input->post('internship'),
                        'intern_interest' => $this->input->post('intern_interest'),
                        'job_loc' => $this->input->post('job_loc'),
                        'transac_id' => $this->input->post('transac_id'),
                        'overseas' => implode(",", $country)

                    );
        $this->db->where('cand_id',$id);
        $insert=$this->db->update('job_seekers',$data);
        if($insert)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    

    public function pr4_registration($id)
    {
        $data2['transac_id']= "";
        if($data2['transac_id'] != "")
        {
        $data2['transac_id']= $this->input->post('transac_id');
        $this->db->where('id',$id);
        $insert=$this->db->update('job_seekers',$data2);
        if($insert)
        {
            $data3['status']= 2;
            $this->db->where('id',$id);
            $this->db->update('signup_details',$data3);
            return true;
        }
        else
        {
            return false;
        }
        }
        else
        {
        $data2['transac_id']= Null;
        $this->db->where('id',$id);
        $insert=$this->db->update('job_seekers',$data2);
        if($insert)
        {
            $data3['status']= 2;
            $this->db->where('id',$id);
            $this->db->update('signup_details',$data3);
            return true;
        }
        else
        {
            return false;
        }
        }

        
    }
}
?>