<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main_controller extends CI_Controller 
{
public function __construct()
{
    parent::__construct();
    $this->load->model('main_model');
    $this->load->library('session');
    $this->load->library('form_validation');
    $this->load->helper(array('form', 'url'));
}
public function loadpage()
{
  $page=$this->uri->segment('1');
  $data['page_title']=$this->uri->segment('1');
  $this->load->view('sub_header',$data);
  $this->load->view($page);
  $this->load->view('footer');
}
public function index()
{
  $data['page_title']=$this->uri->segment('1');
  $data['job_list']=$this->main_model->get_job_list();
  $this->load->view('header',$data);
  $this->load->view('index',$data);
  $this->load->view('footer');
}

public function signin()
{
  $data['page_title']=$this->uri->segment('1');
  $this->load->view('sub_header',$data);
  $this->load->view('login');
  $this->load->view('footer1');
}
public function post_resume()
{
  $data['page_title']=$this->uri->segment('1');
  $this->load->view('sub_header',$data);
  $this->load->view('postresume');
  $this->load->view('footer1');
}
public function job_seekers()
{
  $data['page_title']=$this->uri->segment('1');
  $this->load->view('sub_header',$data);
  $this->load->view('post-job');
  $this->load->view('footer1');
}

public function login()
{
  $login=$this->main_model->login();
  if($login)
  {
    $this->session->set_userdata('cand_data',$login);
    $data['status'] = $this->session->userdata('status');
    $status = $this->session->userdata('status');
    $data['type'] = $this->session->userdata('type');
    $type = $this->session->userdata('type');
    if($status == 1 && $type == 'candidate' || $type == 'company')
    {
      redirect('home');
    }
    elseif($status == 1 && $type == 'cand_core')
    {
      redirect('home');
    }
    elseif($status == 1 && $type == 'cand_virtual')
    {
      redirect('home');
    }
    
  }
  else
  {
    $this->session->set_flashdata('loginerror','Username or Password Is Incorrect');
    redirect('login');
  }
}

public function logout()
{
  $this->session->unset_userdata('cand_data');
  $this->session->sess_destroy();
  $data['page_title']=$this->uri->segment('1');
  $this->load->view('header',$data);
  $this->load->view('login');
  $this->load->view('footer1');
}
public function candidate_registration()
{
  // $image=$this->do_upload_profile_image();
  $resume=$this->do_upload_resume();
  if(isset($_POST['terms']) && $_POST['terms'] == 1) {
  $query=$this->main_model->candidate_registration($resume);
  if($query > 0)
  {
    $this->session->set_flashdata('registration','You Are Registered Successfully');
    redirect('postresume');
  }
  else
  {
    $this->session->set_flashdata('registererr','Failed.... Please try again...');
    redirect('postresume');
  }
  }
  else
  {
    $this->session->set_flashdata('declarationerror','Please check the declaration!!');
    redirect('postresume');
  }
}

 public function do_upload_logo() 
 {
  
   $config['upload_path'] = './upload/logo';
   $config['allowed_types'] = 'jpg|png|jpeg';
   $config['max_size'] = '300000';
   $config['max_width'] = '50';
   $config['max_height'] = '50';
   $config['overwrite'] = TRUE;
   $logo = time().$_FILES['img']['name'];
   $config['file_name'] = $logo;
   $this->load->library('upload', $config);
   $this->upload->initialize($config);
   if (!$this->upload->do_upload('img')) 
   {
       $error = array('error' => $this->upload->display_errors()); 
       print_r($error);
       return FALSE;
   } 
   else 
   {
       $data = array('upload_data' => $this->upload->data());
       return $logo;
   }
 }

public function do_upload_resume() 
{

    $config['upload_path'] = './upload/resume';
    $config['allowed_types'] = 'pdf|doc|docx';
    $config['max_size'] = '5000';
    $config['overwrite'] = TRUE;
    $resume = time().$_FILES['userfile']['name']; 
    $config['file_name'] = $resume;
    $this->load->library('upload', $config);
    $this->upload->initialize($config);
    if (!$this->upload->do_upload('userfile')) 
    {
        $error = array('error' => $this->upload->display_errors());  
        print_r($error);
        return FALSE;
    } 
    else
    {
        $data = array('upload_data' => $this->upload->data());
        return $resume;
    }

}

public function contact_us() 
{
  $contact=$this->main_model->contact_us();
  if($contact)
  {
    $this->session->set_flashdata('contact','Message sent Successfully');
    redirect('contact');
  }
  else
  {
    $this->session->set_flashdata('contacterror','Failed.... Please try again...');
    redirect('contact');
  }
  
}

public function faq() 
{
  $contact=$this->main_model->faq();
  if($contact)
  {
    $this->session->set_flashdata('faq','Message sent Successfully');
    redirect('faq');
  }
  else
  {
    $this->session->set_flashdata('faqerror','Failed.... Please try again...');
    redirect('faq');
  }
  
}

	public function employer_registration()
{
  $logo=$this->do_upload_logo();
  if(isset($_POST['terms']) && $_POST['terms'] == 1) {
  $query=$this->main_model->employer_registration($logo);
  if($query > 0)
  {
      $this->session->set_flashdata('registration','You Are Registered Successfully');
      redirect('post-job');
  }
  else
  {
      $this->session->set_flashdata('registererr','Failed.... Please try again...');
      redirect('post-job');
  }
  }
  else{
      $this->session->set_flashdata('declarationerror','Please check the declaration!!');
      redirect('post-job');
  }
}

public function get_job_details()
{
  $data['page_title']=$this->uri->segment('1');
 	$data['job_details']=$this->main_model->get_job_details();
 //	$job_details=$this->main_model->get_job_details();
 //	print_r($job_details);die();
	$this->load->view('header',$data);
	$this->load->view('job_details',$data);
	$this->load->view('footer'); 
}

}
?>