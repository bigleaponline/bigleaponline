
<!--content start here-->
<div class="banner-slide">
  <div class="slidersection about-page">
    <div class="common-titles">
      <h2 class="header-page-subtitle wow fadeInUp">Finding Us has Never Been So Easy.</h2>
      <h1 class="header-page-title wow fadeInUp">Looking for a Job? </h1>
    </div>
  </div>
</div>
<div class="bigleap">
  <section class="contact-info">
  	<div class="container">
  	<h3 class="main-title wow fadeInUp">Reach Us at Your Convenience.</h3>
        <p class="main-descr wow fadeInUp">We would like to hear from you, be it a query, a request or a review. Drop a message or drop in on us. <br>The choice and convenience is yours.</p>
  		<div class="row">
  			<div class="col-md-4">
  				<div class="cnt-main wow fadeInUp">
  					<div class="content">
  					<i class="fa fa-envelope-o" aria-hidden="true"></i>
					<h5 class="wow fadeInUp">Mail Address</h5>
					<a href="mailto:info@bigleaponline.com" class="wow fadeInUp">info@bigleaponline.com</a>
					<a href="mailto:hr@bigleaponline.com" class="wow fadeInUp">hr@bigleaponline.com</a>
</div>
  				</div>
  			</div>
  			<div class="col-md-4">
  				<div class="cnt-main wow fadeInUp">
  					<div class="content">
  					<i class="fa fa-phone" aria-hidden="true"></i>
					<h5 class="wow fadeInUp">Contact Numbers</h5>
					<a href="tel:+91 9061560224" class="wow fadeInUp">+91 9061560224</a>
					<a href="tel:0495 296060" class="wow fadeInUp">0495 2960600</a>
</div>
  				</div>
  			</div>
  			<div class="col-md-4">
  				<div class="cnt-main wow fadeInUp">
  					<div class="content">
  					<i class="fa fa-address-card-o" aria-hidden="true"></i>
					<h5 class="wow fadeInUp">Office Address</h5>
					<h6>4th Floor - Markaz Complex - Mavoor Road - Kozhikode - Kerala</h6>
</div>
  				</div>
  			</div>
  		</div>
  	</div>
  </section>
  
  <!--<section class="our-team">
    <div class="team" id="team">
      <div class="container">
        <h3 class="main-title wow fadeInUp">We are a smart team of <br>
          leading Skill development</h3>
        <p class="main-descr wow fadeInUp">The first is a non technical method which requires the use of adware removal software. Download free adware and spyware removal software and use advanced tools getting infected.</p>
        <div class="team-grids">
          <div class="team-grid col-lg-3 col-md-3 col-sm-3">
            <div class="pic">
              <div class="stack twisted  wow fadeInUp"> <img src="images/team.png" alt=" " class="img-responsive"> </div>
            </div>
            <h4 class=" wow fadeInUp">Peter Parker</h4>
            <h5 class=" wow fadeInUp">web developer</h5>
            <ul class="social-nav model-3d-0">
              <li class=" wow fadeInUp"> <a href="#" class="facebook">
                <div class="front"> <i class="fa fa-facebook" aria-hidden="true"></i></div>
                <div class="back"><i class="fa fa-facebook" aria-hidden="true"></i></div>
                </a> </li>
              <li class=" wow fadeInUp"> <a href="#" class="twitter">
                <div class="front"> <i class="fa fa-twitter" aria-hidden="true"></i> </div>
                <div class="back"><i class="fa fa-twitter" aria-hidden="true"></i> </div>
                </a> </li>
              <li class=" wow fadeInUp"> <a href="#" class="instagram">
               <div class="front"><i class="fa fa-instagram" aria-hidden="true"></i> </div>
                <div class="back"> <i class="fa fa-instagram" aria-hidden="true"></i></div>
                </a> </li>
              <li class=" wow fadeInUp"> <a href="#" class="linkedin">
                <div class="front"> <i class="fa fa-linkedin" aria-hidden="true"></i> </div>
                <div class="back"> <i class="fa fa-linkedin" aria-hidden="true"></i></div>
                </a> </li>
                <li class=" wow fadeInUp"> <a href="#" class="pinterest">
                <div class="front"><i class="fa fa-pinterest-p" aria-hidden="true"></i> </div>
                <div class="back"><i class="fa fa-pinterest-p" aria-hidden="true"></i></div>
                </a> </li>
            </ul>
          </div>
          <div class="team-grid col-lg-3 col-md-3 col-sm-3">
            <div class="pic">
              <div class="stack twisted  wow fadeInUp"> <img src="images/testi-01.png" alt=" " class="img-responsive"> </div>
            </div>
            <h4 class=" wow fadeInUp">Michael Loe</h4>
            <h5 class=" wow fadeInUp">web developer</h5>
            <ul class="social-nav model-3d-0">
              <li class=" wow fadeInUp"> <a href="#" class="facebook">
                <div class="front"> <i class="fa fa-facebook" aria-hidden="true"></i></div>
                <div class="back"><i class="fa fa-facebook" aria-hidden="true"></i></div>
                </a> </li>
              <li class=" wow fadeInUp"> <a href="#" class="twitter">
                <div class="front"> <i class="fa fa-twitter" aria-hidden="true"></i> </div>
                <div class="back"><i class="fa fa-twitter" aria-hidden="true"></i> </div>
                </a> </li>
              <li class=" wow fadeInUp"> <a href="#" class="instagram">
                <div class="front"> <i class="fa fa-linkedin" aria-hidden="true"></i> </div>
                <div class="back"> <i class="fa fa-linkedin" aria-hidden="true"></i></div>
                </a> </li>
              <li class=" wow fadeInUp"> <a href="#" class="pinterest">
                <div class="front"><i class="fa fa-instagram" aria-hidden="true"></i> </div>
                <div class="back"> <i class="fa fa-instagram" aria-hidden="true"></i></div>
                </a> </li>
                <li class=" wow fadeInUp"> <a href="#" class="pinterest">
                <div class="front"><i class="fa fa-pinterest-p" aria-hidden="true"></i> </div>
                <div class="back"><i class="fa fa-pinterest-p" aria-hidden="true"></i></div>
                </a> </li>
            </ul>
          </div>
          <div class="team-grid col-lg-3 col-md-3 col-sm-3">
            <div class="pic">
              <div class="stack twisted  wow fadeInUp"> <img src="images/team.png" alt=" " class="img-responsive"> </div>
            </div>
            <h4 class=" wow fadeInUp">Mary Jane</h4>
            <h5 class=" wow fadeInUp">web developer</h5>
            <ul class="social-nav model-3d-0">
              <li class=" wow fadeInUp"> <a href="#" class="facebook">
                <div class="front"> <i class="fa fa-facebook" aria-hidden="true"></i></div>
                <div class="back"><i class="fa fa-facebook" aria-hidden="true"></i></div>
                </a> </li>
              <li class=" wow fadeInUp"> <a href="#" class="twitter">
                <div class="front"> <i class="fa fa-twitter" aria-hidden="true"></i> </div>
                <div class="back"><i class="fa fa-twitter" aria-hidden="true"></i> </div>
                </a> </li>
              <li class=" wow fadeInUp"> <a href="#" class="instagram">
                <div class="front"> <i class="fa fa-linkedin" aria-hidden="true"></i> </div>
                <div class="back"> <i class="fa fa-linkedin" aria-hidden="true"></i></div>
                </a> </li>
              <li class=" wow fadeInUp"> <a href="#" class="pinterest">
                <div class="front"><i class="fa fa-instagram" aria-hidden="true"></i> </div>
                <div class="back"> <i class="fa fa-instagram" aria-hidden="true"></i></div>
                </a> </li>
                <li class=" wow fadeInUp"> <a href="#" class="pinterest">
                <div class="front"><i class="fa fa-pinterest-p" aria-hidden="true"></i> </div>
                <div class="back"><i class="fa fa-pinterest-p" aria-hidden="true"></i></div>
                </a> </li>
            </ul>
          </div>
          <div class="team-grid col-lg-3 col-md-3 col-sm-3">
            <div class="pic">
              <div class="stack twisted wow fadeInUp"> <img src="images/team.png" alt=" " class="img-responsive"> </div>
            </div>
            <h4 class=" wow fadeInUp">Mary Jane</h4>
            <h5 class=" wow fadeInUp">web developer</h5>
            <ul class="social-nav model-3d-0">
              <li class=" wow fadeInUp"> <a href="#" class="facebook">
                <div class="front"> <i class="fa fa-facebook" aria-hidden="true"></i></div>
                <div class="back"><i class="fa fa-facebook" aria-hidden="true"></i></div>
                </a> </li>
              <li class=" wow fadeInUp"> <a href="#" class="twitter">
                <div class="front"> <i class="fa fa-twitter" aria-hidden="true"></i> </div>
                <div class="back"><i class="fa fa-twitter" aria-hidden="true"></i> </div>
                </a> </li>
              <li class=" wow fadeInUp"> <a href="#" class="instagram">
                <div class="front"> <i class="fa fa-linkedin" aria-hidden="true"></i> </div>
                <div class="back"> <i class="fa fa-linkedin" aria-hidden="true"></i></div>
                </a> </li>
              <li class=" wow fadeInUp"> <a href="#" class="pinterest">
                <div class="front"><i class="fa fa-instagram" aria-hidden="true"></i> </div>
                <div class="back"> <i class="fa fa-instagram" aria-hidden="true"></i></div>
                </a> </li>
                <li class=" wow fadeInUp"> <a href="#" class="pinterest">
                <div class="front"><i class="fa fa-pinterest-p" aria-hidden="true"></i> </div>
                <div class="back"><i class="fa fa-pinterest-p" aria-hidden="true"></i></div>
                </a> </li>
            </ul>
          </div>
          <div class="clearfix"> </div>
        </div>
      </div>
    </div>
  </section>-->
  

<!--content end here--> 

<!-- start javascript file --> 
<script type="application/javascript" src="<?php echo base_url(); ?>/assets/js/jquery.min.js"></script> 
<script type="application/javascript" src="<?php echo base_url(); ?>/assets/js/bootstrap.min.js"></script> 
<script type="application/javascript" src="<?php echo base_url(); ?>/assets/js/index.js"></script> 
<script type="application/javascript" src="<?php echo base_url(); ?>/assets/js/wow.min.js"></script> 
<script type="application/javascript" src="<?php echo base_url(); ?>/assets/js/scroll-top.js"></script>  
<script>
         jQuery(document).ready(function( $ ) {
           // Initiate the wowjs animation library
           new WOW().init();
         });
         $(window).scroll(function(){
           var sticky = $('.sticky'),
               scroll = $(window).scrollTop();
           if (scroll >= 36) sticky.addClass('fixed');
           else sticky.removeClass('fixed');
         });
      </script> 
<!-- end javascript file -->
</body>
</html>