<!--footer start-->
<section class="footer wow fadeInUp">
  <div class="container">
    <div class="row">
      <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="footer-desc">
          <div class="footer-logo wow fadeInUp"> <a href="#"><img src="<?php echo base_url(); ?>/assets/images/logo.png" class="img-responsive" alt=""></a> </div>
          <p class="wow fadeInUp">We believe manpower is the ultimate source or the genesis of all kinds of developments on this planet.</p>
        </div>
      </div>
      <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6">
        <h6 class="list-title wow fadeInUp">QUICK LINKS</h6>
        <ul class="list-items mob-height">
          <li class="wow fadeInUp"> <a href="<?php echo base_url(); ?>">Home</a> </li>
          <li class="wow fadeInUp"> <a href="<?php echo base_url('about'); ?>">About Us</a> </li>
          <li class="wow fadeInUp"> <a href="<?php echo base_url('service'); ?>">Services</a> </li>
          <!--<li class="wow fadeInUp"> <a href="blog.html">Blogs</a> </li>-->
          <li class="wow fadeInUp"> <a href="<?php echo base_url('events'); ?>">Events</a> </li>
          <li class="wow fadeInUp"> <a href="<?php echo base_url('contact'); ?>">Contact us</a> </li>
        </ul>
      </div>
      <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6">
        <h6 class="list-title wow fadeInUp">Resources</h6>
        <ul class="list-items mob-height">
          <li class="wow fadeInUp"> <a href="<?php echo base_url('jobs'); ?>">Jobs</a> </li>
          <li class="wow fadeInUp"> <a href="<?php echo base_url('internship'); ?>">Intership</a> </li>
          <li class="wow fadeInUp"> <a href="<?php echo base_url('project'); ?>">Project Trainings</a> </li>
          <li class="wow fadeInUp"> <a href="<?php echo base_url('skill'); ?>">Skill Development</a> </li>
        </ul>
      </div>
      <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6">
        <h6 class="list-title wow fadeInUp">Registration</h6>
        <ul class="list-items mob-height mob-heightchng">
            <li class="wow fadeInUp"> <a href="<?php echo base_url('login'); ?>">Login</a> </li>
          <li class="wow fadeInUp"> <a href="<?php echo base_url('jobseekers'); ?>">Post Jobs</a> </li>
          <li class="wow fadeInUp"> <a href="<?php echo base_url('postresume'); ?>">Post Resume </a> </li>
          <li class="wow fadeInUp"> <a href="#">Cookies</a> </li>
          <li class="wow fadeInUp"> <a href="<?php echo base_url('logout'); ?>">Logout</a> </li>
        </ul>
      </div>
      <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6">
        <h6 class="list-title wow fadeInUp">Follow Us</h6>
        <ul class="list-items clearfix social-links">
          <li class="wow fadeInUp"> <a href="https://www.facebook.com/BigleapSolutionsPvtLtd/?fref=ts" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a> </li>
          <li class="wow fadeInUp"> <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i> </a> </li>
          <li class="wow fadeInUp"> <a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a> </li>
          <li class="wow fadeInUp"> <a href="#"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a> </li>
          <li class="wow fadeInUp"> <a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a> </li>
        </ul>
        <form class="ft-newsform clearfix">
          <input class="ft-nwsinpt wow fadeInUp" placeholder="Your Email Address" type="text">
          <button class="ft-btnnws wow fadeInUp" type="submit"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></button>
        </form>
      </div>
    </div>
    <p class="ft-bottom wow fadeInUp">Copyright © 2020 BigLeap Online. All Rights Reserved.</p>
  </div>
</section>
<!--<div class="">
  <div id="bkgOverlay" class="backgroundOverlay"></div>
  <div id="delayedPopup" class="delayedPopupWindow"> <a href="#" id="btnClose" title="Click here to close this deal box">[ X ]</a>
    <div class="">
      <div class=""></div>
      <div class="">
        <div id="mc_embed_signup quick-pop">
          <h6>Don't be shy, say hi!</h6>
          <div class="contact-wrapper">
            <form class="form-horizontal quick-pop" role="form" method="post" action="">
              <div class="form-group">
                <div class="col-md-12 col-sm-12">
                  <input type="text" class="form-control" id="name" placeholder="NAME" name="name" value="">
                </div>
              </div>
              <div class="form-group">
                <div class="col-md-12 col-sm-12">
                  <input type="email" class="form-control" id="email" placeholder="EMAIL" name="email" value="">
                </div>
              </div>
              <div class="form-group">
                <div class="col-md-12 col-sm-12">
                  <textarea class="form-control form-control-text" rows="10" placeholder="MESSAGE" name="message"></textarea>
                </div>
              </div>
              <div class="form-group">
                <div class="col-md-12 col-sm-12">
                  <button class="btn btn-primary send-button" id="submit" type="submit" value="SEND">
                  <div class="button"> <i class="fa fa-paper-plane"></i><span class="send-text">SEND</span> </div>
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
   </div>-->
   <a href="javascript:void(0);" id="rocketmeluncur" class="showrocket" ><i></i></a>
   <script type="application/javascript" src="<?php echo base_url(); ?>/assets/js/jquery.min.js"></script> 
<script type="application/javascript" src="<?php echo base_url(); ?>/assets/js/bootstrap.min.js"></script> 
<script>

         $( "#clickme" ).click(function() {
           $( "#book" ).hide( "slow", function() {
           });
         });
      </script> 
<!--footer ends-->