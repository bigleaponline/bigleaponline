
<!--content start here-->
<div class="banner-slide">
  <div class="slidersection about-page">
    <div class="common-titles">
      <h2 class="header-page-subtitle wow fadeInUp"> We are more than an consultancy </h2>
      <h1 class="header-page-title wow fadeInUp">2013-2014</h1>
    </div>
  </div>
</div>
<div class="bigleap">
  <section class="service-sect">
    <div class="container">
      <h3 class="main-title wow fadeInUp"> Events of 2013-2014</h3>
      <p class="main-descr wow fadeInUp">Our specialized RPO services are designed to effectively handle the entire recruitment process of our clients focusing on speed of delivery, quality of talent &amp; accountability. We are well equipped with talented consultants having varied industry knowledge &amp; expertise to execute complete recruitment assignments of the clients.</p>
      	<div class="events-one">
      			
      		<div class="cont">
 
  <div class="demo-gallery wow fadeInUp">
    <ul id="lightgallery">
      <li data-responsive="images/2014-hcl/2014 Hcl Drive_Candidates.JPG" data-src="images/2014-hcl/2014 Hcl Drive_Candidates.JPG"
      
      data-sub-html="<h4>2014 HCl Drive</h4><p>2014 HCl Drive @Cuiet</p>" data-pinterest-text="Pin it" data-tweet-text="share on twitter ">
       
        <a href="">
          <img class="img-responsive" src="images/2014-hcl/2014 Hcl Drive_Candidates.JPG">
          <div class="demo-gallery-poster">
            <img src="images/serchicon-event.png">
          </div>
        </a>
      </li>
      <li data-responsive="images/2014-hcl/2014 Hcl Drive_Mr.Soji Abraham ,MD,BigLeap with HCL HR Team.JPG" data-src="images/2014-hcl/2014 Hcl Drive_Mr.Soji Abraham ,MD,BigLeap with HCL HR Team.JPG"
      data-sub-html="<h4>2014 Hcl Drive</h4><p>2014 Hcl Drive_Mr.Soji Abraham ,MD,BigLeap with HCL HR Team</p>" data-pinterest-text="Pin it" data-tweet-text="share on twitter ">
        <a href="">
          <img class="img-responsive" src="images/2014-hcl/2014 Hcl Drive_Mr.Soji Abraham ,MD,BigLeap with HCL HR Team.JPG">
          <div class="demo-gallery-poster">
            <img src="images/serchicon-event.png">
          </div>
        </a>
      </li>
      <li data-responsive="images/2014-hcl/2014 Hcl Drive_Technical Team members.JPG" data-src="images/2014-hcl/2014 Hcl Drive_Technical Team members.JPG"
      data-sub-html="<h4>2014 Hcl Drive</h4><p>2014 Hcl Drive_Technical Team members</p>" data-pinterest-text="Pin it" data-tweet-text="share on twitter ">
        <a href="">
          <img class="img-responsive" src="images/2014-hcl/2014 Hcl Drive_Technical Team members.JPG">
          <div class="demo-gallery-poster">
            <img src="images/serchicon-event.png">
          </div>
        </a>
      </li>
      <li data-responsive="images/2014-hcl/2014 Hcl Drive_HR.JPG" data-src="images/2014-hcl/2014 Hcl Drive_HR.JPG"
      data-sub-html="<h4>2014 Hcl Drive</h4><p>2014 Hcl Drive_HR</p>" data-pinterest-text="Pin it" data-tweet-text="share on twitter ">
        <a href="">
          <img class="img-responsive" src="images/2014-hcl/2014 Hcl Drive_HR.JPG">
          <div class="demo-gallery-poster">
            <img src="images/serchicon-event.png">
          </div>
        </a>
      </li>
      <li data-responsive="images/2014-hcl/2014 Hcl Drive _shortlisted candidates.JPG" data-src="images/2014-hcl/2014 Hcl Drive _shortlisted candidates.JPG"
      data-sub-html="<h4>2014 Hcl Drive</h4><p>2014 Hcl Drive _shortlisted candidates</p>" data-pinterest-text="Pin it" data-tweet-text="share on twitter ">
        <a href="">
          <img class="img-responsive" src="images/2014-hcl/2014 Hcl Drive _shortlisted candidates.JPG">
          <div class="demo-gallery-poster">
            <img src="images/serchicon-event.png">
          </div>
        </a>
      </li>

    </ul>
  </div>
</div>
      	</div>
    </div>
  </section>
  
</div>
<!--content end here--> 
