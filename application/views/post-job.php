

<!--content start here-->
<div class="banner-slide">
  <div class="slidersection about-page">
    <div class="common-titles">
      <h2 class="header-page-subtitle wow fadeInUp"> We are more than an consultancy </h2>
      <h1 class="header-page-title wow fadeInUp">Registration - Job Seekers</h1>
    </div>
  </div>
</div>
<div class="bigleap">
  <section class="registration">
    <div class="container">
      <h3 class="main-title wow fadeInUp">Welcome To BigLeap Online</h3>
      <p class="main-descr wow fadeInUp">The first is a non technical method which requires the use of adware removal software. Download free adware and spyware removal software and use advanced tools getting infected.</p>
      <div class="form-details">
        <div class="row">
          <div class="col-md-3">
            <h5 class="wow fadeInUp"><i class="fa fa-file-text" aria-hidden="true"></i>OUR SERVICES</h5>
            <div class="first-sect cmn-sect">
              <ul class="search">
                <li class="wow fadeInUp"><a href="#">Man Power Consultancy</a></li>
                <li class="wow fadeInUp"><a href="#">Employabilty Solution</a></li>
                <li class="wow fadeInUp"><a href="#">Training</a></li>
                
                <li class="wow fadeInUp"><a href="#">Campus Corporate Interface</a></li>
                <li class="wow fadeInUp"><a href="#">Reputation Management</a></li>
                
                <li class="wow fadeInUp"><a href="#">Job Fairs</a></li>
                <li class="wow fadeInUp"><a href="#">Software Development</a></li>
                <li class="wow fadeInUp"><a href="#">Web & Digital Marketing</a></li>
                <li class="wow fadeInUp"><a href="#">Industry-Synergized Education</a></li>
                
                <li class="wow fadeInUp"><a href="#">Industry/Corporate-Readiness Pgogram</a></li>
              </ul>
            </div>
            <h5 class="wow fadeInUp"><i class="fa fa-list" aria-hidden="true"></i>CATEGORY</h5>
            <div class="first-sect cmn-sect">
              <ul class="category-search clearfix">
                <li class="wow fadeInUp"><a href="#">IT</a></li>
                <li class="wow fadeInUp"><a href="#">Media</a></li>
                <li class="wow fadeInUp"><a href="#">BPO</a></li>
                <li class="wow fadeInUp"><a href="#">HR</a></li>
                <li class="wow fadeInUp"><a href="#">Architecture</a></li>
                <li class="wow fadeInUp"><a href="#">Banking</a></li>
                <li class="wow fadeInUp"><a href="#">Engineering</a></li>
                <li class="wow fadeInUp"><a href="#">Non-IT</a></li>
                <li class="wow fadeInUp"><a href="#">Medical</a></li>
                <li class="wow fadeInUp"><a href="#">Accounting </a></li>
                <li class="wow fadeInUp"><a href="#">Hotels</a></li>
                <li class="wow fadeInUp"><a href="#">Telecom </a></li>
                <li class="wow fadeInUp"><a href="#">Sales</a></li>
              </ul>
            </div>
          </div>
          <div class="col-md-9">
    <?php 
      if($this->session->flashdata('registration'))
      {
        echo '<div class="alert alert-success" role="alert">'.$this->session->flashdata('registration').'</div>';   
      }
      if($this->session->flashdata('registererr'))
      {
        echo '<div class="alert alert-danger" role="alert">'.$this->session->flashdata('registererr').'</div>';   
      }
      if($this->session->flashdata('declarationerror'))
      {
        echo '<div class="alert alert-danger" role="alert">'.$this->session->flashdata('declarationerror').'</div>';   
      }
    ?>
            <form class="regi-form" action="<?php echo base_url();?>main_controller/employer_registration" method="post" enctype="multipart/form-data">
              <h6 class="wow fadeInUp">Employer Registration</h6>
              <div class="form-reg-main">
                <div class="row">
                  <div class="col-md-6 col-sm-6 col-lg-6">
                    <label class="reg-label wow fadeInUp">Company Name<sup>*</sup></label>
                    <input class="reg-inpt wow fadeInUp" type="text" placeholder="Enter Company Name:" name="name" required>
                  </div>
               <div class="col-md-6 col-sm-6 col-lg-6">
                    <label class="reg-label wow fadeInUp">Location</label>
                    <select id="experiance" class="reg-inpt wow fadeInUp select-reg" name="location" required>
                      <option value="hide">-- Location --</option>
                      <option value="Kerala">Kerala</option>
                      <option value="Tamilnadu">Tamilnadu</option>
                      <option value="Goa">Goa</option>
                      <option value="Karnataka">Karnataka</option>
 
                    </select>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6 col-sm-6 col-lg-6">
                    <label class="reg-label wow fadeInUp">Contact No<sup>*</sup></label>
                    <input class="reg-inpt wow fadeInUp" placeholder="Enter Contact Number" name="phone" type="number" required>
                  </div>
                  <div class="col-md-6 col-sm-6 col-lg-6">
                    <label class="reg-label wow fadeInUp">Contact Person</label>
                    <input class="reg-inpt wow fadeInUp" type="text" placeholder="Contact Person" name="contact_person" required>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6 col-sm-6 col-lg-6">
                    <label class="reg-label wow fadeInUp">Email ID</label>
                    <input class="reg-inpt wow fadeInUp" type="email" placeholder="Enter Your Email ID" name="email" required>
                  </div>
                  <div class="col-md-6 col-sm-6 col-lg-6">
                    <label class="reg-label wow fadeInUp">Designation</label>
                    <input class="reg-inpt wow fadeInUp" type="text" placeholder="Designation" name="designation" required>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6 col-sm-6 col-lg-6">
                    <label class="reg-label wow fadeInUp">Type of Industry</label>
                    <select id="experiance" class="reg-inpt wow fadeInUp select-reg" name="industry" required>
                      <option value="hide">-- Industries --</option>
                      <option value="Web">Web</option>
                      <option value="Software">Software</option>
                      <option value="Marketing">Marketing</option>
                      <option value="Sale">Sale</option>
 
                    </select>
                  </div>
                  <div class="col-md-6 col-sm-6 col-lg-6">
                    <label class="reg-label wow fadeInUp">Upload Your Logo</label>
                    <input type="file" id="img" name="img" accept="image/*" required>
                  </div>
                </div>
                <div class="row">
                     <div class="col-md-6 col-sm-6 col-lg-6">
                    <label class="reg-label wow fadeInUp">Enter Your Password</label>
                    <input class="reg-inpt reg-textarea wow fadeInUp" type="password" placeholder="Enter Your Password" name="password" required>
                  </div>
                   <div class="col-md-6 col-sm-6 col-lg-6">
                    <label class="reg-label wow fadeInUp">Confirm Your Password</label>
                    <input class="reg-inpt reg-textarea wow fadeInUp" type="password" placeholder="Confirm Your Password" name="c_password" required>
                  </div>
                </div>
                <div class="row">
                     <div class="col-md-12 col-sm-12 col-lg-12">
                    <label class="reg-label wow fadeInUp">Address</label>
                    <input class="reg-inpt reg-textarea wow fadeInUp" placeholder="Enter Your Address" name="address" required>
                  </div>
                </div>
                
                
                 <div class="row">
                     <div class="col-md-12 col-sm-12 col-lg-12">
                     <label class="reg-label wow fadeInUp">Responsibilities</label>
              <textarea name="responsibilities" id="tempalte" required></textarea>
</div>
                </div>


          <div class="col-md-12 hidden-div jd">
              <label class="reg-label wow fadeInUp">Job Details</label>
              <div class="col-md-11 col-xs-11">
                <input type="hidden" name="row_no" value="1" id="row_no">
                
               <div class="table-responsive">
                <table class="table table-bordered" id="myTable">
                  <thead>
                    <tr>
                      <th scope="col">Job Title</th>
                      <th scope="col">Experience</th>
                      <th scope="col">Qualification</th>
                      <th scope="col">Salary</th>
                      <th scope="col">Job Description</th>
                    </tr>
                  </thead>
                  <tbody class="t-body">
                    <tr class="row-periord" id="row1">
                      <td><input type="text" class="form-control title" id="job_title" name="job_title[]" placeholder="Enter Job Title"></td>
                      <td><input type="text" class="form-control experience" id="experience" name="experience[]" placeholder="Enter Experience"></td>
                      <td><input type="text" class="form-control qualification" id="qualification" name="qualification[]" placeholder="Enter Qualification"></td>
                      <td><input type="text" class="form-control salary" id="salary" name="salary[]" placeholder="Enter Salary"></td>
                      <td class="jb-txtarea"><input class="form-control description" id="job_description" name="job_description[]" placeholder="Job Description"></td>
                    </tr>
                  </tbody>
                </table></div>
              </div>
              <div class="col-md-1 add-del-btn add-del-btn-mobi">
                <div class="add-btn"><i class="fa fa-plus" aria-hidden="true"></i></div>
                <div class="del-btn" style="display:none;" id="vk"><i class="fa fa-trash-o" aria-hidden="true"></i></div>
              </div>
            </div>


                <div class="row">
                  <div class="col-md-12 col-sm-12 col-lg-12">
 <div class="main-wrapper">
     <h4>Upload your JD *(txt or pdf file.)</h4>
        <div class="upload-main-wrapper">
            
                <div class="upload-wrapper">
                    
                        <input type="file" id="upload-file" name="jd[]" required>
                        <svg version="1.1" xmlns:xlink="" preserveAspectRatio="xMidYMid meet" viewBox="224.3881704980842 176.8527621722847 221.13266283524905 178.8472378277154" width="221.13" height="178.85"><defs><path d="M357.38 176.85C386.18 176.85 409.53 204.24 409.53 238.02C409.53 239.29 409.5 240.56 409.42 241.81C430.23 246.95 445.52 264.16 445.52 284.59C445.52 284.59 445.52 284.59 445.52 284.59C445.52 309.08 423.56 328.94 396.47 328.94C384.17 328.94 285.74 328.94 273.44 328.94C246.35 328.94 224.39 309.08 224.39 284.59C224.39 284.59 224.39 284.59 224.39 284.59C224.39 263.24 241.08 245.41 263.31 241.2C265.3 218.05 281.96 199.98 302.22 199.98C306.67 199.98 310.94 200.85 314.93 202.46C324.4 186.96 339.88 176.85 357.38 176.85Z" id="b1aO7LLtdW"></path><path d="M306.46 297.6L339.79 297.6L373.13 297.6L339.79 255.94L306.46 297.6Z" id="c4SXvvMdYD"></path><path d="M350.79 293.05L328.79 293.05L328.79 355.7L350.79 355.7L350.79 293.05Z" id="b11si2zUk"></path></defs><g><g><g><use xlink:href="#b1aO7LLtdW" opacity="1" fill="#ffffff" fill-opacity="1"></use></g><g><g><use xlink:href="#c4SXvvMdYD" opacity="1" fill="#363535" fill-opacity="1"></use></g><g><use xlink:href="#b11si2zUk" opacity="1" fill="#363535" fill-opacity="1"></use></g></g></g></g></svg>
                        <span class="file-upload-text">Upload File</span>
                        <div class="file-success-text">
                         <svg version="1.1" id="check" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                    viewBox="0 0 100 100"  xml:space="preserve">
                <circle style="fill:rgba(0,0,0,0);stroke:#ffffff;stroke-width:10;stroke-miterlimit:10;" cx="49.799" cy="49.746" r="44.757"/>
                <polyline style="fill:rgba(0,0,0,0);stroke:#ffffff;stroke-width:10;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;" points="
                    27.114,51 41.402,65.288 72.485,34.205 "/>
                </svg> <span>Successfully</span></div>
                    </div>
                    <p id="file-upload-name"></p>
        </div>
    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12 col-sm-12 col-lg-12">
                    <div class="form-group clearfix wow fadeInUp">
                      <input type="checkbox" id="terms" value="1" name="terms">
                      <label for="terms">I Agree <a href="#">Terms & Conditions</a></label>
                    </div>
                  </div>
                </div>
                <!--<div style="overflow: hidden; margin-top: -100px; margin-left: -15px;">-->
                <!--<iframe -->
                <!--src="https://vm.tiktok.com/peqWb8/" scrolling="no" -->
                <!--style="height: 600px; border: 0px none; width: 550px; margin-top: -55px; margin-left: 5px; -->
                <!--margin-right: 0px; ">-->
                <!--</iframe>-->
                <!--</div>-->
                <div class="row">
                  <div class="col-md-12 col-sm-12 col-lg-12">
                    <button class="reg-btn wow fadeInUp" type="submit">SUBMIT</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="client-sect">
      <h3 class="main-title wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">Our Clientele Includes <br>the Most Wanted</h3>
      <p class="main-descr wow fadeInUp">A wide portfolio of satisfied and reliable clients who stand a testimony to the service<br> we render to the industry and the quality practices we adhere to. </p>
      <div class="container">
        <div class="owl-carousel owl-theme mobilesliderpro" id="carousel01">
          <div class="item wow fadeInUp"><a href="#"><img src="<?php echo base_url(); ?>/assets/images/client-01.png" class="img-responsive"></a></div>
       
          <div class="item wow fadeInUp"><a href="#"><img src="<?php echo base_url(); ?>/assets/images/client-03.png" class="img-responsive"></a></div>
          <div class="item wow fadeInUp"><a href="#"><img src="<?php echo base_url(); ?>/assets/images/client-04.png" class="img-responsive"></a></div>
       
          <div class="item wow fadeInUp"><a href="#"><img src="<?php echo base_url(); ?>/assets/images/client-06.png" class="img-responsive"></a></div>
        </div>
      </div>
    </section>
  
<!-- </div> -->
<!--content end here--> 

   <a href="javascript:void(0);" id="rocketmeluncur" class="showrocket" ><i></i></a>
<!--footer ends--> 
<!-- start javascript file --> 
<script type="application/javascript" src="<?php echo base_url(); ?>/assets/js/jquery.min.js"></script> 
<script type="application/javascript" src="<?php echo base_url(); ?>/assets/js/bootstrap.min.js"></script> 
<script type="application/javascript" src="<?php echo base_url(); ?>/assets/js/owl.carousel.min.js"></script> 
<script type="application/javascript" src="<?php echo base_url(); ?>/assets/js/jquery.touchSwipe.min.js"></script> 
<script type="application/javascript" src="<?php echo base_url(); ?>/assets/js/index.js"></script> 
<script type="application/javascript" src="<?php echo base_url(); ?>/assets/js/progress.js"></script> 
<script type="application/javascript" src="<?php echo base_url(); ?>/assets/js/wow.min.js"></script> 
<script type="application/javascript" src="<?php echo base_url(); ?>/assets/js/scroll-top.js"></script> 
<!--<script type="application/javascript" src="<?php echo base_url(); ?>/assets/js/login-script.js"></script> -->
<script src='https://cdn.tinymce.com/4/tinymce.min.js'></script>
<script>
     $('#carousel01').owlCarousel({
            nav:true,
          margin:10,
          loop:true,
          autoplay: true,
            responsive:{
                 0:{
                     items:1
                 },
                 600:{
                     items:3
                 },
                 1000:{
                     items:6
                 }
             }
         });
$(document).ready(function(){
             $('#upload-file').change(function() {
                var filename = $(this).val();
                $('#file-upload-name').html(filename);
                if(filename!=""){
                    setTimeout(function(){
                        $('.upload-wrapper').addClass("uploaded");
                    }, 600);
                    setTimeout(function(){
                        $('.upload-wrapper').removeClass("uploaded");
                        $('.upload-wrapper').addClass("success");
                    }, 1600);
                }
            });
        });
      </script> 
<script>
         jQuery(document).ready(function( $ ) {
           // Initiate the wowjs animation library
           new WOW().init();
         });
         $(window).scroll(function(){
           var sticky = $('.sticky'),
               scroll = $(window).scrollTop();
           if (scroll >= 36) sticky.addClass('fixed');
           else sticky.removeClass('fixed');
         });
      </script> 
<script type="text/javascript">
$(document).ready(function()
  {
    var tbody = $('#myTable').children('tbody');
   var table = tbody.length ? tbody : $('#myTable');
   $(".row-periord:last").find(".title");
   $('.add-btn').on('click', function()
   {
    var row_no = $('#row_no').val();
    var rows=$('#myTable tr').length;
    var $sr = ($(".row-periord").length + 1);
    var rowid = Math.random();
    if(rows>1)
    {
        $("#vk").show();
    }
    var $html = '<tr class="row-periord" id="' + rowid + '">' +
     
        '<td><input class="form-control"  type="input" name="job_title[]" id="job_title'+$sr+'" value=""/></td>' +
        '<td><input class="form-control"  type="input" name="eperience[]" id="eperience'+$sr+'" value=""/></td>' +
        '<td><input class="form-control"  type="input" name="qualification[]" id="qualification'+$sr+'" value=""/></td>' +
        '<td><input class="form-control"  type="input" name="salary[]" id="salary'+$sr+'" value=""/></td>' +
        '<td><input class="form-control"  name="job_description[]" id="job_description'+$sr+'"></td>' +
        '</tr>';
    $("#myTable").append($html);
    $("#row_no").val($sr);
    return false;
   });
   });
     $('.del-btn').click(function(){
       $( ".row-periord:last").remove();
       var rows=$('#myTable tr').length;
       document.getElementById("row_no").value =rows - 1;
        if(rows <= 2)
        {
            $("#vk").hide();
        }
});
</script>
<script>
tinymce.init({
  selector: 'textarea',
  height: 500,
  plugins: [
    'advlist autolink lists link image charmap print preview anchor',
    'searchreplace visualblocks code fullscreen',
    'insertdatetime media table contextmenu paste code'
  ],
  toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
  content_css: [
  ]
});

$("button").click(function(){
  var template = tinyMCE.activeEditor.getContent({format : 'raw'});
  console.log(template);
                    $("#show-me").text(template);
  $('pre code').each(function(i, block) {
    hljs.highlightBlock(block);
  });
                   });

</script>
<!-- end javascript file -->
</body>
</html>