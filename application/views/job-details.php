
<!--content start here-->
<div class="banner-slide">
  <div class="slidersection about-page about-page-dark">
<!--    <div class="common-titles">
      <h2 class="header-page-subtitle wow fadeInUp"> We are more than an consultancy </h2>
      <h1 class="header-page-title wow fadeInUp"> Job-Details</h1>
    </div>-->
  </div>
</div>
<div class="bigleap">
  <section class="job-details">
    <div class="container">
      <div class="job-titles">
        <div class="row">
          <div class="col-md-4"> <a href="#" class="company-head wow fadeInUp">
            <div class="row">
              <div class="col-md-3">
                <div class="cmp-logo"> <img src="assets/images/cmpy-logo-01.png" class="img-responsive center-block"> </div>
              </div>
              <div class="col-md-8">
                <div class="cmp--detais wow fadeInUp">
                  <h4>Web Designing</h4>
                  <h5>BigLeap PVT</h5>
                </div>
              </div>
            </div>
            </a> </div>
          <div class="col-md-8">
            <div class="cmp-click-sect">
              <div class="row">
                <div class="col-md-3">
                  <div class="cmp-cmn-det wow fadeInUp"><i class="fa fa-phone"></i><a href="#">+91 9746134563</a></div>
                </div>
                <div class="col-md-4">
                  <div class="cmp-cmn-det wow fadeInUp"><i class="fa fa-map-marker"></i>#2852, SCO 20 Kerala</div>
                </div>
                <div class="col-md-2 padd-zero"> <a href="#" class="job-sub wow fadeInUp"><i class="fa fa-paper-plane"></i><span class="hidden-xs">Apply Now</span></a> </div>
                <div class="col-md-2 padd-zero"> <a href="#" class="cmp-bookmark wow fadeInUp"><i class="fa fa-heart-o"></i><span class="hidden-xs">Bookmark</span> </a> </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="clear"></div>
      <div class="job-full-details">
        <div class="row">
          <div class="col-md-8">
            <div class="detail-wrapper wow fadeInUp">
              <div class="detail-wrapper-body ">
                <div class="job-title-bar wow fadeInUp">
                  <h3 class="wow fadeInUp">Web Design <span class="job-tag bg-success-light">Full Time</span></h3>
                  <div>
                    <p class="address wow fadeInUp"> <i class="fa fa-map-marker"></i> 2726 Shinn Street, New York </p>
                    <p class="rolues wow fadeInUp"><strong>Roles</strong> : UX/UI Designer, Web Designer, Graphic Designer</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="detail-wrapper wow fadeInUp">
              <div class="detail-wrapper-header">
                <h4 class="wow fadeInUp">Overview</h4>
              </div>
              <div class="detail-wrapper-body">
                <p class="wow fadeInUp">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                <p class="wow fadeInUp">It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
              </div>
            </div>
            <div class="detail-wrapper wow fadeInUp">
              <div class="detail-wrapper-header">
                <h4 class="wow fadeInUp">Responsibilities</h4>
              </div>
              <div class="detail-wrapper-body">
                <ul class="">
                  <li class="wow fadeInUp"> <i class="fa fa-long-arrow-right" aria-hidden="true"></i>Execute all visual design stages from concept to final hand-off to engineering </li>
                  <li class="wow fadeInUp"> <i class="fa fa-long-arrow-right" aria-hidden="true"></i>Create wireframes, storyboards, user flows, process flows and site maps to communicate interaction and design ideas </li>
                  <li class="wow fadeInUp"> <i class="fa fa-long-arrow-right" aria-hidden="true"></i>Execute all visual design stages from concept to final hand-off to engineering </li>
                  <li class="wow fadeInUp"> <i class="fa fa-long-arrow-right" aria-hidden="true"></i>Execute all visual design stages from concept to final hand-off to engineering </li>
                  <li class="wow fadeInUp"> <i class="fa fa-long-arrow-right" aria-hidden="true"></i>Create wireframes, storyboards, user flows, process flows and site maps to communicate interaction and design ideas </li>
                </ul>
              </div>
            </div>
            <div class="detail-wrapper wow fadeInUp">
              <div class="detail-wrapper-header">
                <h4 class="wow fadeInUp">Requirements</h4>
              </div>
              <div class="detail-wrapper-body">
                <ul class="">
                  <li class="wow fadeInUp"> <i class="fa fa-long-arrow-right" aria-hidden="true"></i>Proven work experienceas a web designer </li>
                  <li class="wow fadeInUp"> <i class="fa fa-long-arrow-right" aria-hidden="true"></i>Proficiency in HTML, CSS and JavaScript for rapid prototyping </li>
                  <li class="wow fadeInUp"> <i class="fa fa-long-arrow-right" aria-hidden="true"></i>Excellent visual design skills with sensitivity to user-system interaction </li>
                  <li class="wow fadeInUp"> <i class="fa fa-long-arrow-right" aria-hidden="true"></i>Execute all visual design stages from concept to final hand-off to engineering </li>
                </ul>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="widget-boxed wow fadeInUp">
              <div class="widget-boxed-body"> <a href="#" class="long-apply-btn wow fadeInUp"><i class="fa fa-paper-plane"></i>Apply For Job</a> <a href="#" class="long-apply-btn long-apply-btnchng wow fadeInUp"><i class="fa fa-linkedin"></i>Apply with Linkedin</a> </div>
            </div>
            <div class="widget-boxed">
              <div class="widget-boxed-header">
                <h4 class="wow fadeInUp"><i class="fa fa-list" aria-hidden="true"></i>Job Details</h4>
              </div>
              <div class="widget-boxed-body wow fadeInUp">
                <div class="side-list no-border">
                  <ul>
                      <li class="wow fadeInUp"><i class="fa fa-map-marker" aria-hidden="true"></i>Location</li>
                    <li class="wow fadeInUp"><i class="fa fa-credit-card-alt" aria-hidden="true"></i>Package: 20K To 50K/Month</li>
                    <!--<li class="wow fadeInUp"><i class="fa fa-globe" aria-hidden="true"></i>https://www.bigleap.com</li>
                    <li class="wow fadeInUp"><i class="fa fa-phone" aria-hidden="true"></i>91 234 567 8765</li>
                    <li class="wow fadeInUp"><i class="fa fa-envelope-o" aria-hidden="true"></i>suppoer@bigleap.com</li>-->
                    <li class="wow fadeInUp"><i class="fa fa-graduation-cap" aria-hidden="true"></i>Bachelor Degree</li>
                    <li class="wow fadeInUp"><i class="fa fa-shield" aria-hidden="true"></i>3 Year Exp.</li>
                  </ul>
                  <h5 class="wow fadeInUp">Share Job</h5>
                  <ul class="clearfix job-social">
                    <li class="wow fadeInUp"><a href="#"><i class="fa fa-facebook theme-cl"></i></a></li>
                    <li class="wow fadeInUp"><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                    <li class="wow fadeInUp"><a href="#"><i class="fa fa-twitter theme-cl"></i></a></li>
                    <li class="wow fadeInUp"><a href="#"><i class="fa fa-linkedin theme-cl"></i></a></li>
                    <li class="wow fadeInUp"><a href="#"><i class="fa fa-share" aria-hidden="true"></i></a></li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="widget-boxed">
              <div class="widget-boxed-header">
                <h4><i class="fa fa-clock-o" aria-hidden="true"></i>Opening Hours</h4>
              </div>
              <div class="widget-boxed-body">
                <div class="side-list">
                  <ul class="tim-sct-job">
                    <li>Monday <span>9 AM - 5 PM</span></li>
                    <li>Tuesday <span>9 AM - 5 PM</span></li>
                    <li>Wednesday <span>9 AM - 5 PM</span></li>
                    <li>Thursday <span>9 AM - 5 PM</span></li>
                    <li>Friday <span>9 AM - 5 PM</span></li>
                    <li>Saturday <span>9 AM - 3 PM</span></li>
                    <li>Sunday <span>Closed</span></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="clear"></div>
  </section>
  <section class="company-list">
    	<div class="container">
    		<h3 class="main-title wow fadeInUp">Similar Jobs</h3>
        <p class="main-descr wow fadeInUp">Each Month, More Than 7 Million Jobhunt Turn To Website In Their Search For Work, Making Over
160,000 Applications Every Day.</p>
   	<div class="row">
   		 <div class="owl-carousel owl-theme mobilesliderpro" id="carousel05">
          <div class="item wow fadeInUp"><div class="cmp-empl-list">
          <span class="job-type full-time">Full Time</span>
          <div class="cmp-log"><img src="assets/images/job-sek-01.png" class="img-responsive center-block"></div>
          <h5>BigLeap</h5>
          <p>2708 Scenic Way, Sutter</p>
          <a href="#" class="vacncy-link">5 Open Position</a>
       
          <div class="heart HeartAnimation"></div>
          </div>
          </div>
          <div class="item wow fadeInUp"><div class="cmp-empl-list">
          <span class="job-type full-time">Full Time</span>
          <div class="cmp-log"><img src="assets/images/job-sek-02.png" class="img-responsive center-block"></div>
          <h5>BigLeap</h5>
          <p>2708 Scenic Way, Sutter</p>
          <a href="#" class="vacncy-link">5 Open Position</a>  
          <div class="heart HeartAnimation"></div>
          </div>
          </div>
         <div class="item wow fadeInUp"><div class="cmp-empl-list">
         <span class="job-type part-time">Part Time</span>
          <div class="cmp-log"><img src="assets/images/job-sek-03.png" class="img-responsive center-block"></div>
          <h5>BigLeap</h5>
          <p>2708 Scenic Way, Sutter</p>
          <a href="#" class="vacncy-link">5 Open Position</a>
          <div class="heart HeartAnimation"></div>
          </div>
          </div>
         <div class="item wow fadeInUp"><div class="cmp-empl-list">
          <span class="job-type part-time">Part Time</span>
          <div class="cmp-log"><img src="assets/images/job-sek-04.png" class="img-responsive center-block"></div>
          <h5>BigLeap</h5>
          <p>2708 Scenic Way, Sutter</p>
          <a href="#" class="vacncy-link">5 Open Position</a>
          <div class="heart HeartAnimation"></div>
          </div>
          </div>
         <div class="item wow fadeInUp"><div class="cmp-empl-list">
          <span class="job-type full-time">Part Time</span>
          <div class="cmp-log"><img src="assets/images/job-sek-01.png" class="img-responsive center-block"></div>
          <h5>BigLeap</h5>
          <p>2708 Scenic Way, Sutter</p>
          <a href="#" class="vacncy-link">5 Open Position</a>
          <div class="heart HeartAnimation"></div>
          </div>
          </div>
          <div class="item wow fadeInUp"><div class="cmp-empl-list">
           <span class="job-type part-time">Part Time</span>
          <div class="cmp-log"><img src="assets/images/job-sek-02.png" class="img-responsive center-block"></div>
          <h5>BigLeap</h5>
          <p>2708 Scenic Way, Sutter</p>
          <a href="#" class="vacncy-link">5 Open Position</a>
          <div class="heart HeartAnimation"></div>
          </div>
          </div>
        </div>
   	</div>
    	</div>
    </section>
</div>
<!--content end here--> 
<a href="javascript:void(0);" id="rocketmeluncur" class="showrocket" ><i></i></a> 
<!--footer ends--> 
<!-- start javascript file --> 
<script type="application/javascript" src="<?php echo base_url(); ?>/assets/js/jquery.min.js"></script> 
<script type="application/javascript" src="<?php echo base_url(); ?>/assets/js/bootstrap.min.js"></script> 
<script type="application/javascript" src="<?php echo base_url(); ?>/assets/js/index.js"></script> 
<script type="application/javascript" src="<?php echo base_url(); ?>/assets/js/owl.carousel.min.js"></script> 
<script type="application/javascript" src="<?php echo base_url(); ?>/assets/js/jquery.touchSwipe.min.js"></script> 
<script type="application/javascript" src="<?php echo base_url(); ?>/assets/js/wow.min.js"></script> 
<script type="application/javascript" src="<?php echo base_url(); ?>/assets/js/scroll-top.js"></script> 
<script>
         jQuery(document).ready(function( $ ) {
           // Initiate the wowjs animation library
           new WOW().init();
         });
         $(window).scroll(function(){
           var sticky = $('.sticky'),
               scroll = $(window).scrollTop();
           if (scroll >= 36) sticky.addClass('fixed');
           else sticky.removeClass('fixed');
         });
	    $('#carousel05').owlCarousel({
            nav:true,
         	margin:5,
         	loop:true,
         	autoplay: true,
			autoplayHoverPause: true,
            responsive:{
                 0:{
                     items:1
                 },
                 600:{
                     items:2	
                 },
                 1000:{
                     items:4
                 }
             }
         });
	$(function() {
  $(".HeartAnimation").click(function() {
    $(this).toggleClass("animate");
  });
});
      </script> 
<!-- end javascript file -->
</body>
</html>