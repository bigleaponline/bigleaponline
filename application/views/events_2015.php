
<!--content start here-->
<div class="banner-slide">
  <div class="slidersection about-page">
    <div class="common-titles">
      <h2 class="header-page-subtitle wow fadeInUp"> We are more than an consultancy </h2>
      <h1 class="header-page-title wow fadeInUp">2014-2015</h1>
    </div>
  </div>
</div>
<div class="bigleap">
  <section class="service-sect">
    <div class="container">
      <h3 class="main-title wow fadeInUp"> Events of 2014-2015</h3>
      <p class="main-descr wow fadeInUp">Our specialized RPO services are designed to effectively handle the entire recruitment process of our clients focusing on speed of delivery, quality of talent &amp; accountability. We are well equipped with talented consultants having varied industry knowledge &amp; expertise to execute complete recruitment assignments of the clients.</p>
      <div class="events-one">
        <div class="cont">
          <div class="demo-gallery wow fadeInUp">
            <ul id="lightgallery">
              <li data-responsive="images/2015-insight/7029.jpg" data-src="images/2015-insight/7029.jpg"
      
      data-sub-html="<h4>2015 Drive</h4><p>2015 Drive</p>" data-pinterest-text="Pin it" data-tweet-text="share on twitter "> <a href=""> <img class="img-responsive" src="images/2015-insight/7029.jpg">
                <div class="demo-gallery-poster"> <img src="images/serchicon-event.png"> </div>
                </a> </li>
              <li data-responsive="images/2015-insight/7093.jpg" data-src="images/2015-insight/7093.jpg"
      data-sub-html="<h4>2015 Drive</h4><p>2015 Drive</p>" data-pinterest-text="Pin it" data-tweet-text="share on twitter "> <a href=""> <img class="img-responsive" src="images/2015-insight/7093.jpg">
                <div class="demo-gallery-poster"> <img src="images/serchicon-event.png"> </div>
                </a> </li>
              <li data-responsive="images/2015-insight/7154.jpg" data-src="images/2015-insight/7154.jpg"
      data-sub-html="<h4>2015 Drive</h4><p>2015 Drive_Technical Team members</p>" data-pinterest-text="Pin it" data-tweet-text="share on twitter "> <a href=""> <img class="img-responsive" src="images/2015-insight/7154.jpg">
                <div class="demo-gallery-poster"> <img src="images/serchicon-event.png"> </div>
                </a> </li>
              <li data-responsive="images/2015-insight/7033.jpg" data-src="images/2015-insight/7033.jpg"
      data-sub-html="<h4>2015 Drive</h4><p>2015 Drive</p>" data-pinterest-text="Pin it" data-tweet-text="share on twitter "> <a href=""> <img class="img-responsive" src="images/2015-insight/7033.jpg">
                <div class="demo-gallery-poster"> <img src="images/serchicon-event.png"> </div>
                </a> </li>
              <li data-responsive="images/2015-insight/7051.jpg" data-src="images/2015-insight/7051.jpg"
      data-sub-html="<h4>2015 Drive</h4><p>2015 Drive</p>" data-pinterest-text="Pin it" data-tweet-text="share on twitter "> <a href=""> <img class="img-responsive" src="images/2015-insight/7051.jpg">
                <div class="demo-gallery-poster"> <img src="images/serchicon-event.png"> </div>
                </a> </li>
              <li data-responsive="images/2015-insight/7049.jpg" data-src="images/2015-insight/7049.jpg"
      data-sub-html="<h4>2015 Drive</h4><p>2015 Drive</p>" data-pinterest-text="Pin it" data-tweet-text="share on twitter "> <a href=""> <img class="img-responsive" src="images/2015-insight/7049.jpg">
                <div class="demo-gallery-poster"> <img src="images/serchicon-event.png"> </div>
                </a> </li>
              <li data-responsive="images/2015-insight/7118.jpg" data-src="images/2015-insight/7118.jpg"
      data-sub-html="<h4>2015 Drive</h4><p>2015 Drive</p>" data-pinterest-text="Pin it" data-tweet-text="share on twitter "> <a href=""> <img class="img-responsive" src="images/2015-insight/7118.jpg">
                <div class="demo-gallery-poster"> <img src="images/serchicon-event.png"> </div>
                </a> </li>
                
                 <li data-responsive="images/2015-insight/Fisat/DSCN0366.JPG" data-src="images/2015-insight/Fisat/DSCN0366.JPGg"
      data-sub-html="<h4>2015 Drive</h4><p>2015 Drive Fisat</p>" data-pinterest-text="Pin it" data-tweet-text="share on twitter "> <a href=""> <img class="img-responsive" src="images/2015-insight/Fisat/DSCN0366.JPG">
                <div class="demo-gallery-poster"> <img src="images/serchicon-event.png"> </div>
                </a> </li>
                
                 <li data-responsive="images/2015-insight/Fisat/DSCN0368.JPG" data-src="images/2015-insight/Fisat/DSCN0368.JPG"
      data-sub-html="<h4>2015 Drive</h4><p>2015 Drive Fisat</p>" data-pinterest-text="Pin it" data-tweet-text="share on twitter "> <a href=""> <img class="img-responsive" src="images/2015-insight/Fisat/DSCN0368.JPG">
                <div class="demo-gallery-poster"> <img src="images/serchicon-event.png"> </div>
                </a> </li>
                
                 <li data-responsive="images/2015-insight/Fisat/DSCN0373.JPG" data-src="images/2015-insight/Fisat/DSCN0373.JPG"
      data-sub-html="<h4>2015 Drive</h4><p>2015 Drive Fisat</p>" data-pinterest-text="Pin it" data-tweet-text="share on twitter "> <a href=""> <img class="img-responsive" src="images/2015-insight/Fisat/DSCN0373.JPG">
                <div class="demo-gallery-poster"> <img src="images/serchicon-event.png"> </div>
                </a> </li>
                
                 <li data-responsive="images/2015-insight/Fisat/DSCN0375.JPG" data-src="images/2015-insight/Fisat/DSCN0375.JPG"
      data-sub-html="<h4>2015 Drive</h4><p>2015 Drive Fisat</p>" data-pinterest-text="Pin it" data-tweet-text="share on twitter "> <a href=""> <img class="img-responsive" src="images/2015-insight/Fisat/DSCN0375.JPG">
                <div class="demo-gallery-poster"> <img src="images/serchicon-event.png"> </div>
                </a> </li>
                
                 <li data-responsive="images/2015-insight/Fisat/DSCN0395.JPG" data-src="images/2015-insight/Fisat/DSCN0395.JPG"
      data-sub-html="<h4>2015 Drive</h4><p>2015 Drive Fisat</p>" data-pinterest-text="Pin it" data-tweet-text="share on twitter "> <a href=""> <img class="img-responsive" src="images/2015-insight/Fisat/DSCN0395.JPG">
                <div class="demo-gallery-poster"> <img src="images/serchicon-event.png"> </div>
                </a> </li>
                 <li data-responsive="images/2015-insight/Fisat/DSCN0396.JPG" data-src="images/2015-insight/Fisat/DSCN0396.JPG"
      data-sub-html="<h4>2015 Drive</h4><p>2015 Drive Fisat</p>" data-pinterest-text="Pin it" data-tweet-text="share on twitter "> <a href=""> <img class="img-responsive" src="images/2015-insight/Fisat/DSCN0396.JPG">
                <div class="demo-gallery-poster"> <img src="images/serchicon-event.png"> </div>
                </a> </li>
                
            </ul>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
<!--content end here--> 
