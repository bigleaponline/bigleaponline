
<!--content start here-->
<div class="banner-slide">
  <div class="slidersection about-page">
    <div class="common-titles">
      <h2 class="header-page-subtitle wow fadeInUp"> We are more than an consultancy </h2>
      <h1 class="header-page-title wow fadeInUp">FAQ</h1>
    </div>
  </div>
</div>
<div class="bigleap">
  <section class="service-sect">
    <div class="container">
      <h3 class="main-title wow fadeInUp"> Frequently Asked Questions </h3>
      <div class="faq">
        <div id="accordion" class="panel-group">
    <div class="panel">
      <div class="panel-heading">
      <h4 class="panel-title wow fadeInUp">
        <a href="#panelBodyOne" class="accordion-toggle" data-toggle="collapse" data-parent="#accordion">Why Choose Us</a>
        </h4>
      </div>
      <div id="panelBodyOne" class="panel-collapse collapse in">
      <div class="panel-body">
          <p>BigLeap has been a trendsetter among the job-seeking youth in Kerala, bringing them world’s respected companies to them who brought with them many an opportunity to succeed. This has been actualised by pioneering the concept of job fairs. BigLeap has been the biggest player in transforming job industry from "Employer Centric” to " Employee centric", at least in terms of its developments in Kerala.</p>
        </div>
      </div>
    </div>
    <div class="panel">
      <div class="panel-heading">
      <h4 class="panel-title wow fadeInUp">
        <a href="#panelBodyTwo" class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion">Why Us</a>
        </h4>
      </div>
      <div id="panelBodyTwo" class="panel-collapse collapse">
      <div class="panel-body">
          <p>⁃	Every year, we conduct an average of 35 recruitment programs of which, 15+ would be for major IT MNC companies.<br>
⁃	A trendsetter when it comes to launching technological training program in disruptive technologies. <br>
⁃	Successful on-campus training initiatives of soft skill development across the state.<br>
⁃	Conduct Mega Job Fairs by roping in 20+ companies, mainly MNCs.<br>
⁃	Tie ups with both the federal and central government agencies like IT ministry, Youth Welfare Departments etc. to conduct job events.
</p>
        </div>
      </div>
    </div>
    <div class="panel">
      <div class="panel-heading">
      <h4 class="panel-title wow fadeInUp">
        <a href="#panelBodyThree" class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion">Our Skill Lie in Improving Your Skills</a>
        </h4>
      </div>
      <div id="panelBodyThree" class="panel-collapse collapse">
      <div class="panel-body">
          <p>Our decades-long industry expertise helps us identify the pulse of corporate communication and how working inside a corporate environment would be. We are skilled at training aspirants who heavily lack knowledge of the corporate work style and the westernised work culture without which they may be deemed unfit for cohort experience.</p>
        </div>
      </div>
    </div>
			<!--<div class="panel">
      <div class="panel-heading">
      <h4 class="panel-title wow fadeInUp">
        <a href="#panelBodyFour" class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion">What Terms & conditions have you for job?</a>
        </h4>
      </div>
      <div id="panelBodyFour" class="panel-collapse collapse">
      <div class="panel-body">
          <p>Energistically drive standardized communities through user friendly results. Phosfluorescently initiate superior technologies vis-a-vis low-risk high-yield solutions. Objectively facilitate clicks-and-mortar partnerships vis-a-vis superior partnerships. Continually generate long-term high-impact methodologies via wireless leadership. Holisticly seize resource maximizing solutions via user friendly outsourcing.</p>
        </div>
      </div>
    </div>
			<div class="panel">
      <div class="panel-heading">
      <h4 class="panel-title  wow fadeInUp">
        <a href="#panelBodyFive" class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion">What Terms & conditions have you for job?</a>
        </h4>
      </div>
      <div id="panelBodyFive" class="panel-collapse collapse">
      <div class="panel-body">
          <p>Energistically drive standardized communities through user friendly results. Phosfluorescently initiate superior technologies vis-a-vis low-risk high-yield solutions. Objectively facilitate clicks-and-mortar partnerships vis-a-vis superior partnerships. Continually generate long-term high-impact methodologies via wireless leadership. Holisticly seize resource maximizing solutions via user friendly outsourcing.</p>
        </div>
      </div>
    </div>
			<div class="panel">
      <div class="panel-heading">
      <h4 class="panel-title wow fadeInUp">
        <a href="#panelBodySix" class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion">What Terms & conditions have you for job?</a>
        </h4>
      </div>
      <div id="panelBodySix" class="panel-collapse collapse">
      <div class="panel-body">
          <p>Energistically drive standardized communities through user friendly results. Phosfluorescently initiate superior technologies vis-a-vis low-risk high-yield solutions. Objectively facilitate clicks-and-mortar partnerships vis-a-vis superior partnerships. Continually generate long-term high-impact methodologies via wireless leadership. Holisticly seize resource maximizing solutions via user friendly outsourcing.</p>
        </div>
      </div>
    </div>-->
  </div>
      </div>
    </div>
  </section>
	<section class="faq-form">
		<div class="container">
			<h3 class="main-title wow fadeInUp"> Do You Have Any
Query?</h3>
    <?php 
      if($this->session->flashdata('faq'))
      {
        echo '<div class="alert alert-success" role="alert">'.$this->session->flashdata('faq').'</div>';   
      }
      if($this->session->flashdata('faqerror'))
      {
        echo '<div class="alert alert-danger" role="alert">'.$this->session->flashdata('faqerror').'</div>';   
      }
    ?>
			<form class="faq-form-main" action="<?php echo base_url();?>main_controller/faq" method="post" enctype="multipart/form-data">
				<div class="faq-frm-sub">
				<div class="row">
					<div class="col-md-6">
					<label class="biglabel wow fadeInUp">Name</label>
						<input class="input-big wow fadeInUp" placeholder="" type="text" name="name">
					</div>
					<div class="col-md-6">
					<label class="biglabel wow fadeInUp">Email<sup>*</sup></label>
						<input class="input-big wow fadeInUp" placeholder="" type="email" required name="email">
					</div>
					
				</div>
					</div>
				<div class="faq-frm-sub">
				<div class="row">
					<div class="col-md-6">
					<label class="biglabel wow fadeInUp">Phone Number</label>
						<input class="input-big wow fadeInUp" placeholder="" type="number" name="phone">
					</div>
					<div class="col-md-6">
					<label class="biglabel wow fadeInUp">subject</label>
						<input class="input-big wow fadeInUp" placeholder="" type="text" name="subject">
					</div>
					
				</div>
					</div>
				<div class="faq-frm-sub">
				<div class="row">
					<div class="col-md-12">
					<label class="biglabel wow fadeInUp">Query </label>
						
						<textarea class="input-big input-bigtext wow fadeInUp"name="message"></textarea>
					</div>
					
				</div>
					</div>
				<div class="faq-frm-sub">
				<div class="row">
					<div class="col-md-12">
<button class="big-btn wow fadeInUp" type="submit" >SUBMIT</button>
					</div>
					
				</div>
					</div>
			</form>
			     <!--   <div class="col-md-6">-->
        <!--<video width="320" height="240" controls autoplay > -->
        <?php //base_url('upload/logo/190802SampleVideo_1280x720_1mb.mp4'); ?>
        <!--        <source src=" https://www.tiktok.com/@darkloverzzz7086/video/6802075698698734850" type="video/mp4">-->
        <!--</video>-->
        <!--</div>-->
        
        <!--<iframe type="text/html" width="560" height="315" src="https://tiktok.com/embed/6802075698698734850?autoplay=1" frameborder="0" allow="autoplay" ></iframe>-->
        <!--<iframe type="text/html" width="720" height="456" src="https://tiktok.com/embed/6802075698698734850" frameborder="0" allow="autoplay" ></iframe>-->
		</div>
	</section>
<!-- start javascript file --> 
<script type="application/javascript" src="<?php echo base_url(); ?>/assets/js/jquery.min.js"></script> 
<script type="application/javascript" src="<?php echo base_url(); ?>/assets/js/bootstrap.min.js"></script> 
<script type="application/javascript" src="<?php echo base_url(); ?>/assets/js/owl.carousel.min.js"></script> 
<script type="application/javascript" src="<?php echo base_url(); ?>/assets/js/jquery.touchSwipe.min.js"></script> 
<script type="application/javascript" src="<?php echo base_url(); ?>/assets/js/index.js"></script> 
<script type="application/javascript" src="<?php echo base_url(); ?>/assets/js/wow.min.js"></script> 
<script type="application/javascript" src="<?php echo base_url(); ?>/assets/js/scroll-top.js"></script> 
<script>
 $(document).ready(function() {
  $(".set > a").on("click", function() {
    if ($(this).hasClass("active")) {
      $(this).removeClass("active");
      $(this)
        .siblings(".content")
        .slideUp(200);
      $(".set > a i")
        .removeClass("fa-minus")
        .addClass("fa-plus");
    } else {
      $(".set > a i")
        .removeClass("fa-minus")
        .addClass("fa-plus");
      $(this)
        .find("i")
        .removeClass("fa-plus")
        .addClass("fa-minus");
      $(".set > a").removeClass("active");
      $(this).addClass("active");
      $(".content").slideUp(200);
      $(this)
        .siblings(".content")
        .slideDown(200);
    }
  });
});

         $( "#clickme" ).click(function() {
           $( "#book" ).hide( "slow", function() {
           });
         });
      </script> 
<script>
         jQuery(document).ready(function( $ ) {
           // Initiate the wowjs animation library
           new WOW().init();
         });
         $(window).scroll(function(){
           var sticky = $('.sticky'),
               scroll = $(window).scrollTop();
           if (scroll >= 36) sticky.addClass('fixed');
           else sticky.removeClass('fixed');
         });
      </script> 
<!-- end javascript file -->
</body>
</html>