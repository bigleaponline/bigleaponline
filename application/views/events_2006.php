
<!--content start here-->
<div class="banner-slide">
  <div class="slidersection about-page">
    <div class="common-titles">
      <h2 class="header-page-subtitle wow fadeInUp"> We are more than an consultancy </h2>
      <h1 class="header-page-title wow fadeInUp">2005-2006</h1>
    </div>
  </div>
</div>
<div class="bigleap">
  <section class="service-sect">
    <div class="container">
      <h3 class="main-title wow fadeInUp"> Events of 2005-2006</h3>
      <p class="main-descr wow fadeInUp">Our specialized RPO services are designed to effectively handle the entire recruitment process of our clients focusing on speed of delivery, quality of talent &amp; accountability. We are well equipped with talented consultants having varied industry knowledge &amp; expertise to execute complete recruitment assignments of the clients.</p>
      	<div class="events-one">
      			
      		<div class="cont">
 
  <div class="demo-gallery wow fadeInUp">
    <ul id="lightgallery">
      <li data-responsive="images/2006/2006 Job Fair @Calicut.JPG" data-src="images/2006/2006 Job Fair @Calicut.JPG"
      
      data-sub-html="<h4>2006 Job Fair</h4><p>2006 Job Fair @Calicut</p>" data-pinterest-text="Pin it" data-tweet-text="share on twitter ">
       
        <a href="">
          <img class="img-responsive" src="images/2006/2006 Job Fair @Calicut.JPG">
          <div class="demo-gallery-poster">
            <img src="images/serchicon-event.png">
          </div>
        </a>
      </li>
      <li data-responsive="images/2006/2006 Job Fair _Accenture.JPG" data-src="images/2006/2006 Job Fair _Accenture.JPG"
      data-sub-html="<h4>2006 Job Fair </h4><p>2006 Job Fair _Accenture</p>" data-pinterest-text="Pin it" data-tweet-text="share on twitter ">
        <a href="">
          <img class="img-responsive" src="images/2006/2006 Job Fair _Accenture.JPG">
          <div class="demo-gallery-poster">
            <img src="images/serchicon-event.png">
          </div>
        </a>
      </li>
      <li data-responsive="images/2006/2006 JOb Fair_ HP,GENPACT HR TEams.JPG" data-src="images/2006/2006 JOb Fair_ HP,GENPACT HR TEams.JPG"
      data-sub-html="<h4>2006 JOb Fair </h4><p>2006 JOb Fair_ HP,GENPACT HR Teams</p>" data-pinterest-text="Pin it" data-tweet-text="share on twitter ">
        <a href="">
          <img class="img-responsive" src="images/2006/2006 JOb Fair_ HP,GENPACT HR TEams.JPG">
          <div class="demo-gallery-poster">
            <img src="images/serchicon-event.png">
          </div>
        </a>
      </li>
      <li data-responsive="images/2006/2006 Job Fair_Participating Companies.JPG" data-src="images/2006/2006 Job Fair_Participating Companies.JPG"
      data-sub-html="<h4>2006 Job Fair_Participating Companies</h4><p>2006 Job Fair_Participating Companies</p>" data-pinterest-text="Pin it" data-tweet-text="share on twitter ">
        <a href="">
          <img class="img-responsive" src="images/2006/2006 Job Fair_Participating Companies.JPG">
          <div class="demo-gallery-poster">
            <img src="images/serchicon-event.png">
          </div>
        </a>
      </li>
      <li data-responsive="images/2006/2006 Job Fair_Registering Candidates.JPG" data-src="images/2006/2006 Job Fair_Registering Candidates.JPG"
      data-sub-html="<h4>2006 Job Fair_Registering Candidates </h4><p>2006 Job Fair_Registering Candidates</p>" data-pinterest-text="Pin it" data-tweet-text="share on twitter ">
        <a href="">
          <img class="img-responsive" src="images/2006/2006 Job Fair_Registering Candidates.JPG">
          <div class="demo-gallery-poster">
            <img src="images/serchicon-event.png">
          </div>
        </a>
      </li>
      <li data-responsive="images/2006/2006 Job Fair_Tata Indicom.JPG" data-src="images/2006/2006 Job Fair_Tata Indicom.JPG"
      data-sub-html="<h4>2006 Job Fair_Tata Indicom</h4><p>2006 Job Fair_Tata Indicom</p>" data-pinterest-text="Pin it" data-tweet-text="share on twitter ">
        <a href="">
          <img class="img-responsive" src="images/2006/2006 Job Fair_Tata Indicom.JPG">
          <div class="demo-gallery-poster">
            <img src="images/serchicon-event.png">
          </div>
        </a>
      </li>
    </ul>
  </div>
</div>
      	</div>
    </div>
  </section>
  
</div>
<!--content end here--> 
