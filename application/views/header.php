<!doctype html>
<html>
<head>
<title><?php echo $page_title; ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<!--bootstrap-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assets/css/bootstrap.css"  media="all" />
<!-- start css file -->
<link href="<?php echo base_url(); ?>/assets/css/base.css" rel="stylesheet" type="text/css" media="all"/>
<link href="<?php echo base_url(); ?>/assets/css/header.css" rel="stylesheet" type="text/css" media="all"/>
<link href="<?php echo base_url(); ?>/assets/css/common.css" rel="stylesheet" type="text/css" media="all"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assets/css/owl-carosel.css" />
<link href="<?php echo base_url(); ?>/assets/css/animate.min.css" rel="stylesheet" type="text/css" media="all" >
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assets/css/font-awesome.min.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assets/css/lightgallery.css" />
<link rel="icon" href="<?php echo base_url(); ?>/assets/images/favicone.png">
<!-- end css file Midas Touch-->
<!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
      <![endif]-->
<script type="text/javascript">function add_chatinline(){var hccid=77698991;var nt=document.createElement("script");nt.async=true;nt.src="https://mylivechat.com/chatinline.aspx?hccid="+hccid;var ct=document.getElementsByTagName("script")[0];ct.parentNode.insertBefore(nt,ct);}
add_chatinline(); </script>
<!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-158680874-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
    
      gtag('config', 'UA-158680874-1');
    </script>

</head>
<body>
<!--header starts here-->
<header class="mainheder">
  <div class="navmenupart sticky">
    <div class="container">
      <div class="row">
        <nav id="cssmenu">
          <div class="logo"><a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>/assets/images/logo.png" class="img-responsive"> </a></div>
          <div id="head-mobile"></div>
          <div class="button"></div>
          <ul>
            <li class="<?php echo ($page_title == "home" ? "active" : "")?>"><a href="<?php echo base_url('home'); ?>">home</a></li>
            <li class="<?php echo ($page_title == "about" ? "active" : "")?>"><a href="<?php echo base_url('about'); ?>">about us</a></li>
            <li class="<?php echo ($page_title == "service" ? "active" : "")?>"> <a href="<?php echo base_url('service'); ?>">services <i class="fa fa-angle-down" aria-hidden="true"></i></a>
              <ul>
                <li> <a href="<?php //echo base_url('jobs'); ?>">Jobs</a> 
                  <!--                  <ul>
                    <li><a href="#">Sub Product</a></li>
                    <li><a href="#">Sub Product</a></li>
                  </ul>--> 
                </li>
                <li> <a href="<?php //echo base_url('internship'); ?>">Internship</a> </li>
                <li><a href="<?php //echo base_url('project'); ?>"> Project training</a></li>
                <li><a href="<?php //echo base_url('skill'); ?>"> Skill development</a> </li>
              </ul>
            </li>
            <li class="<?php echo ($page_title == "events" ? "active" : "")?>"><a href="<?php echo base_url('events'); ?>">Events</a></li>
            <li class="<?php echo ($page_title == "registration" || $page_title == "jobseekers" || $page_title == "postresume" ? "active" : "")?>"> <a href="registration.html">registration <i class="fa fa-angle-down" aria-hidden="true"></i></a>
              <ul>
                <li> <a href="<?php echo base_url('post-job'); ?>">I am Employer</a> </li>
                <li> <a href="<?php echo base_url('postresume'); ?>">I am Candidate</a> </li>
              </ul>
            </li>
            <!--<li><a href="">blog</a></li>-->
            <li class="<?php echo ($page_title == "contact" ? "active" : "")?>"><a href="<?php echo base_url('contact'); ?>">contact us</a></li>
            <li class="looking-li"><a href="<?php echo base_url('login'); ?>"><span class="jobspan"><i class="fa fa-sign-in" aria-hidden="true"></i></span>Login</a></li>
          </ul>
        </nav>
      </div>
    </div>
  </div>
</header>
<!--header end here--> 