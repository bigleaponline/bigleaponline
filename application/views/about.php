 
<!--content start here-->
<div class="banner-slide">
  <div class="slidersection about-page">
    <div class="common-titles">
      <h2 class="header-page-subtitle wow fadeInUp"> We are more than an consultancy </h2>
      <h1 class="header-page-title wow fadeInUp"> About Us</h1>
    </div>
  </div>
</div>
<div class="bigleap">
  <section class="about-page-details">
    <div class="container">
      <div class="row ">
        <div class="col-md-5">
          <div class="img-abt wow fadeInLeft"><img src="<?php echo base_url(); ?>/assets/images/about-page-img-01.png" class="img-responsive center-block"></div>
        </div>
        <div class="col-md-1"></div>
        <div class="col-md-6">
          <h3 class="wow fadeInRight">Leap Into A Big Better Career</h3>
          <p class="wow fadeInUp first">We are a team of proficient and trained recruitment consultants that strives towards providing the companies with the best available talent and offers you the ability to work on every job requisition you get to your full potential. We are one of the best recruitment consultants in the market today. Our consulting team aims to help clients adopt HR best practices. We engage your organisation to understand your needs, formulate strategies to address your challenges in workforce administration, HR Development, Talent Management, Recruitment. Ultimately to help your organisation achieve higher throughput by addressing the Recruitment / Talent acquisition aspect of your organisation.</p>
          <p class="wow fadeInUp"><b>"BigLeap" Trademark is an exclusive copyrighted property of BigLeap Solutions Pvt Ltd.</b></p>
          <p class="wow fadeInUp"><b>Our Core Purpose:Employment, Employment, Employment !!!</b></p>
        </div>
      </div>
      <div class="row marg-top">
        <div class="col-md-6">
          <h3 class="wow fadeInRight">Be a Leader, Not a Follower</h3>
          <p class="wow fadeInUp first">Apart from our HR and RPO services, we are committed to enhance employability through skill development initiatives and all our efforts are directed towards accomplishing it. Our recent partnership with Retailers Association’s skill council of India strengthens the commitment. Similar to these programs we provide multiple skills development training in Finance and Accounting, BFSI, Supply Chain Management and Insurance. Knowing the huge success of BigLeap in the employment space, In the year 2003, India's NumeroUno media house have partnered BigLeap for an event at Cochin. After the huge success of the Job Fair@Cochin, In the same brand "BigLeap" this media giant has done 100's of Job Fairs in all Major metro cities across the country, Making BigLeap the Numero Uno brand in the employment space in India. BigLeap is the only company to have organised more than 5 Mega Job Fairs for the Government of Kerala. We are the trendsetters, now major Government departments, Media Houses have followed our path and are organizing Job Fairs.</p>
        </div>
        <div class="col-md-1"></div>
        <div class="col-md-5">
          <div class="img-abt wow fadeInLeft"><img src="<?php echo base_url(); ?>/assets/images/about-page-img-02.png" class="img-responsive center-block"></div>
        </div>
      </div>
    </div>
  </section>
  
  <!--<section class="abt-process">
    <div class="container">
      <h3 class="main-title wow fadeInUp">The Easiest way of Execution </h3>
      <p class="main-descr wow fadeInUp">The first is a non technical method which requires the use of adware removal software. Download free adware and spyware removal software and use advanced tools getting infected.</p>
      <div class="row">
        <div class="col-md-4">
          <div class="process-main wow fadeInUp">
            <div class="pro-img wow fadeInUp"><img src="images/abt-proce-01.png" class="img-responsive"></div>
            <h4 class="pro-title wow fadeInUp">Great design</h4>
            <p class="pro-details wow fadeInUp">we believe that the success of our company is a result of our clients growth. like what we do? give us a call we believe that the success of our company is a result of our clients growth. like what we do</p>
            <div class="dots wow fadeInUp"><img src="images/proce-dots.png" class="img-responsive center-block"></div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="process-main wow fadeInUp">
            <div class="pro-img wow fadeInUp"><img src="images/abt-proce-02.png" class="img-responsive"></div>
            <h4 class="pro-title wow fadeInUp">Great design</h4>
            <p class="pro-details wow fadeInUp">we believe that the success of our company is a result of our clients growth. like what we do? give us a call we believe that the success of our company is a result of our clients growth. like what we do</p>
            <div class="dots wow fadeInUp"><img src="images/proce-dots.png" class="img-responsive center-block"></div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="process-main wow fadeInUp">
            <div class="pro-img wow fadeInUp"><img src="images/abt-proce-03.png" class="img-responsive"></div>
            <h4 class="pro-title wow fadeInUp">Great design</h4>
            <p class="pro-details wow fadeInUp">we believe that the success of our company is a result of our clients growth. like what we do? give us a call we believe that the success of our company is a result of our clients growth. like what we do</p>
            <div class="dots wow fadeInUp"><img src="images/proce-dots.png" class="img-responsive center-block"></div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="our-team">
    <div class="team" id="team">
      <div class="container">
        <h3 class="main-title wow fadeInUp">We are a smart team of <br>
          leading Skill development</h3>
        <p class="main-descr wow fadeInUp">The first is a non technical method which requires the use of adware removal software. Download free adware and spyware removal software and use advanced tools getting infected.</p>
        <div class="team-grids">
          <div class="team-grid col-lg-3 col-md-3 col-sm-3">
            <div class="pic">
              <div class="stack twisted  wow fadeInUp"> <img src="images/team.png" alt=" " class="img-responsive"> </div>
            </div>
            <h4 class=" wow fadeInUp">Peter Parker</h4>
            <h5 class=" wow fadeInUp">web developer</h5>
            <ul class="social-nav model-3d-0">
              <li class=" wow fadeInUp"> <a href="#" class="facebook">
                <div class="front"> <i class="fa fa-facebook" aria-hidden="true"></i></div>
                <div class="back"><i class="fa fa-facebook" aria-hidden="true"></i></div>
                </a> </li>
              <li class=" wow fadeInUp"> <a href="#" class="twitter">
                <div class="front"> <i class="fa fa-twitter" aria-hidden="true"></i> </div>
                <div class="back"><i class="fa fa-twitter" aria-hidden="true"></i> </div>
                </a> </li>
              <li class=" wow fadeInUp"> <a href="#" class="instagram">
               <div class="front"><i class="fa fa-instagram" aria-hidden="true"></i> </div>
                <div class="back"> <i class="fa fa-instagram" aria-hidden="true"></i></div>
                </a> </li>
              <li class=" wow fadeInUp"> <a href="#" class="linkedin">
                <div class="front"> <i class="fa fa-linkedin" aria-hidden="true"></i> </div>
                <div class="back"> <i class="fa fa-linkedin" aria-hidden="true"></i></div>
                </a> </li>
                <li class=" wow fadeInUp"> <a href="#" class="pinterest">
                <div class="front"><i class="fa fa-pinterest-p" aria-hidden="true"></i> </div>
                <div class="back"><i class="fa fa-pinterest-p" aria-hidden="true"></i></div>
                </a> </li>
            </ul>
          </div>
          <div class="team-grid col-lg-3 col-md-3 col-sm-3">
            <div class="pic">
              <div class="stack twisted  wow fadeInUp"> <img src="images/testi-01.png" alt=" " class="img-responsive"> </div>
            </div>
            <h4 class=" wow fadeInUp">Michael Loe</h4>
            <h5 class=" wow fadeInUp">web developer</h5>
            <ul class="social-nav model-3d-0">
              <li class=" wow fadeInUp"> <a href="#" class="facebook">
                <div class="front"> <i class="fa fa-facebook" aria-hidden="true"></i></div>
                <div class="back"><i class="fa fa-facebook" aria-hidden="true"></i></div>
                </a> </li>
              <li class=" wow fadeInUp"> <a href="#" class="twitter">
                <div class="front"> <i class="fa fa-twitter" aria-hidden="true"></i> </div>
                <div class="back"><i class="fa fa-twitter" aria-hidden="true"></i> </div>
                </a> </li>
              <li class=" wow fadeInUp"> <a href="#" class="instagram">
                <div class="front"> <i class="fa fa-linkedin" aria-hidden="true"></i> </div>
                <div class="back"> <i class="fa fa-linkedin" aria-hidden="true"></i></div>
                </a> </li>
              <li class=" wow fadeInUp"> <a href="#" class="pinterest">
                <div class="front"><i class="fa fa-instagram" aria-hidden="true"></i> </div>
                <div class="back"> <i class="fa fa-instagram" aria-hidden="true"></i></div>
                </a> </li>
                <li class=" wow fadeInUp"> <a href="#" class="pinterest">
                <div class="front"><i class="fa fa-pinterest-p" aria-hidden="true"></i> </div>
                <div class="back"><i class="fa fa-pinterest-p" aria-hidden="true"></i></div>
                </a> </li>
            </ul>
          </div>
          <div class="team-grid col-lg-3 col-md-3 col-sm-3">
            <div class="pic">
              <div class="stack twisted  wow fadeInUp"> <img src="images/team.png" alt=" " class="img-responsive"> </div>
            </div>
            <h4 class=" wow fadeInUp">Mary Jane</h4>
            <h5 class=" wow fadeInUp">web developer</h5>
            <ul class="social-nav model-3d-0">
              <li class=" wow fadeInUp"> <a href="#" class="facebook">
                <div class="front"> <i class="fa fa-facebook" aria-hidden="true"></i></div>
                <div class="back"><i class="fa fa-facebook" aria-hidden="true"></i></div>
                </a> </li>
              <li class=" wow fadeInUp"> <a href="#" class="twitter">
                <div class="front"> <i class="fa fa-twitter" aria-hidden="true"></i> </div>
                <div class="back"><i class="fa fa-twitter" aria-hidden="true"></i> </div>
                </a> </li>
              <li class=" wow fadeInUp"> <a href="#" class="instagram">
                <div class="front"> <i class="fa fa-linkedin" aria-hidden="true"></i> </div>
                <div class="back"> <i class="fa fa-linkedin" aria-hidden="true"></i></div>
                </a> </li>
              <li class=" wow fadeInUp"> <a href="#" class="pinterest">
                <div class="front"><i class="fa fa-instagram" aria-hidden="true"></i> </div>
                <div class="back"> <i class="fa fa-instagram" aria-hidden="true"></i></div>
                </a> </li>
                <li class=" wow fadeInUp"> <a href="#" class="pinterest">
                <div class="front"><i class="fa fa-pinterest-p" aria-hidden="true"></i> </div>
                <div class="back"><i class="fa fa-pinterest-p" aria-hidden="true"></i></div>
                </a> </li>
            </ul>
          </div>
          <div class="team-grid col-lg-3 col-md-3 col-sm-3">
            <div class="pic">
              <div class="stack twisted wow fadeInUp"> <img src="images/team.png" alt=" " class="img-responsive"> </div>
            </div>
            <h4 class=" wow fadeInUp">Mary Jane</h4>
            <h5 class=" wow fadeInUp">web developer</h5>
            <ul class="social-nav model-3d-0">
              <li class=" wow fadeInUp"> <a href="#" class="facebook">
                <div class="front"> <i class="fa fa-facebook" aria-hidden="true"></i></div>
                <div class="back"><i class="fa fa-facebook" aria-hidden="true"></i></div>
                </a> </li>
              <li class=" wow fadeInUp"> <a href="#" class="twitter">
                <div class="front"> <i class="fa fa-twitter" aria-hidden="true"></i> </div>
                <div class="back"><i class="fa fa-twitter" aria-hidden="true"></i> </div>
                </a> </li>
              <li class=" wow fadeInUp"> <a href="#" class="instagram">
                <div class="front"> <i class="fa fa-linkedin" aria-hidden="true"></i> </div>
                <div class="back"> <i class="fa fa-linkedin" aria-hidden="true"></i></div>
                </a> </li>
              <li class=" wow fadeInUp"> <a href="#" class="pinterest">
                <div class="front"><i class="fa fa-instagram" aria-hidden="true"></i> </div>
                <div class="back"> <i class="fa fa-instagram" aria-hidden="true"></i></div>
                </a> </li>
                <li class=" wow fadeInUp"> <a href="#" class="pinterest">
                <div class="front"><i class="fa fa-pinterest-p" aria-hidden="true"></i> </div>
                <div class="back"><i class="fa fa-pinterest-p" aria-hidden="true"></i></div>
                </a> </li>
            </ul>
          </div>
          <div class="clearfix"> </div>
        </div>
      </div>
    </div>
  </section>-->
  
  <!--<section class="client-sect">
    <h3 class="main-title wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">Meet Our Client's</h3>
    <p class="main-descr wow fadeInUp">Our specialized RPO services are designed to effectively handle the entire recruitment process of our clients focusing on speed of delivery.</p>
    <div class="container">
      <div class="owl-carousel owl-theme mobilesliderpro" id="carousel01">
        <div class="item wow fadeInUp"><a href="#"><img src="images/client-01.png" class="img-responsive"></a></div>
        <div class="item wow fadeInUp"><a href="#"><img src="images/client-02.png" class="img-responsive"></a></div>
        <div class="item wow fadeInUp"><a href="#"><img src="images/client-03.png" class="img-responsive"></a></div>
        <div class="item wow fadeInUp"><a href="#"><img src="images/client-04.png" class="img-responsive"></a></div>
        <div class="item wow fadeInUp"><a href="#"><img src="images/client-05.png" class="img-responsive"></a></div>
        <div class="item wow fadeInUp"><a href="#"><img src="images/client-06.png" class="img-responsive"></a></div>
      </div>
    </div>
  </section>-->
  <!-- <section class="newsletter">
      <div class="container">
        <h3 class="main-title wow fadeInUp"> Reaching Us is Never Too Hard<br>
          </h3>
        <p class="main-descr wow fadeInUp">We would like to hear from you, be it a query, a request or a review. Drop a message or drop in on us. <br>The choice and convenience are yours.</p>
        <div class="row">
          <div class="col-md-7">
            <div class="map-sect wow fadeInUp">
              <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d15652.141444286224!2d75.79527265000002!3d11.25880905!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sin!4v1576832177948!5m2!1sen!2sin" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
              <div class="map-addres" id="book"> <span class="click wow fadeInUp" id="clickme"><i class="fa fa-times" aria-hidden="true"></i></span>
                <div class="row adrmarg wow fadeInUp">
                  <div class="col-md-1 adds-loca adds-localeft"> <i class="fa fa-map-marker" aria-hidden="true"></i> </div>
                  <div class="col-md-11 adds-loca wow fadeInUp">
                    <h5>BIGLEAP SOLUTIONS (P) LTD.</h5>
                    <h6>4th Floor - Markaz Complex - Mavoor Road - Kozhikode - Kerala</h6>
                  </div>
                </div>
                <div class="row adrmarg wow fadeInUp">
                  <div class="col-md-1 adds-loca adds-localeft wow fadeInUp"> <i class="fa fa-phone" aria-hidden="true"></i> </div>
                  <div class="col-md-11 adds-loca wow fadeInUp">
                    <h5><a href="tel:+91 9846123123">+91 9061560224</a></h5>
                    <h6>Monday to Saturday - 9am to 5 pm</h6>
                  </div>
                </div>
                <div class="row adrmarg wow fadeInUp">
                  <div class="col-md-1 adds-loca adds-localeft wow fadeInUp"> <i class="fa fa-envelope-o" aria-hidden="true"></i> </div>
                  <div class="col-md-11 adds-loca wow fadeInUp">
                    <h5><a href="mailto:hr@bigleaponline.com">hr@bigleaponline.com</a></h5>
                    <h6>Send us your query anytime!</h6>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-5">
            <form class="footer-form">
              <div class="input-set wow fadeInUp">
                <label class="biglabel wow fadeInUp">Your Name</label>
                <input class="input-big wow fadeInUp" placeholder="Enter your Name" type="text">
              </div>
              <div class="input-set wow fadeInUp">
                <label class="biglabel wow fadeInUp">EMAIL ADDRESS</label>
                <input class="input-big wow fadeInUp" placeholder="Enter your Email" type="email">
              </div>
              <div class="input-set wow fadeInUp">
                <label class="biglabel wow fadeInUp">SUBJECT</label>
                <input class="input-big wow fadeInUp" placeholder="Enter Subject" type="text">
              </div>
              <div class="input-set wow fadeInUp">
                <label class="biglabel wow fadeInUp">MESSAGE</label>
                <textarea class="input-big wow fadeInUp input-txt" placeholder="Enter Message"></textarea>
              </div>
              <button class="big-btn wow fadeInUp" type="submit">SUBMIT</button>
            </form>
          </div>
        </div>
      </div>
    </section>
</div> -->
<!--content end here--> 
<!-- start javascript file --> 
<script type="application/javascript" src="<?php echo base_url(); ?>/assets/js/jquery.min.js"></script> 
<script type="application/javascript" src="<?php echo base_url(); ?>/assets/js/bootstrap.min.js"></script> 
<script type="application/javascript" src="<?php echo base_url(); ?>/assets/js/owl.carousel.min.js"></script> 
<script type="application/javascript" src="<?php echo base_url(); ?>/assets/js/index.js"></script> 
<script type="application/javascript" src="<?php echo base_url(); ?>/assets/js/wow.min.js"></script> 
<script type="application/javascript" src="<?php echo base_url(); ?>/assets/js/scroll-top.js"></script> 
<script>
$( "#clickme" ).click(function() {
           $( "#book" ).hide( "slow", function() {
           });
         });
</script> 
<script>
         jQuery(document).ready(function( $ ) {
           // Initiate the wowjs animation library
           new WOW().init();
         });
         $(window).scroll(function(){
           var sticky = $('.sticky'),
               scroll = $(window).scrollTop();
           if (scroll >= 36) sticky.addClass('fixed');
           else sticky.removeClass('fixed');
         });
      </script> 
<!-- end javascript file -->
</body>
</html>