
<!--content start here-->
<marquee class="marque-one" direction=”right” onmouseover="stop()" onmouseout="start()"> Upcoming Events: ★ On March at - Kannur, Ettumanoor and Trivandrum ★  Date will Update Soon </marquee>
<div class="BigLeap">
  <div class="banner-slide">
    <div class="slidersection"> <img src="<?php echo base_url(); ?>/assets/images/banner-slider.png" class="img-responsive">
      <div class="animationsect">
        <div class="container">
          <div class="row">
            <div class="col-md-6">
              <div class="divslidetxt">
                <h1 class="wow fadeInLeft">looking for your dream job?</h1>
                <a href="<?php echo base_url('about'); ?>" class="cmn-link wow fadeInUp">Read More</a> </div>
            </div>
            <div class="col-md-6">
              <div class="divslidetxt wow fadeInRight"> <img src="<?php echo base_url(); ?>/assets/images/about.png " class="img-responsive center-block "> </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="bigleap">
    <section class="service-sect">
      <div class="container">
        <h3 class="main-title wow fadeInUp">Take a Look at<br>
          What We Are Into<br>
        </h3>
        <p class="main-descr wow fadeInUp"> BigLeap is a comprehensive workforce centre, connecting resources for employment, career development, skill education and training services. Our array of services including job referrals, skill assessment, career counselling, internships, project training and such are spearheaded by talented, industry and subject experts. </p>
        <div class="row">
          <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="box-sect wow fadeInLeft"> <span class="boxspan wow fadeInUp"><i class="fa fa-briefcase" aria-hidden="true"></i></span>
              <h4 class="heading wow fadeInUp">Jobs</h4>
              <p class="detail wow fadeInUp">Helping job seekers and talent hunters find the right picks.</p>
              <a href="#" class="cmn-link wow fadeInUp">Read More</a> </div>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="box-sect wow fadeInUp"> <span class="boxspan boxspan-1 wow fadeInUp"><i class="fa fa-book" aria-hidden="true"></i></span>
              <h4 class="heading wow fadeInUp">Internship</h4>
              <p class="detail wow fadeInUp">Internship opportunities in Web Development and Digital Marketing, Hardware and Networking.</p>
              <a href="#" class="cmn-link wow fadeInUp">Read More</a> </div>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="box-sect wow fadeInUp"> <span class="boxspan boxspan-2 wow fadeInUp"><i class="fa fa-book" aria-hidden="true"></i></span>
              <h4 class="heading wow fadeInUp">Project training </h4>
              <p class="detail wow fadeInUp">Project training and assistance for B-Tech and MCA students by subject and industry experts. </p>
              <a href="#" class="cmn-link wow fadeInUp">Read More</a> </div>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="box-sect wow fadeInRight"> <span class="boxspan boxspan-3 wow fadeInUp"><i class="fa fa-book" aria-hidden="true"></i></span>
              <h4 class="heading wow fadeInUp">Skill development</h4>
              <p class="detail wow fadeInUp">Nine skills that would help candidates with better career paths and opportunities.</p>
              <a href="#" class="cmn-link wow fadeInUp">Read More</a> </div>
          </div>
        </div>
      </div>
    </section>
    <section class="bigleap-intro">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-12"><img src="<?php echo base_url(); ?>/assets/images/big-intro.png" class="wow fadeInLeft img-responsive"></div>
          <div class="col-lg-6 col-md-6 col-sm-12">
            <h3 class="main-title wow fadeInUp">Leap into a Big Better Career</h3>
            <p class="wow fadeInUp">We are a team of proficient and trained recruitment consultants that strives towards providing the companies with the best available talent and offers you the ability to work on every job requisition you get to your full potential. We are one of the best recruitment consultants in the market today. Our consulting team aims to help clients adopt HR best practices.  We engage your organisation to understand your needs, formulate strategies to address your challenges in workforce administration, HR Development, Talent Management, Recruitment. </p>
            <h4 class="main-title wow fadeInUp">Be a Leader, Not a Follower</h4>
            <p class="wow fadeInUp">Apart from our HR and RPO services, we are committed to enhance employability through skill development initiatives and all our efforts are directed towards accomplishing it. Our recent partnership with Retailers Association’s skill council of India strengthens the commitment.</p>
          </div>
        </div>
      </div>
    </section>
    <section class="about-sect padd-zero">
      <div class="container-fluid padd-zero">
        <div class="col-md-6 col-sm-12 padd-zero">
          <div class="half-box employer-box text-center wow fadeInLeft" style="background-image:url(../images/vission.png); background-size: 100% 100%">
            <h2 class="wow fadeInUp">Our Mission</h2>
            <p class="wow fadeInUp">We are a team of proficient and trained recruitment consultants that strives towards providing the companies with the best available talent and offers you the ability to work on every job requisition you get to your full potential. We are one of the best recruitment consultants in the market today.</p>
            <!-- <a href="#" class="cmn-link animatable bounceIn">Read More</a> --></div>
        </div>
        <div class="col-md-6 col-sm-12 padd-zero">
          <div class="half-box candidate-box text-center wow fadeInRight" style="background-image: url(../images/abt-mission.png); background-size: 100% 100%">
            <h2 class="wow fadeInUp">Our Vision</h2>
            <p class="wow fadeInUp">To be the most admired Employment Solutions and Skill Development Partner in this Great Nation, India. To piolet the Employability Enhancement project in Kerala by leveraging the partnership with Government of Kerala enterprise. To improve the present hit ratio of 5% to 15% for our</p>
            <!-- <a href="#" class="cmn-link animatable bounceIn">Read More</a> --></div>
        </div>
      </div>
    </section>
    
   <section class="job-seekers">
      <div class="container">
        <h3 class="main-title wow fadeInUp">Available Jobs</h3>
        <p class="main-descr wow fadeInUp">The first is a non technical method which requires the use of adware removal software. Download free adware and spyware removal software and use advanced tools getting infected.</p>
        <div class="sekers-main">
          <div class="row">
            <div class="table-responsive wow fadeInUp">
              <table class="table table-lg table-hover">
                <thead>
                  <tr>
                    <th>Name</th>
                    <th>Location</th>
                    <th>Qualification</th>
                    <th>Salary</th>
                    <th>Apply</th>
                  </tr>
                </thead>
                <tbody class="wow fadeInUp">
                    <?php
                        $count=1;
                        foreach($job_list as $u){
                        ?>
                  <tr>
                    <td><a href="<?php echo base_url('job-details');?>"><img src="<?php echo base_url('upload/logo/'.$u['logo']);?>" style="border-radius:50%;" class="jeeker-img" alt="Software Development"><?php echo $u['job_title']; ?></a></td>
                    <td><i class="fa fa-map-marker" aria-hidden="true"></i> <?php echo $u['location']; ?> </td>
                    <td><div class="job-type full-time"><?php echo $u['qualification']; ?></div></td>
                    <td><i class="fa fa-money" aria-hidden="true" style="margin-right: 5px"></i><?php echo $u['salary']; ?></td>
                    <td><a href="<?php echo base_url('job-details/'.$u['job_id']);?>" data-toggle="modal" data-target="#apply-job" class="apply-job-btn btn btn-radius theme-btn">Apply</a></td>
                  </tr>
                  <?php $count++;} ?>
                  
                  <!--<tr>-->
                  <!--  <td><a href="job-details.html"><img src="assets/images/job-sek-02.png" class="jeeker-img" alt="Web Developer">Web Developer</a></td>-->
                  <!--  <td><i class="fa fa-map-marker" aria-hidden="true"></i> Any Location</td>-->
                  <!--  <td><div class="job-type full-time">B-Tech</div></td>-->
                  <!--  <td><i class="fa fa-money" aria-hidden="true" style="margin-right: 5px"></i>2.0-2.4 LAKHS</td>-->
                  <!--  <td><a href="#" data-toggle="modal" data-target="#apply-job" class="apply-job-btn btn btn-radius theme-btn">Apply</a></td>-->
                  <!--</tr>-->
                  
                  <!--<tr>-->
                  <!--  <td><a href="job-details.html"><img src="assets/images/job-sek-03.png" class="jeeker-img" alt="PHP Developer">Backend Developer</a></td>-->
                  <!--  <td><i class="fa fa-map-marker" aria-hidden="true"></i> Any Location </td>-->
                  <!--  <td><div class="job-type full-time">B-Tech</div></td>-->
                  <!--  <td><i class="fa fa-money" aria-hidden="true" style="margin-right: 5px"></i> 2.0-2.4 LAKHS</td>-->
                  <!--  <td><a href="#" data-toggle="modal" data-target="#apply-job" class="apply-job-btn btn btn-radius theme-btn">Apply</a></td>-->
                  <!--</tr>-->
                  <!--<tr>-->
                  <!--  <td><a href="job-details.html"><img src="assets/images/job-sek-04.png" class="jeeker-img" alt="Marketing Executive">App Developer</a></td>-->
                  <!--  <td><i class="fa fa-map-marker" aria-hidden="true"></i> Any Location</td>-->
                  <!--  <td><div class="job-type full-time">B-Tech</div></td>-->
                  <!--  <td><i class="fa fa-money" aria-hidden="true" style="margin-right: 5px"></i> 2.0-2.4 LAKHS</td>-->
                  <!--  <td><a href="#" data-toggle="modal" data-target="#apply-job" class="apply-job-btn btn btn-radius theme-btn">Apply</a></td>-->
                  <!--</tr>-->
                  <!--<tr>-->
                  <!--  <td><a href="job-details.html"><img src="assets/images/job-sek-05.png" class="jeeker-img" alt="Graphic Designer">Software Engineering Trainee</a></td>-->
                  <!--  <td><i class="fa fa-map-marker" aria-hidden="true"></i> Hyderabad</td>-->
                  <!--  <td><div class="job-type full-time">B-Tech</div></td>-->
                  <!--  <td><i class="fa fa-money" aria-hidden="true" style="margin-right: 5px"></i> 2.4 Lakhs (Initial)</td>-->
                  <!--  <td><a href="#" data-toggle="modal" data-target="#apply-job" class="apply-job-btn btn btn-radius theme-btn">Apply</a></td>-->
                  <!--</tr>-->
                </tbody>
              </table>
              <nav class="pagination-container">
                <div class="pagination wow fadeInUp"> <a class="pagination-newer wow fadeInUp" href="#"><i class="fa fa-step-backward" aria-hidden="true"></i></a> <span class="pagination-inner"> <a class="pagination-active wow fadeInUp" href="#">1</a> <a class="wow fadeInUp" href="#">2</a> <a class="wow fadeInUp" href="#">3</a> <a class="wow fadeInUp" href="#">4</a> <a class="wow fadeInUp" href="#">5</a> <a class="wow fadeInUp" href="#">6</a> </span> <a class="pagination-older wow fadeInUp" href="#"><i class="fa fa-step-forward" aria-hidden="true"></i></a> </div>
              </nav>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="category-sect">
      <div class="container">
        <h3 class="main-title wow fadeInUp">Browse By Category</h3>
        <p class="main-descr wow fadeInUp">Each Month, More Than 7 Million Jobhunt Turn To Website In Their Search For Work, Making Over<br>
          160,000 Applications Every Day.</p>
        <div class="catgry-main">
          <div class="row">
            <div class="col-md-3">
              <div class="catgry-sub wow fadeInUp">
                <div class="category-icon wow fadeInUp"> <i class="fa fa-laptop" aria-hidden="true"></i> <i class="fa fa-laptop hide-icon" aria-hidden="true"></i> </div>
                <a href="browse-jobs.html" class="wow fadeInUp" title=""> Web &amp; Graphics </a>
                <p class="wow fadeInUp">122 Jobs</p>
              </div>
            </div>
            <div class="col-md-3">
              <div class="catgry-sub wow fadeInUp">
                <div class="category-icon wow fadeInUp"> <i class="fa fa-briefcase" aria-hidden="true"></i> <i class="fa fa-briefcase hide-icon" aria-hidden="true"></i> </div>
                <a href="browse-jobs.html" class="wow fadeInUp" title="">Business</a>
                <p class="wow fadeInUp">122 Jobs</p>
              </div>
            </div>
            <div class="col-md-3">
              <div class="catgry-sub wow fadeInUp">
                <div class="category-icon wow fadeInUp"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> <i class="fa fa-pencil-square-o hide-icon" aria-hidden="true"></i> </div>
                <a href="browse-jobs.html" class="wow fadeInUp" title="">Education Training</a>
                <p class="wow fadeInUp">122 Jobs</p>
              </div>
            </div>
            <div class="col-md-3">
              <div class="catgry-sub wow fadeInUp">
                <div class="category-icon wow fadeInUp"> <i class="fa fa-cutlery" aria-hidden="true"></i> <i class="fa fa-cutlery hide-icon" aria-hidden="true"></i> </div>
                <a href="browse-jobs.html" class="wow fadeInUp" title="">Restaurant &amp; Food</a>
                <p class="wow fadeInUp">122 Jobs</p>
              </div>
            </div>
            <div class="col-md-3">
              <div class="catgry-sub wow fadeInUp">
                <div class="category-icon wow fadeInUp"> <i class="fa fa-cogs" aria-hidden="true"></i> <i class="fa fa-cogs hide-icon" aria-hidden="true"></i> </div>
                <a href="browse-jobs.html" class="wow fadeInUp" title="">Automotive Jobs</a>
                <p class="wow fadeInUp">122 Jobs</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
   
    <section class="home-gallery events">
    <div class="container">
     <h3 class="main-title wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">Previous Job Fair History</h3>
      <p class="main-descr wow fadeInUp">No event is too small or irrelevant. We plan, design and execute each of the events keeping your future in mind. Each event will benefit you in terms of building your career and personal growth. Get benefited.</p>
      <div class="order-div" id="main-order">
        <div class="row">
          <div class="col-md-3 col-sm-6 col-lg-3 col-xs-6">
            <div class="folder-link wow fadeInUp" id="9-div">
                 <a href="events-2020.html"><img src="<?php echo base_url(); ?>/assets/images/folder-09.png" class="img-responsive"></a>
              <h3>2019 - 2020</h3>
            </div>
          </div>
          <div class="col-md-3 col-sm-6 col-lg-3 col-xs-6">
            <div class="folder-link wow fadeInUp" id="8-div">
                <a href="events-2018.html"><img src="<?php echo base_url(); ?>/assets/images/folder-08.png" class="img-responsive"></a>
              <h3>2017 - 2018</h3>
            </div>
          </div>
          <div class="col-md-3 col-sm-6 col-lg-3 col-xs-6">
            <div class="folder-link wow fadeInUp" id="7-div">
                <a href="events-2015.html"><img src="<?php echo base_url(); ?>/assets/images/folder-07.png" class="img-responsive"></a>
              <h3>2014 - 2015</h3>
            </div>
          </div>
          <div class="col-md-3 col-sm-6 col-lg-3 col-xs-6">
            <div class="folder-link wow fadeInUp" id="6-div">
                <a href="events-2014.html"><img src="<?php echo base_url(); ?>/assets/images/folder-06.png" class="img-responsive"></a>
              <h3>2013 - 2014</h3>
            </div>
          </div>
          <div class="col-md-3 col-sm-6 col-lg-3 col-xs-6">
            <div class="folder-link wow fadeInUp" id="5-div">
                <a href="events-2013.html"><img src="<?php echo base_url(); ?>/assets/images/folder-05.png" class="img-responsive"></a>
              <h3>2012 - 2013</h3>
            </div>
          </div>
          <div class="col-md-3 col-sm-6 col-lg-3 col-xs-6">
            <div class="folder-link wow fadeInUp" id="4-div">
                <a href="events-2011.html"><img src="<?php echo base_url(); ?>/assets/images/folder-04.png" class="img-responsive"></a>
              <h3>2010 - 20011</h3>
            </div>
          </div>
          <div class="col-md-3 col-sm-6 col-lg-3 col-xs-6">
            <div class="folder-link wow fadeInUp" id="3-div">
               <a href="events-2006.html"><img src="<?php echo base_url(); ?>/assets/images/folder-03.png" class="img-responsive"></a>
              <h3>2005 - 2006</h3>
            </div>
          </div>
          <div class="col-md-3 col-sm-6 col-lg-3 col-xs-6">
            <div class="folder-link wow fadeInUp" id="2-div">
                <a href="events-2005.html"><img src="<?php echo base_url(); ?>/assets/images/folder-02.png" class="img-responsive"></a>
              <h3>2004 - 2005</h3>
            </div>
          </div>
          <div class="col-md-3 col-sm-6 col-lg-3 col-xs-6">
            <div class="folder-link wow fadeInUp" id="1-div">
                <a href="events-2004.html"><img src="<?php echo base_url(); ?>/assets/images/folder-01.png" class="img-responsive"></a>
              <h3>2003 - 2004</h3>
            </div>
          </div>
        </div>
      </div>
      </div>
    </section>
    <section class="client-sect">
      <h3 class="main-title wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">Our Clientele Includes <br>
        the Most Wanted</h3>
      <p class="main-descr wow fadeInUp">A wide portfolio of satisfied and reliable clients who stand a testimony to the service<br>
        we render to the industry and the quality practices we adhere to. </p>
      <div class="container">
        <div class="owl-carousel owl-theme mobilesliderpro" id="carousel01">
          <div class="item wow fadeInUp"><a href="#"><img src="<?php echo base_url(); ?>/assets/images/client-03.png" class="img-responsive"></a></div>
          <div class="item wow fadeInUp"><a href="#"><img src="<?php echo base_url(); ?>/assets/images/client-04.png" class="img-responsive"></a></div>
          <div class="item wow fadeInUp"><a href="#"><img src="<?php echo base_url(); ?>/assets/images/client-06.png" class="img-responsive"></a></div>
        </div>
      </div>
    </section>
    
    <!-- <section class="blog-sect">
      <div class="container">
        <h3 class="main-title wow fadeInUp">All What You Would Want to Know <br>
           </h3>
        <p class="main-descr wow fadeInUp">There’s a lot to read, a lot to know. Pick your favourite pieces. Read what interests you and help you<br> throughout your career development.  </p>
        <div class="row">
          <div class="col-md-4 col-sm-4">
            <div class="blog-mian wow fadeInUp">
              <div class="blog-img wow fadeInUp"><a href="#"><img src="images/blog-01.png" class="img-responsive"></a></div>
              <div class="bolg-body">
                <h5 class="wow fadeInUp">20 - December - 2019</h5>
                <h4 class="wow fadeInUp"><a href="#">Motivation And Your Personal Vision An Force</a></h4>
                <ul class="clearfix">
                  <li class="wow fadeInUp"><a href="#"><i class="fa fa-eye" aria-hidden="true"></i>views</a></li>
                  <li class="wow fadeInUp"><a href="#"><i class="fa fa-commenting" aria-hidden="true"></i>10</a></li>
                  <li class="wow fadeInUp"><a href="#"><i class="fa fa-share-alt" aria-hidden="true"></i>320</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="col-md-4 col-sm-4">
            <div class="blog-mian wow fadeInUp">
              <div class="blog-img wow fadeInUp"><a href="#"><img src="images/blog-02.png" class="img-responsive"></a></div>
              <div class="bolg-body">
                <h5 class="wow fadeInUp">22 - December - 2019</h5>
                <h4 class="wow fadeInUp"><a href="#">Benjamin Franklin S Method Of Habit Formation</a></h4>
                <ul class="clearfix">
                  <li class="wow fadeInUp"><a href="#"><i class="fa fa-eye" aria-hidden="true"></i>views</a></li>
                  <li class="wow fadeInUp"><a href="#"><i class="fa fa-commenting" aria-hidden="true"></i>10</a></li>
                  <li class="wow fadeInUp"><a href="#"><i class="fa fa-share-alt" aria-hidden="true"></i>320</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="col-md-4 col-sm-4">
            <div class="blog-mian wow fadeInUp">
              <div class="blog-img wow fadeInUp"><a href="#"><img src="images/blog-03.png" class="img-responsive"></a></div>
              <div class="bolg-body">
                <h5 class="wow fadeInUp">25 - December - 2019</h5>
                <h4 class="wow fadeInUp"><a href="#">How To Set Intentions That Energize You</a></h4>
                <ul class="clearfix">
                  <li class="wow fadeInUp"><a href="#"><i class="fa fa-eye" aria-hidden="true"></i>views</a></li>
                  <li class="wow fadeInUp"><a href="#"><i class="fa fa-commenting" aria-hidden="true"></i>10</a></li>
                  <li class="wow fadeInUp"><a href="#"><i class="fa fa-share-alt" aria-hidden="true"></i>320</a></li>
                </ul>
              </div>
            </div>
          </div>
          <a href="#" class="cmn-link animatable bounceIn animate-in">Load More articles</a> </div>
      </div>
    </section>
    <section class="testimonial-sect">
      <div class="">
        <h3 class="main-title wow fadeInUp">Here’s What People Said</h3>
        <p class="main-descr wow fadeInUp">Views or reviews as you call them, here’s what people had to say about us - <br>what we were to them and what we are. Need we say any more?</p>
        <div class="owl-carousel owl-theme mobilesliderpro" id="carousel02">
          <div class="item wow fadeInUp">
            <div class="test-main">
              <div class="test-text">
                <p>"By having a bigger purpose, it just might be that Avira's building a 21st century design studio: the crucible of big, world-changing ideas."</p>
                <div class="rating rating2"> <a href="#1" title="Give 1 star">★</a> <a href="#2" title="Give 2 stars">★</a> <a href="#3" title="Give 3 stars">★</a> <a href="#4" title="Give 4 stars">★</a> <a href="#5" title="Give 5 stars">★</a> </div>
              </div>
              <ul class="clearfix">
                <li class="">
                  <div class="testi-img"><img src="images/testi-01.png" class="img-responsive center-block"></div>
                </li>
                <li class=""> <span class="testi-name">ROBERT SMITH</span> <span class="test-desig">web developer</span> </li>
              </ul>
            </div>
          </div>
          <div class="item wow fadeInUp">
            <div class="test-main">
              <div class="test-text">
                <p>"By having a bigger purpose, it just might be that Avira's building a 21st century design studio: the crucible of big, world-changing ideas."</p>
                <div class="rating rating2"> <a href="#1" title="Give 1 star">★</a> <a href="#2" title="Give 2 stars">★</a> <a href="#3" title="Give 3 stars">★</a> <a href="#4" title="Give 4 stars">★</a> <a href="#5" title="Give 5 stars">★</a> </div>
              </div>
              <ul class="clearfix">
                <li class="">
                  <div class="testi-img"><img src="images/testi-02.png" class="img-responsive center-block"></div>
                </li>
                <li class=""> <span class="testi-name">ROBERT SMITH</span> <span class="test-desig">web developer</span> </li>
              </ul>
            </div>
          </div>
          <div class="item wow fadeInUp">
            <div class="test-main">
              <div class="test-text">
                <p>"By having a bigger purpose, it just might be that Avira's building a 21st century design studio: the crucible of big, world-changing ideas."</p>
                <div class="rating rating2"> <a href="#1" title="Give 1 star">★</a> <a href="#2" title="Give 2 stars">★</a> <a href="#3" title="Give 3 stars">★</a> <a href="#4" title="Give 4 stars">★</a> <a href="#5" title="Give 5 stars">★</a> </div>
              </div>
              <ul class="clearfix">
                <li class="">
                  <div class="testi-img"><img src="images/testi-01.png" class="img-responsive center-block"></div>
                </li>
                <li class=""> <span class="testi-name">ROBERT SMITH</span> <span class="test-desig">web developer</span> </li>
              </ul>
            </div>
          </div>
          <div class="item wow fadeInUp">
            <div class="test-main">
              <div class="test-text">
                <p>"By having a bigger purpose, it just might be that Avira's building a 21st century design studio: the crucible of big, world-changing ideas."</p>
                <div class="rating rating2"> <a href="#1" title="Give 1 star">★</a> <a href="#2" title="Give 2 stars">★</a> <a href="#3" title="Give 3 stars">★</a> <a href="#4" title="Give 4 stars">★</a> <a href="#5" title="Give 5 stars">★</a> </div>
              </div>
              <ul class="clearfix">
                <li class="">
                  <div class="testi-img"><img src="images/testi-02.png" class="img-responsive center-block"></div>
                </li>
                <li class=""> <span class="testi-name">ROBERT SMITH</span> <span class="test-desig">web developer</span> </li>
              </ul>
            </div>
          </div>
          <div class="item wow fadeInUp">
            <div class="test-main">
              <div class="test-text">
                <p>"By having a bigger purpose, it just might be that Avira's building a 21st century design studio: the crucible of big, world-changing ideas."</p>
                <div class="rating rating2"> <a href="#1" title="Give 1 star">★</a> <a href="#2" title="Give 2 stars">★</a> <a href="#3" title="Give 3 stars">★</a> <a href="#4" title="Give 4 stars">★</a> <a href="#5" title="Give 5 stars">★</a> </div>
              </div>
              <ul class="clearfix">
                <li class="">
                  <div class="testi-img"><img src="images/testi-01.png" class="img-responsive center-block"></div>
                </li>
                <li class=""> <span class="testi-name">ROBERT SMITH</span> <span class="test-desig">web developer</span> </li>
              </ul>
            </div>
          </div>
          <div class="item wow fadeInUp">
            <div class="test-main">
              <div class="test-text">
                <p>"By having a bigger purpose, it just might be that Avira's building a 21st century design studio: the crucible of big, world-changing ideas."</p>
                <div class="rating rating2"> <a href="#1" title="Give 1 star">★</a> <a href="#2" title="Give 2 stars">★</a> <a href="#3" title="Give 3 stars">★</a> <a href="#4" title="Give 4 stars">★</a> <a href="#5" title="Give 5 stars">★</a> </div>
              </div>
              <ul class="clearfix">
                <li class="">
                  <div class="testi-img"><img src="images/testi-02.png" class="img-responsive center-block"></div>
                </li>
                <li class=""> <span class="testi-name">ROBERT SMITH</span> <span class="test-desig">web developer</span> </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </section>-->
    
<!--     <section class="newsletter">
      <div class="container">
        <h3 class="main-title wow fadeInUp"> Reaching Us is Never Too Hard<br>
        </h3>
        <p class="main-descr wow fadeInUp">We would like to hear from you, be it a query, a request or a review. Drop a message or drop in on us. <br>
          The choice and convenience are yours.</p>
        <div class="row">
          <div class="col-md-7">
            <div class="map-sect wow fadeInUp">
              <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d15652.141444286224!2d75.79527265000002!3d11.25880905!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sin!4v1576832177948!5m2!1sen!2sin" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
              <div class="map-addres" id="book"> <span class="click wow fadeInUp" id="clickme"><i class="fa fa-times" aria-hidden="true"></i></span>
                <div class="row adrmarg wow fadeInUp">
                  <div class="col-md-1 adds-loca adds-localeft"> <i class="fa fa-map-marker" aria-hidden="true"></i> </div>
                  <div class="col-md-11 adds-loca wow fadeInUp">
                    <h5>BIGLEAP SOLUTIONS (P) LTD.</h5>
                    <h6>4th Floor - Markaz Complex - Mavoor Road - Kozhikode - Kerala</h6>
                  </div>
                </div>
                <div class="row adrmarg wow fadeInUp">
                  <div class="col-md-1 adds-loca adds-localeft wow fadeInUp"> <i class="fa fa-phone" aria-hidden="true"></i> </div>
                  <div class="col-md-11 adds-loca wow fadeInUp">
                    <h5><a href="tel:+91 9061560224">+91 9061560224</a></h5>
                    <h6>Monday to Saturday - 9am to 5 pm</h6>
                  </div>
                </div>
                <div class="row adrmarg wow fadeInUp">
                  <div class="col-md-1 adds-loca adds-localeft wow fadeInUp"> <i class="fa fa-envelope-o" aria-hidden="true"></i> </div>
                  <div class="col-md-11 adds-loca wow fadeInUp">
                    <h5><a href="mailto:hr@bigleaponline.com">hr@bigleaponline.com</a></h5>
                    <h6>Send us your query anytime!</h6>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-5">
            <form class="footer-form">
              <div class="input-set wow fadeInUp">
                <label class="biglabel wow fadeInUp">Your Name</label>
                <input class="input-big wow fadeInUp" placeholder="Enter your Name" type="text">
              </div>
              <div class="input-set wow fadeInUp">
                <label class="biglabel wow fadeInUp">EMAIL ADDRESS</label>
                <input class="input-big wow fadeInUp" placeholder="Enter your Email" type="email">
              </div>
              <div class="input-set wow fadeInUp">
                <label class="biglabel wow fadeInUp">SUBJECT</label>
                <input class="input-big wow fadeInUp" placeholder="Enter Subject" type="text">
              </div>
              <div class="input-set wow fadeInUp">
                <label class="biglabel wow fadeInUp">MESSAGE</label>
                <textarea class="input-big wow fadeInUp input-txt" placeholder="Enter Message"></textarea>
              </div>
              <button class="big-btn wow fadeInUp" type="submit">SUBMIT</button>
            </form>
          </div>
        </div>
      </div>
    </section>
  </div> -->
<!-- </div> -->
<!--content end here--> 
<!-- start javascript file --> 
<script type="application/javascript" src="<?php echo base_url(); ?>/assets/js/jquery.min.js"></script> 
<script type="application/javascript" src="<?php echo base_url(); ?>/assets/js/bootstrap.min.js"></script> 
<script type="application/javascript" src="<?php echo base_url(); ?>/assets/js/owl.carousel.min.js"></script> 
<script type="application/javascript" src="<?php echo base_url(); ?>/assets/js/jquery.touchSwipe.min.js"></script> 
<script type="application/javascript" src="<?php echo base_url(); ?>/assets/js/index.js"></script> 
<script type="application/javascript" src="<?php echo base_url(); ?>/assets/js/wow.min.js"></script> 
<script type="application/javascript" src="<?php echo base_url(); ?>/assets/js/scroll-top.js"></script>
<script type="application/javascript" src="<?php echo base_url(); ?>/assets/js/lightgallery-all.js"></script> 
<script type="application/javascript" src="<?php echo base_url(); ?>/assets/js/mousewheel.js"></script>  
<script>
         $('#carousel01').owlCarousel({
            nav:true,
         	margin:10,
         	loop:true,
			autoplayTimeout:2000,
			 autoplayHoverPause: true,
         	autoplay: true,
            responsive:{
                 0:{
                     items:1
                 },
                 600:{
                     items:3
                 },
                 1000:{
                     items:6
                 }
             }
         });
         $('#carousel02').owlCarousel({
            nav:true,
         	margin:20,
         	loop:true,
         	autoplay: true,
			 autoplayHoverPause: true,
            responsive:{
                 0:{
                     items:1
                 },
                 600:{
                     items:2	
                 },
                 1000:{
                     items:3
                 }
             }
         });
	         $('#carousel05').owlCarousel({
            nav:true,
         	margin:5,
         	loop:true,
         	autoplay: true,
			autoplayHoverPause: true,
            responsive:{
                 0:{
                     items:1
                 },
                 600:{
                     items:2	
                 },
                 1000:{
                     items:4
                 }
             }
         });
         $( "#clickme" ).click(function() {
           $( "#book" ).hide( "slow", function() {
           });
         });
      </script> 
<script>
         jQuery(document).ready(function( $ ) {
           new WOW().init();
         });
         $(window).scroll(function(){
           var sticky = $('.sticky'),
               scroll = $(window).scrollTop();
           if (scroll >= 36) sticky.addClass('fixed');
           else sticky.removeClass('fixed');
         });
      </script> 
<script>
	   $(document).ready(function ()
{
	$("#bkgOverlay").delay(4800).fadeIn(400);
	$("#delayedPopup").delay(5000).fadeIn(400);
	$("#btnClose").click(function (e)
	{
		HideDialog();
		e.preventDefault();
	});
});
function HideDialog()
{
	$("#bkgOverlay").fadeOut(400);
	$("#delayedPopup").fadeOut(300);
}
       $('.pagination-inner a').on('click', function() {
		$(this).siblings().removeClass('pagination-active');
		$(this).addClass('pagination-active');
})
         $( "#clickme" ).click(function() {
           $( "#book" ).hide( "slow", function() {
           });
         });
	
$(function() {
  $(".HeartAnimation").click(function() {
    $(this).toggleClass("animate");
  });
});
		$(document).ready(function() {
  $('#lightgallery').lightGallery({
    pager: true
  });
});
	   </script> 
<!-- end javascript file -->
</body>
</html>