
<!--content start here-->
<div class="banner-slide">
  <div class="slidersection about-page">
    <div class="common-titles">
      <h2 class="header-page-subtitle wow fadeInUp"> We are more than an consultancy </h2>
      <h1 class="header-page-title wow fadeInUp">Login</h1>
    </div>
  </div>
</div>
<div class="bigleap bg-login">
<div class="login-base">


<section>
  <header>
<h1>Log in</h1>
  </header>
  <main>
  <?php 
  if($this->session->flashdata('loginerror'))
  {
    echo '<div class="alert alert-danger" role="alert">'.$this->session->flashdata('loginerror').'</div>';   
  }
?>
<form action="<?php echo base_url();?>main_controller/login" method="post">
  <label for=""><i class="fa fa-user"></i><input type="text" name="username" value="" placeholder="User Name"></label>


  <label for=""><i class="fa fa-lock"></i><input type="password" name="password" value="" placeholder="Password"></label>

  <input type="submit" name="" value="Submit">
</form>

  </main>
  <footer class="clearfix">
      <a href="">Forgot your password?</a>
   <h2> Don't have an account?  <a href="registration">Sign up</a></h2>
  </footer>
</section>

</div>

</div>
<!--content end here--> 
<a href="javascript:void(0);" id="rocketmeluncur" class="showrocket" ><i></i></a> 
<!--footer start-->

<!--footer ends--> 
<!-- start javascript file --> 
<script type="application/javascript" src="<?php echo base_url(); ?>/assets/js/jquery.min.js"></script> 
<script type="application/javascript" src="<?php echo base_url(); ?>/assets/js/bootstrap.min.js"></script> 
<script type="application/javascript" src="<?php echo base_url(); ?>/assets/js/index.js"></script> 
<script type="application/javascript" src="<?php echo base_url(); ?>/assets/js/wow.min.js"></script> 
<script type="application/javascript" src="<?php echo base_url(); ?>/assets/js/scroll-top.js"></script> 
<script type="application/javascript" src="<?php echo base_url(); ?>/assets/js/login-script.js"></script> 
<script>
         jQuery(document).ready(function( $ ) {
           // Initiate the wowjs animation library
           new WOW().init();
         });
         $(window).scroll(function(){
           var sticky = $('.sticky'),
               scroll = $(window).scrollTop();
           if (scroll >= 36) sticky.addClass('fixed');
           else sticky.removeClass('fixed');
         });
      </script> 
      <script>
      </script>
<!-- end javascript file -->
</body>
</html>