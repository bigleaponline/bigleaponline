
<!--content start here-->
<div class="banner-slide">
  <div class="slidersection about-page">
    <div class="common-titles">
      <h2 class="header-page-subtitle wow fadeInUp"> We are more than an consultancy </h2>
      <h1 class="header-page-title wow fadeInUp">2010-2011</h1>
    </div>
  </div>
</div>
<div class="bigleap">
  <section class="service-sect">
    <div class="container">
      <h3 class="main-title wow fadeInUp"> Events of 2010-2011</h3>
      <p class="main-descr wow fadeInUp">Our specialized RPO services are designed to effectively handle the entire recruitment process of our clients focusing on speed of delivery, quality of talent &amp; accountability. We are well equipped with talented consultants having varied industry knowledge &amp; expertise to execute complete recruitment assignments of the clients.</p>
      	<div class="events-one">
      			
      		<div class="cont">
 
  <div class="demo-gallery wow fadeInUp">
    <ul id="lightgallery">
      <li data-responsive="images/2011/2011 Job Fair_Announcements.JPG" data-src="images/2011/2011 Job Fair_Announcements.JPG"
      
      data-sub-html="<h4>Job Fair_Announcements</h4><p>2011 Job Fair_Announcements</p>" data-pinterest-text="Pin it" data-tweet-text="share on twitter ">
       
        <a href="">
          <img class="img-responsive" src="images/2011/2011 Job Fair_Announcements.JPG">
          <div class="demo-gallery-poster">
            <img src="images/serchicon-event.png">
          </div>
        </a>
      </li>
      <li data-responsive="images/2011/2011 Job Fair_ Reg Counter (Copy).JPG" data-src="images/2011/2011 Job Fair_ Reg Counter (Copy).JPG"
      data-sub-html="<h4>2011 Job Fair</h4><p>2011 Job Fair_ Reg Counter</p>" data-pinterest-text="Pin it" data-tweet-text="share on twitter ">
        <a href="">
          <img class="img-responsive" src="images/2011/2011 Job Fair_ Reg Counter (Copy).JPG">
          <div class="demo-gallery-poster">
            <img src="images/serchicon-event.png">
          </div>
        </a>
      </li>
      <li data-responsive="images/2011/2011 Job Fair_BigLeap MD Mr.Soji Abraham giving directions (Copy).JPG" data-src="images/2011/2011 Job Fair_BigLeap MD Mr.Soji Abraham giving directions (Copy).JPG"
      data-sub-html="<h4>2011 Job Fair</h4><p>2011 Job Fair_BigLeap MD Mr.Soji Abraham giving directions</p>" data-pinterest-text="Pin it" data-tweet-text="share on twitter ">
        <a href="">
          <img class="img-responsive" src="images/2011/2011 Job Fair_BigLeap MD Mr.Soji Abraham giving directions (Copy).JPG">
          <div class="demo-gallery-poster">
            <img src="images/serchicon-event.png">
          </div>
        </a>
      </li>
      <li data-responsive="images/2011/2011 Job Fair_Inaugural Function (Copy).JPG" data-src="images/2011/2011 Job Fair_Inaugural Function (Copy).JPG"
      data-sub-html="<h4>2011 Job Fair_Inaugural Function</h4><p>2011 Job Fair_Inaugural Function</p>" data-pinterest-text="Pin it" data-tweet-text="share on twitter ">
        <a href="">
          <img class="img-responsive" src="images/2011/2011 Job Fair_Inaugural Function (Copy).JPG">
          <div class="demo-gallery-poster">
            <img src="images/serchicon-event.png">
          </div>
        </a>
      </li>
      <li data-responsive="images/2011/2011 Job Fair_Mayor Arriving for Inauguration (Copy).JPG" data-src="images/2011/2011 Job Fair_Mayor Arriving for Inauguration (Copy).JPG"
      data-sub-html="<h4>2011 Job Fair</h4><p>2011 Job Fair_Mayor Arriving for Inauguration</p>" data-pinterest-text="Pin it" data-tweet-text="share on twitter ">
        <a href="">
          <img class="img-responsive" src="images/2011/2011 Job Fair_Mayor Arriving for Inauguration (Copy).JPG">
          <div class="demo-gallery-poster">
            <img src="images/serchicon-event.png">
          </div>
        </a>
      </li>
      <li data-responsive="images/2011/2011 Job Fair_Registration Candidates (Copy).JPG" data-src="images/2011/2011 Job Fair_Registration Candidates (Copy).JPG"
      data-sub-html="<h4>2011 Job Fair</h4><p>2011 Job Fair_Registration Candidates</p>" data-pinterest-text="Pin it" data-tweet-text="share on twitter ">
        <a href="">
          <img class="img-responsive" src="images/2011/2011 Job Fair_Registration Candidates (Copy).JPG">
          <div class="demo-gallery-poster">
            <img src="images/serchicon-event.png">
          </div>
        </a>
      </li>
    </ul>
  </div>
</div>
      	</div>
    </div>
  </section>
  
</div>
<!--content end here--> 
