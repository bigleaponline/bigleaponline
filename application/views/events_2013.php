
<!--content start here-->
<div class="banner-slide">
  <div class="slidersection about-page">
    <div class="common-titles">
      <h2 class="header-page-subtitle wow fadeInUp"> We are more than an consultancy </h2>
      <h1 class="header-page-title wow fadeInUp">2012-2013</h1>
    </div>
  </div>
</div>
<div class="bigleap">
  <section class="service-sect">
    <div class="container">
      <h3 class="main-title wow fadeInUp"> Events of 2012-2013</h3>
      <p class="main-descr wow fadeInUp">Our specialized RPO services are designed to effectively handle the entire recruitment process of our clients focusing on speed of delivery, quality of talent &amp; accountability. We are well equipped with talented consultants having varied industry knowledge &amp; expertise to execute complete recruitment assignments of the clients.</p>
      	<div class="events-one">
      			
      		<div class="cont">
 
  <div class="demo-gallery wow fadeInUp">
    <ul id="lightgallery">
      <li data-responsive="images/IBM-2013/DSCN0138.JPG" data-src="images/IBM-2013/DSCN0138.JPG"
      
      data-sub-html="<h4>2013 Job Fair</h4><p>2013 Job Fair_Announcements</p>" data-pinterest-text="Pin it" data-tweet-text="share on twitter ">
       
        <a href="">
          <img class="img-responsive" src="images/IBM-2013/DSCN0138.JPG">
          <div class="demo-gallery-poster">
            <img src="images/serchicon-event.png">
          </div>
        </a>
      </li>
      <li data-responsive="images/IBM-2013/DSCN0117.JPG" data-src="images/IBM-2013/DSCN0117.JPG"
      data-sub-html="<h4>2013 Job Fair</h4><p>2013 Job Fair</p>" data-pinterest-text="Pin it" data-tweet-text="share on twitter ">
        <a href="">
          <img class="img-responsive" src="images/IBM-2013/DSCN0117.JPG">
          <div class="demo-gallery-poster">
            <img src="images/serchicon-event.png">
          </div>
        </a>
      </li>
      <li data-responsive="images/IBM-2013/DSCN0132.JPG" data-src="images/IBM-2013/DSCN0132.JPG"
      data-sub-html="<h4>2011 Job Fair</h4><p>2011 Job Fair_BigLeap -IBM</p>" data-pinterest-text="Pin it" data-tweet-text="share on twitter ">
        <a href="">
          <img class="img-responsive" src="images/IBM-2013/DSCN0132.JPG">
          <div class="demo-gallery-poster">
            <img src="images/serchicon-event.png">
          </div>
        </a>
      </li>
    <li data-responsive="images/IBM-2013/DSCN0152.JPG" data-src="images/IBM-2013/DSCN0152.JPG"
      data-sub-html="<h4>2013 Job Fair</h4><p>2013 Job Fair_Inaugural Function</p>" data-pinterest-text="Pin it" data-tweet-text="share on twitter ">
        <a href="">
          <img class="img-responsive" src="images/IBM-2013/DSCN0152.JPG">
          <div class="demo-gallery-poster">
            <img src="images/serchicon-event.png">
          </div>
        </a>
      </li>
      <li data-responsive="images/IBM-2013/DSCN0144.JPG" data-src="images/IBM-2013/DSCN0144.JPG"
      data-sub-html="<h4>2013 Job Fair</h4><p>2013 Job Fair</p>" data-pinterest-text="Pin it" data-tweet-text="share on twitter ">
        <a href="">
          <img class="img-responsive" src="images/IBM-2013/DSCN0144.JPG">
          <div class="demo-gallery-poster">
            <img src="images/serchicon-event.png">
          </div>
        </a>
      </li>

    </ul>
  </div>
</div>
      	</div>
    </div>
  </section>
  
</div>
<!--content end here--> 
