
<!--content start here-->
<div class="banner-slide">
  <div class="slidersection about-page">
    <div class="common-titles">
      <h2 class="header-page-subtitle wow fadeInUp"> We are more than an consultancy </h2>
      <h1 class="header-page-title wow fadeInUp">2003-2004</h1>
    </div>
  </div>
</div>
<div class="bigleap">
  <section class="service-sect">
    <div class="container">
      <h3 class="main-title wow fadeInUp"> Events of 2003-2004</h3>
      <p class="main-descr wow fadeInUp">Our specialized RPO services are designed to effectively handle the entire recruitment process of our clients focusing on speed of delivery, quality of talent &amp; accountability. We are well equipped with talented consultants having varied industry knowledge &amp; expertise to execute complete recruitment assignments of the clients.</p>
      <div class="events-one">
        <div class="cont">
          <div class="demo-gallery wow fadeInUp">
            <ul id="lightgallery">
              <li data-responsive="images/2004/BigLeap Job Fair on Dec 2004.jpg" data-src="images/2004/BigLeap Job Fair on Dec 2004.jpg"
      
      data-sub-html="<h4>BigLeap Job Fair on Dec 2004</h4><p>BigLeap Job Fair on Dec 2004 - Paerticipants</p>" data-pinterest-text="Pin it" data-tweet-text="share on twitter "> <a href=""> <img class="img-responsive" src="images/2004/BigLeap Job Fair on Dec 2004.jpg">
                <div class="demo-gallery-poster"> <img src="images/serchicon-event.png"> </div>
                </a> </li>
              <li data-responsive="images/2004/icici Hr With Candidates.jpg" data-src="images/2004/icici Hr With Candidates.jpg"
      data-sub-html="<h4>ICICI Hr With Candidates</h4><p>ICICI Hr Discuss With Candidates</p>" data-pinterest-text="Pin it" data-tweet-text="share on twitter "> <a href=""> <img class="img-responsive" src="images/2004/icici Hr With Candidates.jpg">
                <div class="demo-gallery-poster"> <img src="images/serchicon-event.png"> </div>
                </a> </li>
              <li data-responsive="images/2004/Infosys HR Team.jpg" data-src="images/2004/Infosys HR Team.jpg"
      data-sub-html="<h4>Infosys HR Team</h4><p>Infosys HR Team	Dscussion</p>" data-pinterest-text="Pin it" data-tweet-text="share on twitter "> <a href=""> <img class="img-responsive" src="images/2004/Infosys HR Team.jpg">
                <div class="demo-gallery-poster"> <img src="images/serchicon-event.png"> </div>
                </a> </li>
              <li data-responsive="images/2004/Press Conference About Job Fair.jpg" data-src="images/2004/Press Conference About Job Fair.jpg"
      data-sub-html="<h4>Press Conference</h4><p>Press Conference About Job Fair</p>" data-pinterest-text="Pin it" data-tweet-text="share on twitter "> <a href=""> <img class="img-responsive" src="images/2004/Press Conference About Job Fair.jpg">
                <div class="demo-gallery-poster"> <img src="images/serchicon-event.png"> </div>
                </a> </li>
             
            </ul>
          </div>
        </div>
      </div>
    </div>
  </section>
  
</div>
<!--content end here--> 
