
<!--content start here-->
<div class="banner-slide">
  <div class="slidersection about-page">
    <div class="common-titles">
      <h2 class="header-page-subtitle wow fadeInUp">Don’t Miss Them!</h2>
      <h1 class="header-page-title wow fadeInUp">Know What’s Up Next.</h1>
    </div>
  </div>
</div>
<div class="bigleap">
  <section class="events">
    <div class="container">
      <h3 class="main-title wow fadeInUp">What’s Planned Ahead for You.</h3>
      <p class="main-descr wow fadeInUp">No event is too small or irrelevant. We plan, design and execute each of the events keeping your future in mind. Each event will benefit you in terms of building your career and personal growth. Get benefited.</p>
      <div class="order-div" id="main-order">
        <div class="row">
            <div class="col-md-3 col-sm-3 col-lg-3">
            <div class="folder-link wow fadeInUp" id="9-div"><a href="events-2020.html" target="_blank"><img src="<?php echo base_url(); ?>/assets/images/folder-09.png" class="img-responsive"></a>
              <h3>2020 - 2021</h3>
            </div>
          </div>
            <div class="col-md-3 col-sm-3 col-lg-3">
            <div class="folder-link wow fadeInUp" id="8-div"><a href="events-2018.html" target="_blank"><img src="<?php echo base_url(); ?>/assets/images/folder-08.png" class="img-responsive"></a>
              <h3>2017 - 2018</h3>
            </div>
          </div>
          <div class="col-md-3 col-sm-3 col-lg-3">
            <div class="folder-link wow fadeInUp" id="7-div"><a href="events-2015.html" target="_blank"><img src="<?php echo base_url(); ?>/assets/images/folder-07.png" class="img-responsive"></a>
              <h3>2014 - 2015</h3>
            </div>
          </div>
          <div class="col-md-3 col-sm-3 col-lg-3">
            <div class="folder-link wow fadeInUp" id="6-div"><a href="events-2014.html" target="_blank"><img src="<?php echo base_url(); ?>/assets/images/folder-06.png" class="img-responsive"></a>
              <h3>2013 - 2014</h3>
            </div>
          </div>
          <div class="col-md-3 col-sm-3 col-lg-3">
            <div class="folder-link wow fadeInUp" id="5-div"><a href="events-2013.html" target="_blank"><img src="<?php echo base_url(); ?>/assets/images/folder-05.png" class="img-responsive"></a>
              <h3>2012 - 2013</h3>
            </div>
          </div>
          <div class="col-md-3 col-sm-3 col-lg-3">
            <div class="folder-link wow fadeInUp" id="4-div"><a href="events-2011.html" target="_blank"><img src="<?php echo base_url(); ?>/assets/images/folder-04.png" class="img-responsive"></a>
              <h3>2010 - 20011</h3>
            </div>
          </div>
          <div class="col-md-3 col-sm-3 col-lg-3">
            <div class="folder-link wow fadeInUp" id="3-div"><a href="events-2006.html" target="_blank"><img src="<?php echo base_url(); ?>/assets/images/folder-03.png" class="img-responsive"></a>
              <h3>2005 - 2006</h3>
            </div>
          </div>
          <div class="col-md-3 col-sm-3 col-lg-3">
            <div class="folder-link wow fadeInUp" id="2-div"><a href="events-2005.html" target="_blank"><img src="<?php echo base_url(); ?>/assets/images/folder-02.png" class="img-responsive"></a>
              <h3>2004 - 2005</h3>
            </div>
          </div>
          <div class="col-md-3 col-sm-3 col-lg-3">
            <div class="folder-link wow fadeInUp" id="1-div"><a href="events-2004.html"><img src="<?php echo base_url(); ?>/assets/images/folder-01.png" class="img-responsive"></a>
              <h3>2003 - 2004</h3>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

<!-- start javascript file --> 
<script type="application/javascript" src="<?php echo base_url(); ?>/assets/js/jquery.min.js"></script> 
<script type="application/javascript" src="<?php echo base_url(); ?>/assets/js/bootstrap.min.js"></script> 
<script type="application/javascript" src="<?php echo base_url(); ?>/assets/js/index.js"></script> 
<script type="application/javascript" src="<?php echo base_url(); ?>/assets/js/wow.min.js"></script> 
<script type="application/javascript" src="<?php echo base_url(); ?>/assets/js/scroll-top.js"></script> 
<script>
         jQuery(document).ready(function( $ ) {
           // Initiate the wowjs animation library
           new WOW().init();
         });
         $(window).scroll(function(){
           var sticky = $('.sticky'),
               scroll = $(window).scrollTop();
           if (scroll >= 36) sticky.addClass('fixed');
           else sticky.removeClass('fixed');
         });
      </script> 
<!-- end javascript file -->
</body>
</html>