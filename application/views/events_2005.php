
<!--content start here-->
<div class="banner-slide">
  <div class="slidersection about-page">
    <div class="common-titles">
      <h2 class="header-page-subtitle wow fadeInUp"> We are more than an consultancy </h2>
      <h1 class="header-page-title wow fadeInUp">2004-2005</h1>
    </div>
  </div>
</div>
<div class="bigleap">
  <section class="service-sect">
    <div class="container">
      <h3 class="main-title wow fadeInUp"> Events of 2004-2005</h3>
      <p class="main-descr wow fadeInUp">Our specialized RPO services are designed to effectively handle the entire recruitment process of our clients focusing on speed of delivery, quality of talent &amp; accountability. We are well equipped with talented consultants having varied industry knowledge &amp; expertise to execute complete recruitment assignments of the clients.</p>
      	<div class="events-one">
      			
      		<div class="cont">
 
  <div class="demo-gallery wow fadeInUp">
    <ul id="lightgallery">
      <li data-responsive="images/2005/2005  Job Fair@Al Ameen_Reg  Counter 2.jpg" data-src="images/2005/2005  Job Fair@Al Ameen_Reg  Counter 2.jpg"
      
      data-sub-html="<h4>2005 BigLeap Job Fair </h4><p>2005  Job Fair@Al Ameen_Reg  Counter 2</p>" data-pinterest-text="Pin it" data-tweet-text="share on twitter ">
       
        <a href="">
          <img class="img-responsive" src="images/2005/2005  Job Fair@Al Ameen_Reg  Counter 2.jpg">
          <div class="demo-gallery-poster">
            <img src="images/serchicon-event.png">
          </div>
        </a>
      </li>
      <li data-responsive="images/2005/2005 BigLeap Job Fair @ Al Ameen Public School.jpg" data-src="images/2005/2005 BigLeap Job Fair @ Al Ameen Public School.jpg"
      data-sub-html="<h4>2005 BigLeap Job Fair </h4><p>2005 BigLeap Job Fair @ Al Ameen Public School</p>" data-pinterest-text="Pin it" data-tweet-text="share on twitter ">
        <a href="">
          <img class="img-responsive" src="images/2005/2005 BigLeap Job Fair @ Al Ameen Public School.jpg">
          <div class="demo-gallery-poster">
            <img src="images/serchicon-event.png">
          </div>
        </a>
      </li>
      <li data-responsive="images/2005/2005 Drive Reg Counter 1.jpg" data-src="images/2005/2005 Drive Reg Counter 1.jpg"
      data-sub-html="<h4>2005 BigLeap Job Fair </h4><p>2005 Drive Reg Counter-1</p>" data-pinterest-text="Pin it" data-tweet-text="share on twitter ">
        <a href="">
          <img class="img-responsive" src="images/2005/2005 Drive Reg Counter 1.jpg">
          <div class="demo-gallery-poster">
            <img src="images/serchicon-event.png">
          </div>
        </a>
      </li>
      <li data-responsive="images/2005/2005 Drive Reg Counter 3.jpg" data-src="images/2005/2005 Drive Reg Counter 3.jpg"
      data-sub-html="<h4>2005 BigLeap Job Fair </h4><p>2005 Drive Reg Counter-3</p>" data-pinterest-text="Pin it" data-tweet-text="share on twitter ">
        <a href="">
          <img class="img-responsive" src="images/2005/2005 Drive Reg Counter 3.jpg">
          <div class="demo-gallery-poster">
            <img src="images/serchicon-event.png">
          </div>
        </a>
      </li>
      <li data-responsive="images/2005/2005 Job Fair @ Trivandrum.JPG" data-src="images/2005/2005 Job Fair @ Trivandrum.JPG"
      data-sub-html="<h4>2005 BigLeap Job Fair </h4><p>2005 Job Fair @ Trivandrum</p>" data-pinterest-text="Pin it" data-tweet-text="share on twitter ">
        <a href="">
          <img class="img-responsive" src="images/2005/2005 Job Fair @ Trivandrum.JPG">
          <div class="demo-gallery-poster">
            <img src="images/serchicon-event.png">
          </div>
        </a>
      </li>
      <li data-responsive="images/2005/2005 Job Fair @ TVM_Aztech Software Technologies.JPG" data-src="images/2005/2005 Job Fair @ TVM_Aztech Software Technologies.JPG"
      data-sub-html="<h4>2005 BigLeap Job Fair </h4><p>2005 Job Fair @ TVM_Aztech Software Technologies</p>" data-pinterest-text="Pin it" data-tweet-text="share on twitter ">
        <a href="">
          <img class="img-responsive" src="images/2005/2005 Job Fair @ TVM_Aztech Software Technologies.JPG">
          <div class="demo-gallery-poster">
            <img src="images/serchicon-event.png">
          </div>
        </a>
      </li>
    </ul>
  </div>
</div>
      	</div>
    </div>
  </section>
  
</div>
<!--content end here--> 
