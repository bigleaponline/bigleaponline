
<!--content start here-->
<div class="banner-slide">
  <div class="slidersection about-page">
    <div class="common-titles">
      <h2 class="header-page-subtitle wow fadeInUp"> We are more than an consultancy </h2>
      <h1 class="header-page-title wow fadeInUp">Our Services</h1>
    </div>
  </div>
</div>
<div class="bigleap">
  <section class="service-sect">
    <div class="container">
      <h3 class="main-title wow fadeInUp"> Services that make it Simple </h3>
      <p class="main-descr wow fadeInUp">The first is a non technical method which requires the use of adware removal software. Download free adware and spyware removal software and use advanced tools getting infected.</p>
      <div class="servic-tab">
        <div class="row">
          <div  class="col-lg-12 col-md-12 col-sm-12">
            <div class="col-lg-3 col-md-3 col-sm-3"> 
              <h3 class="module-title wow fadeInUp">Services</h3>
              <ul class="nav nav-tabs tabs-left sideways">
                <li class="active wow fadeInUp"><a href="#Jobs" data-toggle="tab">Jobs</a></li>
                <li class="wow fadeInUp"><a href="#Internship" data-toggle="tab">Internship</a></li>
                <li class="wow fadeInUp"><a href="#Project" data-toggle="tab">Project training</a></li>
                <li class="wow fadeInUp"><a href="#Skill" data-toggle="tab"> Skill development</a></li>
              </ul>
              <div class="download-area">
              	<h3 class="module-title wow fadeInUp">Download Brochure</h3>
              	<div class="download-main">
              		<ul class="">
              			<li class="wow fadeInUp"><a href="#"><i class="fa fa-download" aria-hidden="true"></i>Company Proposal</a></li>
              			<li class="wow fadeInUp"><a href="#"><i class="fa fa-download" aria-hidden="true"></i>Agreement</a></li>
              			<li class="wow fadeInUp"><a href="#"><i class="fa fa-download" aria-hidden="true"></i>Our Portfolio</a></li>
              		</ul>
              	</div>
              </div>
              
              <div class="download-area">
              	<h3 class="module-title wow fadeInUp">Fast Contact</h3>
              	<div class="download-main servc-cnt">
              		<div class="row marg-btm">
              			<div class="col-md-4 padd-lftr">
              				<div class="srv-cnt-img wow fadeInUp"><img src="<?php echo base_url(); ?>/assets/images/team-new.png" class="img-responsive"></div>
              			</div>
              			<div class="col-md-8 padd-zero">
              				<h5 class="wow fadeInUp">Priya</h5>
              				<h6 class="wow fadeInUp">HR Consultant</h6>
              				<p><a href="mailto:hr@bigleaponline.com" class="wow fadeInUp">hr@bigleaponline.com</a></p>
              			</div>
              		</div>
              		<div class="row">
              			<div class="col-md-4 padd-lftr">
              				<div class="srv-cnt-img wow fadeInUp"><img src="<?php echo base_url(); ?>/assets/images/team-new.png" class="img-responsive"></div>
              			</div>
              			<div class="col-md-8 padd-zero">
              				<h5 class="wow fadeInUp">Aishwarya</h5>
              				<h6 class="wow fadeInUp">Administration</h6>
              				<p><a href="mailto:hr@bigleaponline.com" class="wow fadeInUp">hr@bigleaponline.com</a></p>
              			</div>
              		</div>
              	</div>
              </div>
              <div class="download-area">
              	<h3 class="module-title wow fadeInUp">Have Any Question?</h3>
              	<div class="download-main">
              		<p class="wow fadeInUp">BigLeap has been a trendsetter among the job-seeking youth in Kerala, bringing them world’s respected companies to them who brought with them many an opportunity to succeed. </p>
              		<a href="<?php echo base_url('faq'); ?>" class="cmn-link wow fadeInUp"><i class="fa fa-question-circle" aria-hidden="true"></i> Ask Now </a>
              	</div>
              </div>
              
            </div>
            <div class="col-lg-9 col-md-9 col-sm-9"> 
              <!-- Tab panes -->
              <div class="tab-content">
                <div class="tab-pane active" id="Jobs">
                  <div class="tab-contents">
                    <div class="tab-imgs wow fadeInUp"> <img src="<?php echo base_url(); ?>/assets/images/service-img-01.png" class="img-responsive">
                      <div class="serv-txt">
                        <h3 class="wow fadeInUp">Right Job for the Right Talents</h3>
                        <p class="sudser wow fadeInUp">Be it a fresher, an experienced candidate or an organisation looking to hire right candidates, our assistance and industry knowledge shall help you seek what you are looking for.</p>
                      </div>
                    </div>
                    <div class="other-details">
                      <h4 class="wow fadeInUp">Job Detail</h4>
                      <p class="wow fadeInUp">Be it a fresher, an experienced candidate or an organisation looking to hire right candidates, our assistance and industry knowledge shall help you seek what you are looking for. We are talent acquisition partner to many national and international organisations, which gives us an upper hand in serving both job seekers and talent hunters alike. </p>
                      <p class="wow fadeInUp">It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                      <h5 class="wow fadeInUp">Helping job seekers and talent hunters find the right picks.</h5>
                    <!--  <p class="wow fadeInUp">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
                      <ul class="">
                        <li class="wow fadeInUp"><i class="fa fa-check-square" aria-hidden="true"></i>It was popularised in the 1960s with the release.</li>
                        <li class="wow fadeInUp"><i class="fa fa-check-square" aria-hidden="true"></i>It was popularised in the 1960s with the release.</li>
                        <li class="wow fadeInUp"><i class="fa fa-check-square" aria-hidden="true"></i>It was popularised in the 1960s with the release.</li>
                        <li class="wow fadeInUp"><i class="fa fa-check-square" aria-hidden="true"></i>It was popularised in the 1960s with the release.</li>
                      </ul>
                      <p class="wow fadeInUp">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>-->
                    </div>
                  </div>
                </div>
                <div class="tab-pane" id="Internship">
                  <div class="tab-contents">
                    <div class="tab-imgs"> <img src="<?php echo base_url(); ?>/assets/images/service-img-02.png" class="img-responsive">
                      <div class="serv-txt">
                        <h3 class="wow fadeInUp">Practice Makes You Industry Ready</h3>
                        <p class="sudser wow fadeInUp">Offering internships opportunities for freshers and students who are aiming for a prospective career opportunity in Web Development and Digital Marketing besides Hardware and Networking.</p>
                      </div>
                    </div>
                    <div class="other-details">
                      <h4 class="wow fadeInUp">Service Detail</h4>
                      <p class="wow fadeInUp">Offering internships opportunities for freshers and students who are aiming for a prospective career opportunity in Web Development and Digital Marketing besides Hardware and Networking.</p>
                      <h5 class="wow fadeInUp">Internship opportunities in Web Development and Digital Marketing, Hardware and Networking.</h5>
                   <!--   <p class="wow fadeInUp">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
                      <ul class="">
                        <li class="wow fadeInUp"><i class="fa fa-check-square" aria-hidden="true"></i>It was popularised in the 1960s with the release.</li>
                        <li class="wow fadeInUp"><i class="fa fa-check-square" aria-hidden="true"></i>It was popularised in the 1960s with the release.</li>
                        <li class="wow fadeInUp"><i class="fa fa-check-square" aria-hidden="true"></i>It was popularised in the 1960s with the release.</li>
                        <li class="wow fadeInUp"><i class="fa fa-check-square" aria-hidden="true"></i>It was popularised in the 1960s with the release.</li>
                      </ul>
                      <p class="wow fadeInUp">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>-->
                    </div>
                  </div>
                </div>
                <div class="tab-pane" id="Project">
                  <div class="tab-contents">
                    <div class="tab-imgs"> <img src="<?php echo base_url(); ?>/assets/images/service-img-01.png" class="img-responsive">
                      <div class="serv-txt">
                        <h3 class="wow fadeInUp">Seek Guidance from Industry Experts</h3>
                        <p class="sudser wow fadeInUp">B-Tech and MCA students can seek enough subject guidance and training from both subject and industry experts with immense knowledge about IT sector and allied fields in order to complete their mini or main projects. </p>
                      </div>
                    </div>
                    <div class="other-details">
                      <h4 class="wow fadeInUp">Service Detail</h4>
                      <p class="wow fadeInUp">B-Tech and MCA students can seek enough subject guidance and training from both subject and industry experts with immense knowledge about IT sector and allied fields in order to complete their mini or main projects. </p>
                      <h5 class="wow fadeInUp">Project training and assistance for B-Tech and MCA students by subject and industry experts.</h5>
                      <p class="wow fadeInUp"><b>.NET</b><br>
                      .NET is quite simply the backbone of software development industry. This meticulous platform is essential for undertaking different aspects of software development ranging from accessing data to establishing internet connectivity in software applications. The far reaching nature of .NET makes it a vital part of every software company. So if you are well versed in .NET you will not find it difficult to land jobs in leading software companies.

The .NET development training program of Big Leap is led by industry experts who will help you learn the latest of the .NET platform. Through this extensive and in-depth training program you will learn to develop and implement software applications. We believe in providing practical knowledge to our students, so we will turn you into an ‘industry ready’ talent at the end of training course.
<br>
<b>PHP</b><br>
PHP is one of the simplest tools out there for publishing information on the web. It is this simplicity of PHP that has helped it in becoming one of the most popular and powerful web development tools. It is estimated that around two-thirds of the Internet uses in PHP in some form. So it is no surprise that PHP coders are hot property in the job market right now. More importantly, all kinds of firm-from big multinational companies to small start-ups- are hiring talented PHP programmers to develop and manage highly scalable online databases. So an excellent knowledge in PHP will surely help you land your dream job in IT sector.
<br>
<b>JAVA</b>
<br>
JAVA is a programming atmosphere to develop and deploy enterprise applications. It is important for information technology industry to develop and create multiple web-based or server based applications to enhance the industrial competency. The Java ecosystem is self-sustaining. From mobility (Android) to middleware (Hadoop), Java will continue to make a big impact.</p>
                      <!--<ul class="">
                        <li class="wow fadeInUp"><i class="fa fa-check-square" aria-hidden="true"></i>It was popularised in the 1960s with the release.</li>
                        <li class="wow fadeInUp"><i class="fa fa-check-square" aria-hidden="true"></i>It was popularised in the 1960s with the release.</li>
                        <li class="wow fadeInUp"><i class="fa fa-check-square" aria-hidden="true"></i>It was popularised in the 1960s with the release.</li>
                        <li class="wow fadeInUp"><i class="fa fa-check-square" aria-hidden="true"></i>It was popularised in the 1960s with the release.</li>
                      </ul>
                      <p class="wow fadeInUp">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>-->
                    </div>
                  </div>
                </div>
                <div class="tab-pane" id="Skill">
                  <div class="tab-contents">
                    <div class="tab-imgs"> <img src="<?php echo base_url(); ?>/assets/images/service-img-01.png" class="img-responsive">
                      <div class="serv-txt">
                        <h3 class="wow fadeInUp">Tune Your Skills Up</h3>
                        <p class="sudser wow fadeInUp">Helping candidates refine their skills in critical and analytical thinking, technical domain, problem solving, general awareness, interpersonal and communication skills, emotional quotient, time management, initiation and leadership, and creativity in order to achieve a successful career.</p>
                      </div>
                    </div>
                    <div class="other-details">
                      <h4 class="wow fadeInUp">Service Detail</h4>
                      <p class="wow fadeInUp">Helping candidates refine their skills in critical and analytical thinking, technical domain, problem solving, general awareness, interpersonal and communication skills, emotional quotient, time management, initiation and leadership, and creativity in order to achieve a successful career.</p>
                      <p class="wow fadeInUp">It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                      <h5 class="wow fadeInUp">Nine skills that would help candidates with better career paths and opportunities.</h5>
                     <!-- <p class="wow fadeInUp">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
                      <ul class="">
                        <li class="wow fadeInUp"><i class="fa fa-check-square" aria-hidden="true"></i>It was popularised in the 1960s with the release.</li>
                        <li class="wow fadeInUp"><i class="fa fa-check-square" aria-hidden="true"></i>It was popularised in the 1960s with the release.</li>
                        <li class="wow fadeInUp"><i class="fa fa-check-square" aria-hidden="true"></i>It was popularised in the 1960s with the release.</li>
                        <li class="wow fadeInUp"><i class="fa fa-check-square" aria-hidden="true"></i>It was popularised in the 1960s with the release.</li>
                      </ul>
                      <p class="wow fadeInUp">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>-->
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
    </div>
  </section>
<!--   <section class="newsletter">
      <div class="container">
        <h3 class="main-title wow fadeInUp"> Reaching Us is Never Too Hard<br>
          </h3>
        <p class="main-descr wow fadeInUp">We would like to hear from you, be it a query, a request or a review. Drop a message or drop in on us. <br>The choice and convenience are yours.</p>
        <div class="row">
          <div class="col-md-7">
            <div class="map-sect wow fadeInUp">
              <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d15652.141444286224!2d75.79527265000002!3d11.25880905!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sin!4v1576832177948!5m2!1sen!2sin" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
              <div class="map-addres" id="book"> <span class="click wow fadeInUp" id="clickme"><i class="fa fa-times" aria-hidden="true"></i></span>
                <div class="row adrmarg wow fadeInUp">
                  <div class="col-md-1 adds-loca adds-localeft"> <i class="fa fa-map-marker" aria-hidden="true"></i> </div>
                  <div class="col-md-11 adds-loca wow fadeInUp">
                    <h5>BIGLEAP SOLUTIONS (P) LTD.</h5>
                    <h6>4th Floor - Markaz Complex - Mavoor Road - Kozhikode - Kerala</h6>
                  </div>
                </div>
                <div class="row adrmarg wow fadeInUp">
                  <div class="col-md-1 adds-loca adds-localeft wow fadeInUp"> <i class="fa fa-phone" aria-hidden="true"></i> </div>
                  <div class="col-md-11 adds-loca wow fadeInUp">
                    <h5><a href="tel:+91 9061560224">+91 9061560224</a></h5>
                    <h6>Monday to Saturday - 9am to 6 pm</h6>
                  </div>
                </div>
                <div class="row adrmarg wow fadeInUp">
                  <div class="col-md-1 adds-loca adds-localeft wow fadeInUp"> <i class="fa fa-envelope-o" aria-hidden="true"></i> </div>
                  <div class="col-md-11 adds-loca wow fadeInUp">
                    <h5><a href="mailto:hr@bigleaponline.com">hr@bigleaponline.com</a></h5>
                    <h6>Send us your query anytime!</h6>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-5">
            <form class="footer-form">
              <div class="input-set wow fadeInUp">
                <label class="biglabel wow fadeInUp">Your Name</label>
                <input class="input-big wow fadeInUp" placeholder="Enter your Name" type="text">
              </div>
              <div class="input-set wow fadeInUp">
                <label class="biglabel wow fadeInUp">EMAIL ADDRESS</label>
                <input class="input-big wow fadeInUp" placeholder="Enter your Email" type="email">
              </div>
              <div class="input-set wow fadeInUp">
                <label class="biglabel wow fadeInUp">SUBJECT</label>
                <input class="input-big wow fadeInUp" placeholder="Enter Subject" type="text">
              </div>
              <div class="input-set wow fadeInUp">
                <label class="biglabel wow fadeInUp">MESSAGE</label>
                <textarea class="input-big wow fadeInUp input-txt" placeholder="Enter Message"></textarea>
              </div>
              <button class="big-btn wow fadeInUp" type="submit">SUBMIT</button>
            </form>
          </div>
        </div>
      </div>
    </section> -->
<!-- </div> -->
<!--content end here--> 
<!-- start javascript file --> 
<script type="application/javascript" src="<?php echo base_url(); ?>/assets/js/jquery.min.js"></script> 
<script type="application/javascript" src="<?php echo base_url(); ?>/assets/js/bootstrap.min.js"></script> 
<script type="application/javascript" src="<?php echo base_url(); ?>/assets/js/owl.carousel.min.js"></script> 
<script type="application/javascript" src="<?php echo base_url(); ?>/assets/js/jquery.touchSwipe.min.js"></script> 
<script type="application/javascript" src="<?php echo base_url(); ?>/assets/js/index.js"></script> 

<script type="application/javascript" src="<?php echo base_url(); ?>/assets/js/wow.min.js"></script> 
<script type="application/javascript" src="<?php echo base_url(); ?>/assets/js/scroll-top.js"></script> 
<script>
 
         $( "#clickme" ).click(function() {
           $( "#book" ).hide( "slow", function() {
           });
         });
      </script> 
<script>
         jQuery(document).ready(function( $ ) {
           // Initiate the wowjs animation library
           new WOW().init();
         });
         $(window).scroll(function(){
           var sticky = $('.sticky'),
               scroll = $(window).scrollTop();
           if (scroll >= 36) sticky.addClass('fixed');
           else sticky.removeClass('fixed');
         });
      </script> 
<!-- end javascript file -->
</body>
</html>