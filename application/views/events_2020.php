
<!--content start here-->
<div class="banner-slide">
  <div class="slidersection about-page">
    <div class="common-titles">
      <h2 class="header-page-subtitle wow fadeInUp"> We are more than an consultancy </h2>
      <h1 class="header-page-title wow fadeInUp">2020-2021</h1>
    </div>
  </div>
</div>
<div class="bigleap">
  <section class="service-sect">
    <div class="container">
      <h3 class="main-title wow fadeInUp"> Events of 2020-2021</h3>
      <p class="main-descr wow fadeInUp">Our specialized RPO services are designed to effectively handle the entire recruitment process of our clients focusing on speed of delivery, quality of talent &amp; accountability. We are well equipped with talented consultants having varied industry knowledge &amp; expertise to execute complete recruitment assignments of the clients.</p>
      <div class="events-one">
        <div class="cont">
          <div class="demo-gallery wow fadeInUp">
            <ul id="lightgallery">
                
              <li data-responsive="images/2020/hm-gallery-01.jpg" data-src="images/2020/hm-gallery-01.jpg"
      
      data-sub-html="<h4>2018 IBM Drive</h4><p>2018 IBM Drive Registration Section</p>" data-pinterest-text="Pin it" data-tweet-text="share on twitter "> <a href=""> <img class="img-responsive" src="images/2020/hm-gallery-01.jpg">
                <div class="demo-gallery-poster"> <img src="images/serchicon-event.png"> </div>
                </a> </li>
                
              <li data-responsive="images/2020/hm-gallery-02.jpg" data-src="images/2020/hm-gallery-02.jpg"
      data-sub-html="<h4>2018 IBM Drive</h4><p>2018 IBM Drive</p>" data-pinterest-text="Pin it" data-tweet-text="share on twitter "> <a href=""> <img class="img-responsive" src="images/2020/hm-gallery-02.jpg">
                <div class="demo-gallery-poster"> <img src="images/serchicon-event.png"> </div>
                </a> </li>
                
              <li data-responsive="images/2020/hm-gallery-03.jpg" data-src="images/2020/hm-gallery-03.jpg"
      data-sub-html="<h4>2018 IBM Drive</h4><p>2018 IBM Drive</p>" data-pinterest-text="Pin it" data-tweet-text="share on twitter "> <a href=""> <img class="img-responsive" src="images/2020/hm-gallery-03.jpg">
                <div class="demo-gallery-poster"> <img src="images/serchicon-event.png"> </div>
                </a> </li>
                
              <li data-responsive="images/2020/hm-gallery-04.jpg" data-src="images/2020/hm-gallery-04.jpg"
      data-sub-html="<h4>2018 IBM Drive</h4><p>2018 IBM Drive</p>" data-pinterest-text="Pin it" data-tweet-text="share on twitter "> <a href=""> <img class="img-responsive" src="images/2020/hm-gallery-04.jpg">
                <div class="demo-gallery-poster"> <img src="images/serchicon-event.png"> </div>
                </a> </li>
                
              <li data-responsive="images/2020/hm-gallery-05.jpg" data-src="images/2020/hm-gallery-05.jpg"
      data-sub-html="<h4>2018 IBM Drive</h4><p>2018 IBM Drive</p>" data-pinterest-text="Pin it" data-tweet-text="share on twitter "> <a href=""> <img class="img-responsive" src="images/2020/hm-gallery-05.jpg">
                <div class="demo-gallery-poster"> <img src="images/serchicon-event.png"> </div>
                </a> </li>
                
              <li data-responsive="images/2020/hm-gallery-06.jpg" data-src="images/2020/hm-gallery-06.jpg"
      data-sub-html="<h4>2018 IBM Drive</h4><p>2018 IBM Drive</p>" data-pinterest-text="Pin it" data-tweet-text="share on twitter "> <a href=""> <img class="img-responsive" src="images/2020/hm-gallery-06.jpg">
                <div class="demo-gallery-poster"> <img src="images/serchicon-event.png"> </div>
                </a> </li>
                
              <li data-responsive="images/2020/hm-gallery-07.jpg" data-src="images/2020/hm-gallery-07.jpg"
      data-sub-html="<h4>2018 IBM Drive</h4><p>2018 IBM Drive</p>" data-pinterest-text="Pin it" data-tweet-text="share on twitter "> <a href=""> <img class="img-responsive" src="images/2020/hm-gallery-07.jpg">
                <div class="demo-gallery-poster"> <img src="images/serchicon-event.png"> </div>
                </a> </li>
                
                <li data-responsive="images/2020/hm-gallery-08.jpg" data-src="images/2020/hm-gallery-08.jpg"
      data-sub-html="<h4>2018 IBM Drive</h4><p>2018 IBM Drive</p>" data-pinterest-text="Pin it" data-tweet-text="share on twitter "> <a href=""> <img class="img-responsive" src="images/2020/hm-gallery-08.jpg">
                <div class="demo-gallery-poster"> <img src="images/serchicon-event.png"> </div>
                </a> </li>
                
                <li data-responsive="images/2020/hm-gallery-09.jpg" data-src="images/2020/hm-gallery-09.jpg"
      data-sub-html="<h4>2018 IBM Drive</h4><p>2018 IBM Drive</p>" data-pinterest-text="Pin it" data-tweet-text="share on twitter "> <a href=""> <img class="img-responsive" src="images/2020/hm-gallery-09.jpg">
                <div class="demo-gallery-poster"> <img src="images/serchicon-event.png"> </div>
                </a> </li>
                
                <li data-responsive="images/2020/hm-gallery-10.jpg" data-src="images/2020/hm-gallery-10.jpg"
      data-sub-html="<h4>2018 IBM Drive</h4><p>2018 IBM Drive</p>" data-pinterest-text="Pin it" data-tweet-text="share on twitter "> <a href=""> <img class="img-responsive" src="images/2020/hm-gallery-10.jpg">
                <div class="demo-gallery-poster"> <img src="images/serchicon-event.png"> </div>
                </a> </li>
                
                <li data-responsive="images/2020/hm-gallery-11.jpg" data-src="images/2020/hm-gallery-11.jpg"
      data-sub-html="<h4>2018 IBM Drive</h4><p>2018 IBM Drive</p>" data-pinterest-text="Pin it" data-tweet-text="share on twitter "> <a href=""> <img class="img-responsive" src="images/2020/hm-gallery-11.jpg">
                <div class="demo-gallery-poster"> <img src="images/serchicon-event.png"> </div>
                </a> </li>
                
                <li data-responsive="images/2020/hm-gallery-12.jpg" data-src="images/2020/hm-gallery-12.jpg"
      data-sub-html="<h4>2018 IBM Drive</h4><p>2018 IBM Drive</p>" data-pinterest-text="Pin it" data-tweet-text="share on twitter "> <a href=""> <img class="img-responsive" src="images/2020/hm-gallery-12.jpg">
                <div class="demo-gallery-poster"> <img src="images/serchicon-event.png"> </div>
                </a> </li>
                
                <li data-responsive="images/2020/hm-gallery-13.jpg" data-src="images/2020/hm-gallery-13.jpg"
      data-sub-html="<h4>2018 IBM Drive</h4><p>2018 IBM Drive</p>" data-pinterest-text="Pin it" data-tweet-text="share on twitter "> <a href=""> <img class="img-responsive" src="images/2020/hm-gallery-13.jpg">
                <div class="demo-gallery-poster"> <img src="images/serchicon-event.png"> </div>
                </a> </li>
                
                <li data-responsive="images/2020/hm-gallery-14.jpg" data-src="images/2020/hm-gallery-14.jpg"
      data-sub-html="<h4>2018 IBM Drive</h4><p>2018 IBM Drive</p>" data-pinterest-text="Pin it" data-tweet-text="share on twitter "> <a href=""> <img class="img-responsive" src="images/2020/hm-gallery-14.jpg">
                <div class="demo-gallery-poster"> <img src="images/serchicon-event.png"> </div>
                </a> </li>
                <li data-responsive="images/2020/hm-gallery-15.jpg" data-src="images/2020/hm-gallery-15.jpg"
      data-sub-html="<h4>2018 IBM Drive</h4><p>2018 IBM Drive</p>" data-pinterest-text="Pin it" data-tweet-text="share on twitter "> <a href=""> <img class="img-responsive" src="images/2020/hm-gallery-15.jpg">
                <div class="demo-gallery-poster"> <img src="images/serchicon-event.png"> </div>
                </a> </li>
                <li data-responsive="images/2020/hm-gallery-16.jpg" data-src="images/2020/hm-gallery-16.jpg"
      data-sub-html="<h4>2018 IBM Drive</h4><p>2018 IBM Drive</p>" data-pinterest-text="Pin it" data-tweet-text="share on twitter "> <a href=""> <img class="img-responsive" src="images/2020/hm-gallery-16.jpg">
                <div class="demo-gallery-poster"> <img src="images/serchicon-event.png"> </div>
                </a> </li>
                <li data-responsive="images/2020/hm-gallery-17.jpg" data-src="images/2020/hm-gallery-17.jpg"
      data-sub-html="<h4>2018 IBM Drive</h4><p>2018 IBM Drive</p>" data-pinterest-text="Pin it" data-tweet-text="share on twitter "> <a href=""> <img class="img-responsive" src="images/2020/hm-gallery-17.jpg">
                <div class="demo-gallery-poster"> <img src="images/serchicon-event.png"> </div>
                </a> </li>
                <li data-responsive="images/2020/hm-gallery-18.jpg" data-src="images/2020/hm-gallery-18.jpg"
      data-sub-html="<h4>2018 IBM Drive</h4><p>2018 IBM Drive</p>" data-pinterest-text="Pin it" data-tweet-text="share on twitter "> <a href=""> <img class="img-responsive" src="images/2020/hm-gallery-18.jpg">
                <div class="demo-gallery-poster"> <img src="images/serchicon-event.png"> </div>
                </a> </li>
                
                
                
                
                
                
                
                
                
                
               
                
                
                
                
                
                
                
            </ul>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
<!--content end here--> 
