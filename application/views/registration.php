
<!--content start here-->
<div class="banner-slide">
  <div class="slidersection about-page">
    <div class="common-titles">
      <h2 class="header-page-subtitle wow fadeInUp">Make the Most of Our Services</h2>
      <h1 class="header-page-title wow fadeInUp">An Extra Step to Gain Extra</h1>
    </div>
  </div>
</div>
<div class="bigleap">
  <section class="registration">
    <div class="container">
      <h3 class="main-title wow fadeInUp">Sign Up for Something Big</h3>
      <p class="main-descr wow fadeInUp">Register with us to make the most of what we have to offer you. <br>Just an extra step to avail extra services and benefits.</p>
      <div class="selecting-are">
        <div class="row">
          <div class="col-md-6"> <a href="<?php echo base_url('post-job'); ?>" class="choose-sect wow fadeInUp">
            <div class="job-sek wow fadeInUp"> <i class="fa fa-briefcase" aria-hidden="true"></i> </div>
            <h3 class="wow fadeInUp">I am Employer</h3>
            </a> </div>
          <div class="col-md-6"> <a href="<?php echo base_url('postresume'); ?>" class="choose-sect wow fadeInUp">
            <div class="job-sek wow fadeInUp"> <i class="fa fa-users" aria-hidden="true"></i> </div>
            <h3 class="wow fadeInUp">I am Candidate</h3>
            </a> </div>
        </div>
      </div>
      
       <section class="company-list">
      <div class="container">
        <h3 class="main-title wow fadeInUp">Browse By Category</h3>
        <p class="main-descr wow fadeInUp">Each Month, More Than 7 Million Jobhunt Turn To Website In Their Search For Work, Making Over
          160,000 Applications Every Day.</p>
        <div class="row">
          <div class="owl-carousel owl-theme mobilesliderpro" id="carousel05">
            <div class="item wow fadeInUp">
              <div class="cmp-empl-list"> <span class="job-type full-time">Full Time</span>
                <div class="cmp-log"><img src="assets/images/job-sek-01.png" class="img-responsive center-block"></div>
                <h5>BigLeap</h5>
                <p>2708 Scenic Way, Sutter</p>
                <a href="#" class="vacncy-link">5 Open Position</a>
                <div class="heart HeartAnimation"></div>
              </div>
            </div>
            <div class="item wow fadeInUp">
              <div class="cmp-empl-list"> <span class="job-type full-time">Full Time</span>
                <div class="cmp-log"><img src="assets/images/job-sek-02.png" class="img-responsive center-block"></div>
                <h5>BigLeap</h5>
                <p>2708 Scenic Way, Sutter</p>
                <a href="#" class="vacncy-link">5 Open Position</a>
                <div class="heart HeartAnimation"></div>
              </div>
            </div>
            <div class="item wow fadeInUp">
              <div class="cmp-empl-list"> <span class="job-type part-time">Part Time</span>
                <div class="cmp-log"><img src="assets/images/job-sek-03.png" class="img-responsive center-block"></div>
                <h5>BigLeap</h5>
                <p>2708 Scenic Way, Sutter</p>
                <a href="#" class="vacncy-link">5 Open Position</a>
                <div class="heart HeartAnimation"></div>
              </div>
            </div>
            <div class="item wow fadeInUp">
              <div class="cmp-empl-list"> <span class="job-type part-time">Part Time</span>
                <div class="cmp-log"><img src="assets/images/job-sek-04.png" class="img-responsive center-block"></div>
                <h5>BigLeap</h5>
                <p>2708 Scenic Way, Sutter</p>
                <a href="#" class="vacncy-link">5 Open Position</a>
                <div class="heart HeartAnimation"></div>
              </div>
            </div>
            <div class="item wow fadeInUp">
              <div class="cmp-empl-list"> <span class="job-type full-time">Part Time</span>
                <div class="cmp-log"><img src="assets/images/job-sek-01.png" class="img-responsive center-block"></div>
                <h5>BigLeap</h5>
                <p>2708 Scenic Way, Sutter</p>
                <a href="#" class="vacncy-link">5 Open Position</a>
                <div class="heart HeartAnimation"></div>
              </div>
            </div>
            <div class="item wow fadeInUp">
              <div class="cmp-empl-list"> <span class="job-type part-time">Part Time</span>
                <div class="cmp-log"><img src="assets/images/job-sek-02.png" class="img-responsive center-block"></div>
                <h5>BigLeap</h5>
                <p>2708 Scenic Way, Sutter</p>
                <a href="#" class="vacncy-link">5 Open Position</a>
                <div class="heart HeartAnimation"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    </div>
  </section>
  <section class="client-sect">
      <h3 class="main-title wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">Our Clientele Includes <br>the Most Wanted</h3>
      <p class="main-descr wow fadeInUp">A wide portfolio of satisfied and reliable clients who stand a testimony to the service<br> we render to the industry and the quality practices we adhere to. </p>
      <div class="container">
        <div class="owl-carousel owl-theme mobilesliderpro" id="carousel01">
          <div class="item wow fadeInUp"><a href="#"><img src="assets/images/client-03.png" class="img-responsive"></a></div>
          <div class="item wow fadeInUp"><a href="#"><img src="assets/images/client-04.png" class="img-responsive"></a></div>
     
          <div class="item wow fadeInUp"><a href="#"><img src="assets/images/client-06.png" class="img-responsive"></a></div>
        </div>
      </div>
    </section>

<!--content end here--> 
<!-- start javascript file --> 
<script type="application/javascript" src="<?php echo base_url(); ?>/assets/js/jquery.min.js"></script> 
<script type="application/javascript" src="<?php echo base_url(); ?>/assets/js/bootstrap.min.js"></script> 
<script type="application/javascript" src="<?php echo base_url(); ?>/assets/js/owl.carousel.min.js"></script> 
<script type="application/javascript" src="<?php echo base_url(); ?>/assets/js/jquery.touchSwipe.min.js"></script> 
<script type="application/javascript" src="<?php echo base_url(); ?>/assets/js/index.js"></script> 
<script type="application/javascript" src="<?php echo base_url(); ?>/assets/js/progress.js"></script> 
<script type="application/javascript" src="<?php echo base_url(); ?>/assets/js/wow.min.js"></script> 
<script type="application/javascript" src="<?php echo base_url(); ?>/assets/js/scroll-top.js"></script> 
<script>
	   $('#carousel01').owlCarousel({
            nav:true,
         	margin:10,
         	loop:true,
         	autoplay: true,
            responsive:{
                 0:{
                     items:1
                 },
                 600:{
                     items:3
                 },
                 1000:{
                     items:6
                 }
             }
         });
                 $('#carousel05').owlCarousel({
            nav:true,
         	margin:5,
         	loop:true,
         	autoplay: true,
			autoplayHoverPause: true,
            responsive:{
                 0:{
                     items:1
                 },
                 600:{
                     items:2	
                 },
                 1000:{
                     items:4
                 }
             }
         });
	$(document).ready(function() {
  $(".set > a").on("click", function() {
    if ($(this).hasClass("active")) {
      $(this).removeClass("active");
      $(this)
        .siblings(".content")
        .slideUp(200);
      $(".set > a i")
        .removeClass("fa-minus")
        .addClass("fa-plus");
    } else {
      $(".set > a i")
        .removeClass("fa-minus")
        .addClass("fa-plus");
      $(this)
        .find("i")
        .removeClass("fa-plus")
        .addClass("fa-minus");
      $(".set > a").removeClass("active");
      $(this).addClass("active");
      $(".content").slideUp(200);
      $(this)
        .siblings(".content")
        .slideDown(200);
    }
  });
});
         $( "#clickme" ).click(function() {
           $( "#book" ).hide( "slow", function() {
           });
         });
      </script> 
<script>
         jQuery(document).ready(function( $ ) {
           // Initiate the wowjs animation library
           new WOW().init();
         });
         $(window).scroll(function(){
           var sticky = $('.sticky'),
               scroll = $(window).scrollTop();
           if (scroll >= 36) sticky.addClass('fixed');
           else sticky.removeClass('fixed');
         });
         	$(function() {
  $(".HeartAnimation").click(function() {
    $(this).toggleClass("animate");
  });
});
      </script> 
<!-- end javascript file -->
</body>
</html>