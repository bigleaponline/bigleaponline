<?php
ob_start();
class Main_model extends CI_Model 
{
public function __construct()
{
    parent::__construct();
    $this->db->cache_on();
    $this->load->database();
   
}

public function login()
{
    $username=$this->input->post('username');
    $password=base64_encode($this->input->post('password'));
    $query=$this->db->get_where('signup_details',array('email'=>$username,'password'=>$password));
    if($query->num_rows()>0)
    {
       foreach($query->result() as $details)
        $cand_data=array(
                        'id'=>$details->id,
                        'name'=>$details->name,
                        'password'=>$details->password,
                        'type'=>$details->type,
                        'status'=>$details->status
                    );
        $this->session->set_userdata($cand_data);
        return true;
    }
    else
    {
        return false;
    }
}

public function candidate_registration($resume)
{
    $data2['name']  = $this->input->post('name');
    $data2['phone']  = $this->input->post('phone');
    $data2['email']  = $this->input->post('email');
    $data2['type']  = "candidate";
    $data2['password']  = base64_encode($this->input->post('password'));
    $data2['status']  = 1;
    $this->db->insert('signup_details',$data2);
    $id=$this->db->insert_id();

    $data=array(
        'cand_id'=>$id,
        'f_name'=>$this->input->post('name'),
        'l_name'=>$this->input->post('l_name'),
        'qualification'=>$this->input->post('qualification'),
        'stream'=>$this->input->post('stream'),
        'experience'=>$this->input->post('experience'),
        'interest'=>$this->input->post('interest'),
        'address'=>$this->input->post('address'),
        'phone'=>$this->input->post('phone'),
        'email'=>$this->input->post('email'),
        'country'=>$this->input->post('country'),
        'applied'=>$this->input->post('applied'),
        // 'image'=>$image
        'password'=>base64_encode($this->input->post('password')),
        'c_password'=>base64_encode($this->input->post('c_password')),
        'resume'=>$resume,
        );
    $insert=$this->db->insert('job_seekers',$data);
    if($insert)
    {

        return true;
    }
    else
    {
        return false;
    }

}

public function contact_us()
{
	$name    = $this->input->post('name');
    $email    = $this->input->post('email');
    $subject = $this->input->post('subject');
    $message1 = $this->input->post('message');

    $data=array(
                'name'=>$name,
                'email'=>$email,
                'subject'=>$subject,
                'message'=>$message1
                );
    $insert=$this->db->insert('contact',$data);
    if($insert)
    {
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'TLS://smtp.gmail.com',
            'smtp_port' => 587,
            'smtp_user' => 'developers@bigleaponline.com',
            'smtp_pass' => 'bglp@123',
            'mailtype'  => 'html', 
            'charset'   => 'iso-8859-1',
            'wordwrap' => TRUE
        );
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $message=$message1;
        $this->email->from($email);
        $this->email->to('developers@bigleaponline.com'); 
        $this->email->subject($subject);
        $this->email->message($message);  
        // if($this->email->send())
        // {
        //     // echo $message;exit();
        //     echo "send successfully";
        // }
        // else
        // {
        //     show_error($this->email->print_debugger());
        // }
        return true;
    }
    else
    {
        return false;
    }
}

public function faq()
{
	$name    = $this->input->post('name');
    $email    = $this->input->post('email');
    $phone    = $this->input->post('phone');
    $subject = $this->input->post('subject');
    $message1 = $this->input->post('message');

    $data=array(
                'name'=>$name,
                'email'=>$email,
                'phone'=>$phone,
                'subject'=>$subject,
                'message'=>$message1
                );
    $insert=$this->db->insert('faq',$data);
    if($insert)
    {
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'TLS://smtp.gmail.com',
            'smtp_port' => 587,
            'smtp_user' => 'developers@bigleaponline.com',
            'smtp_pass' => 'bglp@123',
            'mailtype'  => 'html', 
            'charset'   => 'iso-8859-1',
            'wordwrap' => TRUE
        );
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $message=$message1;
        $this->email->from($email);
        $this->email->to('developers@bigleaponline.com'); 
        $this->email->subject($subject);
        $this->email->message($message);  
        // if($this->email->send())
        // {
        //     // echo $message;exit();
        //     echo "send successfully";
        // }
        // else
        // {
        //     show_error($this->email->print_debugger());
        // }
        return true;
    }
    else
    {
        return false;
    }
}

public function employer_registration($logo)
{

    $data3['name']  = $this->input->post('name');
    $data3['phone']  = $this->input->post('phone');
    $data3['email']  = $this->input->post('email');
    $data3['type']  = "company";
    $data3['password']  = base64_encode($this->input->post('password'));
    $data2['status']  = 1;
    $this->db->insert('signup_details',$data3);
    $id=$this->db->insert_id();


    $row_no = $this->input->post('row_no');
    $data['c_id']  = $id;
    $data['company_name']  = $this->input->post('name');
    $data['contact_no']  = $this->input->post('phone');
    $data['email_id']  = $this->input->post('email');
    $data['industry']  = $this->input->post('industry');
    $data['address']  = $this->input->post('address');
    $data['contact_person']  = $this->input->post('contact_person');
    $data['designation']  = $this->input->post('designation');
    $data['emp_date']  = date('m-d-Y');
    $data['location']  = $this->input->post('location');
    $data['logo']  = $logo;
    $data['responsibilities']  = $this->input->post('responsibilities');
    $data['password']  = base64_encode($this->input->post('password'));
    $data['c_password']  = base64_encode($this->input->post('c_password'));
    $employer=$this->db->insert('employer_registration',$data);
    if($employer)
    {

        $emp_id = $this->db->insert_id();
        $job_title = $this->input->post('job_title');
        $experience = $this->input->post('experience');
        $salary = $this->input->post('salary');
        $qualification = $this->input->post('qualification');
        $job_description = $this->input->post('job_description');
        for($i=0;$i<$row_no;$i++)
        {
            $data1['emp_id']  = $emp_id;
            $data1['job_title'] = $job_title[$i];
            $data1['experience'] = $experience[$i];
            $data1['salary'] = $salary[$i];
            $data1['job_description'] = $job_description[$i];
            $data1['qualification'] = $qualification[$i];
            $this->db->insert('emp_job_details',$data1);
        }
        $cpt=count($_FILES['jd']['name']);
        if($cpt>0)
        {
            for($i=0; $i<$cpt; $i++)
            {
                $jd=$_FILES['jd']['name'][$i];
                if($jd!="")
                {
                    $mimetype = mime_content_type($_FILES['jd']['tmp_name'][$i]);
                    if(in_array($mimetype, array('text/plain', 'application/pdf')))
                    {
                        $jd=time().".".$jd;
                            move_uploaded_file(
                            $_FILES['jd']['tmp_name'][$i], 'upload/jd/' . $jd
                                );
                    }
                    else
                    {
                        $jd="";
                        $this->session->set_flashdata('jd','Invalid JD..Support Only Pdf and txt');
                    }
                    $data2['emp_id']=$emp_id;
                    $data2['emp_jd']  = $jd;
                    $this->db->insert('tbl_jd',$data2);
                }
            }
        }
        return true;
    }
    else
    {
        $this->session->set_flashdata('employererror','Failed ..... Please Try Again....');
        return false;
    }
}

public function get_job_list()
{
    $this->db->select('A.*,B.*');
    $this->db->from('employer_registration A');
    $this->db->join('emp_job_details B', 'A.emp_id=B.emp_id','left');
    $this->db->order_by('A.emp_id' ,'ASC');
    $query = $this->db->get()->result_array();
    return $query;
}

public function get_job_details()
{
    $this->db->select('A.*,B.*');
    $this->db->from('employer_registration A');
    $this->db->join('emp_job_details B', 'A.emp_id=B.emp_id','left');
    $this->db-> where(array('B.job_id' => 1 ));
    $query = $this->db->get()->result_array();
    // print_r($this->db->last_query());die();
    return $query;
}
}
?>