<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require_once(APPPATH . "third_party/phpmailer/src/Exception.php");
require_once(APPPATH . "third_party/phpmailer/src/PHPMailer.php");
require_once(APPPATH . "third_party/phpmailer/src/SMTP.php");


class Mail_model extends CI_Model

{

    public function __construct() {
        $this->load->database();
    }


    public function sendEmail($subject, $mail_body, $email, $full_name) {

        $message = $mail_body;

        $mail = new PHPMailer();

        $mail->SMTPDebug = 0;                                    // Enable verbose debug output
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'mail.bigleaponline.com';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'developers@bigleaponline.com';                 // SMTP username
        $mail->Password = 'bglp@123';                           // SMTP password
        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 26;                                       // TCP port to connect to

        //Recipients
        $mail->setFrom($email, 'bigleaponline.com');
        $mail->addAddress('developers@bigleaponline.com', $full_name);     // Add a recipient

        //Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = $subject;
        $mail->Body    = $message;
        $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

        $mail->send();

        return true;
    }

   public function sendContactEmail($email,$subject,$message) {

        $subject = "Contact Mail - bigleaponline.com";
        $register_body = "
           <tr align='center'>
               <td style='font-family: Arial;padding-bottom: 2.1rem'><h3 style='margin: 10px 0;font-size: 1.1rem; font-weight: 500;'>$subject</h3></td>
           </tr>
           <tr align='center'>
               <td style='padding-bottom: 0.5rem'><strong style='font-size: 1.3rem; font-family: Arial, sans-serif'>Details are,$message</strong></td>
           </tr>
            ";

        $this->sendEmail($subject, $register_body, $email);

        return true;
    }

   
    

}

/* 

 * To change this license header, choose License Headers in Project Properties.

 * To change this template file, choose Tools | Templates

 * and open the template in the editor.

 */



