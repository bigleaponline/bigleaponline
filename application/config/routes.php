<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Main_controller';
$route['index'] = 'Main_controller';
$route['login']='Main_controller/signin';
$route['logout']='Main_controller/logout';
$route['home']='Main_controller/loadpage/home';
$route['about']='Main_controller/loadpage/about';
$route['service']='Main_controller/loadpage/service';
$route['events']='Main_controller/loadpage/events';
$route['registration']='Main_controller/loadpage/registration';
$route['contact']='Main_controller/loadpage/contact';
$route['post-job']='Main_controller/job_seekers';
$route['postresume']='Main_controller/post_resume';
$route['faq']='Main_controller/loadpage/faq';
// $route['login']='Main_controller/loadpage/login';
$route['job-details']='Main_controller/loadpage/job-details';
$route['job_details']='Main_controller/get_job_details';
$route['about']='Main_controller/loadpage/about';
$route['events_2020']='Main_controller/loadpage/events_2020';
$route['events_2018']='Main_controller/loadpage/events_2018';
$route['events_2015']='Main_controller/loadpage/events_2015';
$route['events_2014']='Main_controller/loadpage/events_2014';
$route['events_2013']='Main_controller/loadpage/events_2013';
$route['events_2011']='Main_controller/loadpage/events_2011';
$route['events_2006']='Main_controller/loadpage/events_2006';
$route['events_2005']='Main_controller/loadpage/events_2005';
$route['events_2004']='Main_controller/loadpage/events_2004';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
